<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * funciones para validacion de permsos de usuario
 */
	function hay_usuario()
	{ // valida si hay usuario logiado
		if(isset($_SESSION['session'])) // si esta logiado
		{
			return true;
		}else {
            redirect(site_url('Login/sesionInvalida'));
		}
	}   