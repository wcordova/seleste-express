<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('notification_danger'))
{
    function notification_danger($descripcion) {
        $CI =& get_instance();
        $CI->load->library('session');

        $CI->session->set_flashdata('noti_alert', 'color danger');
        $CI->session->set_flashdata('noti_titulo', "Error");
        $CI->session->set_flashdata('noti_descrip', $descripcion);
    }

}

if ( ! function_exists('notification_success'))
{
    function notification_success($descripcion) {
        $CI =& get_instance();
        $CI->load->library('session');

        $CI->session->set_flashdata('noti_alert', 'color success');
        $CI->session->set_flashdata('noti_titulo', "Exito!");
        $CI->session->set_flashdata('noti_descrip', $descripcion);
    }

}

if ( ! function_exists('notification_warning'))
{
    function notification_warning($descripcion) {
        $CI =& get_instance();
        $CI->load->library('session');

        $CI->session->set_flashdata('noti_alert', 'color warning');
        $CI->session->set_flashdata('noti_titulo', "Advertencia");
        $CI->session->set_flashdata('noti_descrip', $descripcion);
    }

}

if ( ! function_exists('notification_primary'))
{
    function notification_primary($descripcion) {
        $CI =& get_instance();
        $CI->load->library('session');

        $CI->session->set_flashdata('noti_alert', 'color primary');
        $CI->session->set_flashdata('noti_titulo', "Aviso!");
        $CI->session->set_flashdata('noti_descrip', $descripcion);
    }

}