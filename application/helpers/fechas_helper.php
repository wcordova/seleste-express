<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	function fecha_larga()
    { 
        setlocale(LC_TIME,'es_ES.UTF-8');
        return strftime("%A, %d de %B de %Y");
	}   
