<div class="card card-table">
    <div class="card-header">Listado de Tipo Productos <button class="btn btn-space btn-primary mdi mdi-collection-plus" onclick="location.href='<?= site_url('cla/tipo_producto/form'); ?>'"> Agregar</button>
    </div>
    <div class="card-body">
        <table class="table table-striped table-hover table-fw-widget table3">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Nombre</th>
                    <th>Estado</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($resultados as $row) : ?>
                    <tr class="<?php if ($row->inactivo == 0) {
                                    echo "primary";
                                } else {
                                    echo "danger";
                                } ?>">
                        <td><?= $row->tipo_producto; ?></td>
                        <td><?= $row->nombre; ?></td>
                        <td><?php if ($row->inactivo == 0) {
                                echo "Activo";
                            } else {
                                echo "Inactivo";
                            } ?></td>
                        <td class="actions">
                            <a class="icon" href="<?= site_url('cla/tipo_producto/form/' . $row->tipo_producto); ?>"><i class="mdi mdi-edit"></i></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>