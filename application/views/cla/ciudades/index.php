<?php
$resultados = $this->Model_ciudades->getCiudades($resultado->pais);
?>
</div>
<div class="card-body">
    <table id="" class="table table-striped table-hover table-fw-widget table1">
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Estado</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($resultados as $row) : ?>
                <tr class="<?php if ($row->inactivo == 0) {
                                echo "primary";
                            } else {
                                echo "danger";
                            } ?>">
                    <td><?= $row->nombre; ?></td>
                    <td id="estado"><?php if ($row->inactivo == 0) {
                                        echo "Activo";
                                    } else {
                                        echo "Inactivo";
                                    } ?></td>
                    <td class="actions">
                        <div class="switch-button switch-button-xs">                        
                          <input type="checkbox" onchange="cambiarEstado(this.value);" value="<?= $row->ciudad ?>" <?php if($row->inactivo == 0){ echo "checked"; } ?> name="swt1<?php echo $row->ciudad ?>" id="swt1<?php echo $row->ciudad ?>"><span>
                            <label for="swt1<?php echo $row->ciudad ?>"></label></span>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<script type="text/javascript">
    function cambiarEstado(value){
        console.log("ciudad " + value);
        $.ajax({
	          	url:'<?= site_url('cla/Ciudades/cambiarEstado') ?>',
	          	method: "post",
	          	dataType : 'json',
	          	data: { ciudad: value },          	
	          	success: function (data) { 
	          		console.log(data);
	            }
	        });	
    }
</script>