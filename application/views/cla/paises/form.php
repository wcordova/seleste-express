<?php
$pais = "";
$nombre = "";
$inactivo = 0;

if (!empty($resultado)) {
    $pais = $resultado->pais;
    $nombre = $resultado->nombre;
    $inactivo = $resultado->inactivo;
}

?>

<div class="card card-border-color card-border-color-primary">
    <div class="card-header card-header-divider"><?= $titulo; ?><span class="card-subtitle"><?= $subTitulo; ?></span></div>
    
    <?php if ($this->session->flashdata('eok')<>''): ?>
		<div class="alert alert-success alert-dismissible" role="alert">
            <div class="icon"><span class="mdi mdi-check"></span></div>
            <div class="message">
                <button class="close" type="button" data-dismiss="alert" aria-label="Close">
                    <span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <strong>Ok!</strong> <?php echo $this->session->flashdata('eok');?>
            </div>
        </div>
	<?php endif ?>

	<?php if ($this->session->flashdata('eerror')<>''): ?>
        <div class="alert alert-danger alert-dismissible" role="alert">
            <div class="icon"><span class="mdi mdi-close-circle-o"></span></div>
            <div class="message">
                <button class="close" type="button" data-dismiss="alert" aria-label="Close">
                    <span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <strong>Error!</strong> <?php $this->session->flashdata('eerror'); ?>
            </div>
        </div>
	<?php endif ?>
    
    <div class="card-body">
        <form action="<?= $action; ?>" method="post" data-parsley-validate novalidate>
            <div class="form-group pt-2"> 
                <label for="nombre">Abreviatura País</label>
                <input type="text" class="form-control" id="pais" name="pais" maxlength="2" value="<?= $pais; ?>">
            </div>
            <div class="form-group pt-2">
                <label for="nombre">Nombre</label>
                <input class="form-control" id="nombre" name="nombre" type="text" value="<?= $nombre; ?>" placeholder="Ingrese el nombre" required>
            </div>
           
            <?php if($pais != ""): ?>
                <div class="form-group">
                    <label for="estado">Estado</label>
                    <div class="form-check mt-1">
                        <label class="custom-control custom-radio">
                            <input class="custom-control-input" type="radio" id="inactivo" name="inactivo" value="0" <?php if($inactivo == '0'){ echo "checked"; } ?>><span class="custom-control-label">Activo</span>
                        </label>
                        <label class="custom-control custom-radio">
                            <input class="custom-control-input" type="radio" id="inactivo" name="inactivo" value="1" <?php if($inactivo == '1'){ echo "checked"; } ?>><span class="custom-control-label">Inactivo</span>
                        </label>
                    </div>
                </div>
            <?php endif; ?>
            <div class="row pt-3">
                    <p class="text-right">
                        <button class="btn btn-space btn-primary" type="submit">Guardar</button>
                        <button onclick="location.href='<?= site_url('cla/paises'); ?>'" type="button" class="btn btn-space btn-secondary">Cancel</button>
                    </p>
            </div>
        </form>
    </div>
    <div class="card-body">
        <?php 
        if($pais != ""){
            $this->load->view('cla/ciudades/index', $resultado);
        }  
        
        ?>
    </div>
</div>