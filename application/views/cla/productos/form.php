<?php
$producto = "";
$tipo_producto = "";
$descripcion = "";
$nombre = "";
$inactivo = 0;

if (!empty($resultado)) {
    $producto = $resultado->producto;
    $tipo_producto = $resultado->tipo_producto;
    $descripcion = $resultado->descripcion;
    $nombre = $resultado->nombre;
    $inactivo = $resultado->inactivo;
}

?>

<div class="card card-border-color card-border-color-primary">
    <div class="card-header card-header-divider"><?= $titulo; ?><span class="card-subtitle"><?= $subTitulo; ?></span></div>

    <?php if ($this->session->flashdata('eok') <> '') : ?>
        <div class="alert alert-success alert-dismissible" role="alert">
            <div class="icon"><span class="mdi mdi-check"></span></div>
            <div class="message">
                <button class="close" type="button" data-dismiss="alert" aria-label="Close">
                    <span class="mdi mdi-close" aria-hidden="true"></span></button>
                <strong>Ok!</strong> <?php echo $this->session->flashdata('eok'); ?>
            </div>
        </div>
    <?php endif ?>

    <?php if ($this->session->flashdata('eerror') <> '') : ?>
        <div class="alert alert-danger alert-dismissible" role="alert">
            <div class="icon"><span class="mdi mdi-close-circle-o"></span></div>
            <div class="message">
                <button class="close" type="button" data-dismiss="alert" aria-label="Close">
                    <span class="mdi mdi-close" aria-hidden="true"></span></button>
                <strong>Error!</strong> <?php $this->session->flashdata('eerror'); ?>
            </div>
        </div>
    <?php endif ?>

    <div class="card-body">
        <form action="<?= $action; ?>" method="post" data-parsley-validate novalidate>
        <input hidden id="producto" name="producto" value="<?= $producto ?>">
            <div class="form-group">
                <label for="tipo_producto">Tipo de Producto</label>
                <select name="tipo_producto" id="tipo_producto" class="select2" required>
                    <option>Seleccione una opción..<option>
                    <?php foreach ($tipo_productos as $row) {
                        if ($tipo_producto == $row->tipo_producto) { ?>
                            <option value="<?= $row->tipo_producto; ?>" selected><?= $row->nombre; ?></option>
                        <?php
                        } else {
                        ?>
                            <option value="<?= $row->tipo_producto; ?>"><?= $row->nombre; ?></option>
                    <?php
                        }
                    } ?>
                </select>
            </div>

            <div class="form-group pt-2">
                <label for="nombre">Nombre</label>
                <input class="form-control" id="nombre" name="nombre" type="text" value="<?= $nombre; ?>" placeholder="Ingrese el nombre" required>
            </div>
            <div class="form-group pt-2">
                <label for="nombre">Descripción</label>
                <textarea class="form-control" id="descripcion" name="descripcion" type="text" placeholder="Ingrese la descripcion"><?= $descripcion; ?></textarea>
            </div>

            <?php if ($producto != "") : ?>
                <div class="form-group">
                    <label for="estado">Estado</label>
                    <div class="form-check mt-1">
                        <label class="custom-control custom-radio">
                            <input class="custom-control-input" type="radio" id="inactivo" name="inactivo" value="0" <?php if ($inactivo == '0') { echo "checked"; } ?>><span class="custom-control-label">Activo</span>
                        </label>
                        <label class="custom-control custom-radio">
                            <input class="custom-control-input" type="radio" id="inactivo" name="inactivo" value="1" <?php if ($inactivo == '1') { echo "checked"; } ?>><span class="custom-control-label">Inactivo</span>
                        </label>
                    </div>
                </div>
            <?php endif; ?>
            <div class="row pt-3">
                <p class="text-right">
                    <button class="btn btn-space btn-primary" type="submit">Guardar</button>
                    <button onclick="location.href='<?= site_url('cla/productos'); ?>'" type="button" class="btn btn-space btn-secondary">Cancel</button>
                </p>
            </div>
        </form>
    </div>
</div>