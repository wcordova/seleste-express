<div class="card card-table">
    <div class="card-header">Listado de productos <button class="btn btn-space btn-primary mdi mdi-collection-plus" onclick="location.href='<?= site_url('cla/productos/form'); ?>'"> Agregar</button>
    </div>
    <div class="card-body">
        <table id="" class="table table-striped table-hover table-fw-widget table3">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Tipo Producto</th>
                    <th>Nombre</th>
                    <th>Descripción</th>
                    <th>Estado</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($resultados as $row) : ?>
                    <tr class="<?php if ($row->inactivo == 0) {
                                    echo "primary";
                                } else {
                                    echo "danger";
                                } ?>">
                        <td><?= $row->producto; ?></td>
                        <th>
                            <?php  
                                $dataTipoProducto = $this->Model_tipo_producto->getData($row->tipo_producto);
                                echo $dataTipoProducto->nombre;
                            ?>
                        </th>
                        <td><?= $row->nombre; ?></td>
                        <th><?= $row->descripcion; ?></th>
                        <td id="estado"><?php if ($row->inactivo == 0) {
                                echo "Activo";
                            } else {
                                echo "Inactivo";
                            } ?></td>
                        <td class="actions">
                            <a class="icon" href="<?= site_url('cla/productos/form/' . $row->producto); ?>"><i class="mdi mdi-edit"></i></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>