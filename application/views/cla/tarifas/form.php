<?php
$tarifa = "";
$producto = "";
$tipo_producto = "";
$pais = "";
$ciudad = "";
$monto = 0.00;
$descripcion = "";
$inactivo = 0;

if (!empty($resultado)) {
    $tarifa = $resultado->tarifa;
    $producto = $resultado->producto;
    $tipo_producto = $resultado->tipo_producto;
    $pais = $resultado->pais;
    $ciudad = $resultado->ciudad;
    $monto = $resultado->monto;
    $descripcion = $resultado->descripcion;
    $inactivo = $resultado->inactivo;
}

?>

<div class="card card-border-color card-border-color-primary">
    <div class="card-header card-header-divider"><?= $titulo; ?><span class="card-subtitle"><?= $subTitulo; ?></span></div>

    <?php if ($this->session->flashdata('eok') <> '') : ?>
        <div class="alert alert-success alert-dismissible" role="alert">
            <div class="icon"><span class="mdi mdi-check"></span></div>
            <div class="message">
                <button class="close" type="button" data-dismiss="alert" aria-label="Close">
                    <span class="mdi mdi-close" aria-hidden="true"></span></button>
                <strong>Ok!</strong> <?php echo $this->session->flashdata('eok'); ?>
            </div>
        </div>
    <?php endif ?>

    <?php if ($this->session->flashdata('eerror') <> '') : ?>
        <div class="alert alert-danger alert-dismissible" role="alert">
            <div class="icon"><span class="mdi mdi-close-circle-o"></span></div>
            <div class="message">
                <button class="close" type="button" data-dismiss="alert" aria-label="Close">
                    <span class="mdi mdi-close" aria-hidden="true"></span></button>
                <strong>Error!</strong> <?php $this->session->flashdata('eerror'); ?>
            </div>
        </div>
    <?php endif ?>

    <div class="card-body main-content container-fluid">
        <form action="<?= $action; ?>" method="post" data-parsley-validate novalidate>
            <input hidden id="tarifa" name="tarifa" value="<?= $tarifa ?>">
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="pais">País</label>
                        <select name="pais" id="pais" class="select2" onchange="cargarCiudad(this.value);" required>
                            <option>Seleccione una opción..
                            <option>
                                <?php foreach ($paises as $row):
                                    if ($pais == $row->pais): ?>
                                        <option value="<?= $row->pais; ?>" selected><?= $row->nombre; ?></option>
                                    <?php
                                    else:
                                    ?>
                                        <option value="<?= $row->pais; ?>"><?= $row->nombre; ?></option>
                                    <?php
                                    endif;
                                endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="ciudad">Ciudad</label>
                        <select name="ciudad" id="ciudad" class="select2">
                            <option>Seleccione una opción..
                            <option>

                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="tipo_producto">Tipo de Producto</label>
                        <select name="tipo_producto" id="tipo_producto" class="select2" onchange="cargarProducto(this.value);" required>
                            <option>Seleccione una opción..
                            <option>
                                <?php foreach ($tipo_productos as $row): 
                                    if ($tipo_producto == $row->tipo_producto) : ?>
                                        <option value="<?= $row->tipo_producto; ?>" selected><?= $row->nombre; ?></option>
                                    <?php
                                         else: 
                                    ?>
                                        <option value="<?= $row->tipo_producto; ?>"><?= $row->nombre; ?></option>
                                <?php
                                    endif;
                                endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="producto">Producto</label>
                        <select name="producto" id="producto" class="select2" required>
                            <option>Seleccione una opción..
                            <option>

                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6">
                    <label for="">Tarifa</label>
                    <input type="number" class="form-control text-right" type="text" placeholder="$0" id="monto" name="monto" value="<?=$monto; ?>" required>
                </div>
            </div>

            <div class="form-group pt-2">
                <label for="nombre">Descripción</label>
                <textarea class="form-control" id="descripcion" name="descripcion" type="text" placeholder="Ingrese la descripcion" required><?= $descripcion; ?></textarea>
            </div>

            <?php if ($producto != "") : ?>
                <div class="form-group">
                    <label for="estado">Estado</label>
                    <div class="form-check mt-1">
                        <label class="custom-control custom-radio">
                            <input class="custom-control-input" type="radio" id="inactivo" name="inactivo" value="0" <?php if ($inactivo == '0') {
                                                                                                                            echo "checked";
                                                                                                                        } ?>><span class="custom-control-label">Activo</span>
                        </label>
                        <label class="custom-control custom-radio">
                            <input class="custom-control-input" type="radio" id="inactivo" name="inactivo" value="1" <?php if ($inactivo == '1') {
                                                                                                                            echo "checked";
                                                                                                                        } ?>><span class="custom-control-label">Inactivo</span>
                        </label>
                    </div>
                </div>
            <?php endif; ?>
            <div class="row pt-3">
                <p class="text-right">
                    <button class="btn btn-space btn-primary" type="submit">Guardar</button>
                    <button onclick="location.href='<?= site_url('cla/tarifas'); ?>'" type="button" class="btn btn-space btn-secondary">Cancel</button>
                </p>
            </div>
        </form>
    </div>
</div>
<script src="<?= base_url('public/') ?>assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function(){
        cargarCiudad('<?= $pais; ?>');
        cargarProducto('<?= $tipo_producto; ?>');
    });
    

    function cargarCiudad(value) {
        let ciudad = '<?= $ciudad ?>';
        $.ajax({
            url: '<?= site_url('cla/ciudades/getCiudades') ?>',
            method: "POST",
            dataType: "json",
            data: { pais: value },               
            success: function(data) {
                $('#ciudad').empty();
                App.ajaxLoader();
                if (data == 0) {
                    $("#ciudad").append('<option value>No existen registros que mostrar..</option>');
                } else {
                    $("#ciudad").append('<option value>Seleccione una opción..</option>');
                    data.forEach(function(datosOpcion) {
                        if(datosOpcion.ciudad == ciudad){
                            $("#ciudad").append('<option value="' + datosOpcion.ciudad + '" selected>' + datosOpcion.nombre + '</option>');
                        }else{
                            $("#ciudad").append('<option value="' + datosOpcion.ciudad + '">' + datosOpcion.nombre + '</option>');
                        }
                    });
                }
            }
        });
    }

    function cargarProducto(value) {
        let tipo_producto = '<?= $producto ?>';
        $.ajax({
            url: '<?= site_url('cla/productos/getProductos') ?>',
            method: "POST",
            dataType: "json",
            data: { tipo_producto: value },               
            success: function(data) {
                $('#producto').empty();
                App.ajaxLoader();
                if (data == 0) {
                    $("#producto").append('<option value>No existen registros que mostrar..</option>');
                } else {
                    $("#producto").append('<option value>Seleccione una opción..</option>');
                    data.forEach(function(datosOpcion) {
                        if (datosOpcion.producto == tipo_producto) {
                            $("#producto").append('<option value="' + datosOpcion.producto + '" selected>' + datosOpcion.nombre + '</option>');
                        } else {
                            $("#producto").append('<option value="' + datosOpcion.producto + '">' + datosOpcion.nombre + '</option>');
                        }
                    });
                }
            }
        });
    }
</script>