<div class="">
    <div class="card-header">Listado de tarifas <button class="btn btn-space btn-primary mdi mdi-collection-plus" onclick="location.href='<?= site_url('cla/tarifas/form'); ?>'"> Agregar</button>
    </div>
    <div class="main-content container-fluid">
        <div class="tab-container">
            <ul class="nav nav-tabs nav-tabs-classic" role="tablist">
                <?php foreach ($paises as $pais) : ?>
                    <li class="nav-item"><a class="nav-link <?php if ($pais->pais == 'US') { echo "active"; } ?>" href="#<?= $pais->pais ?>" data-toggle="tab" role="tab"><?= $pais->pais ?></a></li>
                <?php endforeach; ?>
            </ul>
            <div class="tab-content">
                <?php foreach ($paises as $pais) : 
                        $resultados = $this->Model_tarifas->getDatosPorPais($pais->pais);
                ?>
                    <div class="tab-pane <?php if ($pais->pais == 'US') { echo "active";  } ?>" id="<?= $pais->pais ?>" role="tabpanel">
                        <h4><?= $pais->nombre ?></h4>
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>                                    
                                    <th>Producto</th>
                                    <th>Ciudad</th>
                                    <th>Descripción</th>
                                    <th>Tarifas</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if(!empty($resultados)):                                            
                                    foreach ($resultados as $result): 
                                        $ciudad = "Resto del país";
                                        if ($result->des_ciudad != null) {
                                            $ciudad = $result->des_ciudad;
                                        }   

                                    ?>
                                        <tr class="primary">                                            
                                            <td class="cell-detail"><span><?= $result->des_producto; ?></span><span class="cell-detail-description"><?= $result->des_tipo_producto; ?></span></td>
                                            <td class="cell-detail"><span><?= $ciudad; ?></span></td>                                            
                                            <td class="cell-detail"><span><?= '$'.$result->monto; ?></span></td>
                                            <td class="cell-detail"><?= $result->descripcion; ?></td>
                                            <td class="actions">
                                                <a class="icon" href="<?= site_url('cla/tarifas/form/' . $result->tarifa); ?>"><i class="mdi mdi-edit"></i></a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <tr>
                                        <td colspan="6">No existen datos</td>
                                    </tr>
                                <?php endif; ?>
                            </tbody>
                        </table>

                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>