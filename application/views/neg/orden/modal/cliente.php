<?php
    $cliente = $resultado->cliente;
    $cui = $resultado->cui;
    $nit = $resultado->nit;
    $codigo_postal = $resultado->codigo_postal;
    $direccion2 = $resultado->direccion2;
    $direccion3 = $resultado->direccion3;
?>

<div class="modal-header modal-header-colored">
    <h3 class="modal-title"><?= $titulo ?></h3>
    <button class="close md-close" type="button" data-dismiss="modal" aria-hidden="true"><span class="mdi mdi-close"> </span></button>
</div>
<div class="modal-body">
    <input type="hidden" type="text" id="cliente" name="cliente" value="<?= $cliente ?>" readonly>

    <label><b>CUI</b></label>
    <input class="form-control" type="text" id="cui" name="cui" value="<?= $cui ?>" placeholder="Ingrese el CUI">

    <label><b>Nit</b></label>
    <input class="form-control" type="text" id="nit" name="nit" value="<?= $nit ?>" placeholder="Ingrese el NIT">

    <label><b>Código Postal</b></label>
    <input class="form-control" type="text" id="codigo_postal" name="codigo_postal" value="<?= $codigo_postal ?>" placeholder="Ingrese código postal">

    <label><b>Dirección secundaria</b></label>
    <input class="form-control" type="text" id="direccion2" name="direccion2" value="<?= $direccion2 ?>" placeholder="Ingrese una dirección">

    <label><b>Dirección (otro)</b></label>
    <input class="form-control" type="text" id="direccion3" name="direccion3" value="<?= $direccion3 ?>" placeholder="Ingrese una dirección">


</div>
<div class="modal-footer">
    <button class="btn btn-secondary md-close" type="button" data-dismiss="modal">Cancelar</button>
    <button class="btn btn-primary md-close" type="button" data-dismiss="modal" onclick="guardarForm();">Guardar</button>
</div>
<script type="text/javascript">
    function guardarForm(){
        let cliente = $('#cliente').val();
        let cui = $('#cui').val();
        let nit = $('#nit').val();
        let codigo_postal = $('#codigo_postal').val();
        let direccion2 = $('#direccion2').val();
        let direccion3 = $('#direccion3').val();

        $.ajax({
            url:'<?= site_url('neg/orden/guardarForm') ?>',
            method: "POST",
            data: { 
                cliente: cliente, 
                cui: cui, 
                nit: nit, 
                codigo_postal: codigo_postal, 
                direccion2: direccion2, 
                direccion3: direccion3
            },              
            success: function (data) {        
                $.gritter.add({
                    title: 'Actualización!',
                    text: 'Cliente actualizado con éxito!',
                    class_name: 'color success'
                });
            },
            error: function (data) {
                $.gritter.add({
                    title: 'Error',
                    text: 'Cliente no ha sido actualizado!',
                    class_name: 'color danger'
                });
            }
            
        });

    }
</script>