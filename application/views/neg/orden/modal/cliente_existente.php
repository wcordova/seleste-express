<div class="modal-header modal-header-colored">
    <h3 class="modal-title"><?= $titulo ?></h3>
    <button class="close md-close" type="button" data-dismiss="modal" aria-hidden="true"><span class="mdi mdi-close"> </span></button>
</div>

<div class="container-fluid">
    <h4>Elija un cliente para esta órden!</h4>
</div>

<div class="modal-body">
    <table class="table">
        <thead>
            <tr>
                <th>Nombre Cliente</th>
                <th>Teléfono</th>
                <th>Dirección</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($resultado as $row) :
                $getCiudad = $this->Model_ciudades->getData($row->ciudad);
            ?>
                <tr>
                    <td><?= $row->nombres . ' ' . $row->apellidos; ?></td>
                    <td><?= $row->telefono; ?></td>
                    <td><?= $row->direccion1 . ', ' . $getCiudad->nombre . ', ' . $row->pais ?></td>
                    <td>
                    <button class="btn btn-primary md-close btn-xs" type="button" data-dismiss="modal" onclick="clienteExistenteNuevaOrden(<?= $row->cliente; ?>, <?= $row->ciudad; ?>);">Cargar</button>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

</div>
<div class="modal-footer">
    <button class="btn btn-secondary md-close" type="button" data-dismiss="modal">Cancelar</button>
</div>

<script type="text/javascript">
    function clienteExistenteNuevaOrden(cliente, ciudad) {
        let orden = "<?= $orden; ?>";
        let pais = "<?= $pais; ?>";
        let tipo = "<?= $tipo; ?>";
        
        console.log("cliente: " + cliente);
        console.log("pais: " + pais);
        console.log("ciudad: " + ciudad);
        console.log("orden: " + orden);
        console.log("tipo: " + tipo);

        $.ajax({
            url: '<?= site_url('neg/clientes/clienteExistenteNuevaOrden') ?>',
            method: "POST",
            data: { cliente: cliente, tipo: tipo, orden: orden, pais: pais, ciudad: ciudad },
            success: function(data) {
                $('#contenidoModal').html(data);
                $('#form-cliente-origen').modal('show');
                window.location.href = '<?= site_url('neg/Orden/form/') ?>' + data;
            }

        });
    }
</script>