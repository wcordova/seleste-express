<div class="card card-table">
    <div class="card-header">Listado de Guías Iniciadas<button class="btn btn-space btn-primary mdi mdi-collection-plus" onclick="location.href='<?= site_url('neg/orden/form'); ?>'"> Nueva Órden</button>
    </div>
    <div class="card-body">
        <table class="table table-striped table-hover table-fw-widget table3">
            <thead>
                <tr>
                    <th>No. Orden</th>
                    <th>Origen</th>
                    <th>De</th>
                    <th>Destino</th>
                    <th>A</th>
                    <th>Estado</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($resultados as $row) :
                    if ($row->cliente_origen > 0) {
                        $cliente_origen = $this->Model_cliente->getData($row->cliente_origen);
                        $ciudad_origen = $this->Model_ciudades->getData($row->ciudad_origen);
                    }
                    if ($row->cliente_destino > 0) {
                        $cliente_destino = $this->Model_cliente->getData($row->cliente_destino);
                        $ciudad_destino = $this->Model_ciudades->getData($row->ciudad_destino);
                    }

                ?>
                    <tr>
                        <td style="width:8%"><?= $row->orden; ?></td>
                        <td>
                            <?php if ($row->cliente_origen > 0 && $row->ciudad_origen > 0) {
                                echo $row->origen . ' - ' . $ciudad_origen->nombre;
                            } ?>
                        </td>
                        <td>
                            <?php if ($row->cliente_origen > 0) {
                                echo $cliente_origen->nombres . ' ' . $cliente_origen->apellidos;
                            } ?>
                        </td>
                        <td>
                            <?php if ($row->cliente_destino > 0 && $row->ciudad_destino > 0) {
                                echo $row->destino . ' - ' . $ciudad_destino->nombre;
                            } ?>
                        </td>
                        <td>
                            <?php if ($row->cliente_destino > 0) {
                                echo $cliente_destino->nombres . ' ' . $cliente_destino->apellidos;
                            } ?>

                        </td>
                        <td>
                            <?= ($row->estado == 0) ? 'Iniciado' : 'Completado'; ?>
                        </td>
                        <td class="actions">
                            <button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown">Acciones <span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                            <div class="dropdown-menu" role="menu">
                                <a class="dropdown-item" href="<?= site_url('neg/orden/form/' . $row->orden); ?>">Editar</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" onclick="cancelarOrden(<?= $row->orden; ?>);" href="#">Cancelar</a>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
<script type="text/javascript">
    function cancelarOrden(orden) {
        $.ajax({
            url: '<?= site_url('neg/Orden/cancelarOrden') ?>',
            method: "POST",
            data: {
                orden: orden
            },
            success: function(data) {
                window.location.href = '<?= site_url('neg/Orden') ?>';
            }
        });
    }
</script>