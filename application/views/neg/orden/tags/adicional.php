<div id="ordenResumen">
    <?php $this->load->view('neg/orden/tags/resumen', $datosOrden); ?>
</div>

<div class="form-group row">
    <div class="col-sm-12">
        <button class="btn btn-secondary btn-space wizard-previous" data-wizard="#wizard1">Anterior</button>
        <button onclick="completarForm(<?= $datosOrden->orden ?>);" class="btn btn-success btn-space wizard-next" data-wizard="#wizard1">Completado</button>
    </div>
</div>

<script type="text/javascript">
    function imprimir(orden) {
        $.ajax({
            url: '<?= site_url('neg/Reportes/printOrden') ?>',
            method: "POST",
            data: { orden: orden },
            success: function(data) {                
                let printContents = data;
                w = window.open();
                w.document.write(printContents);
                w.document.close(); 
                w.focus(); 
                w.print();
                w.close();
                return true;
            }
        });        

    }

    function completarForm(orden) {
        $.ajax({
            url: '<?= site_url('neg/Orden/completarOrden') ?>',
            method: "POST",
            data: { orden: orden },
            success: function(data) {                
                window.location.href = '<?= site_url('neg/Orden') ?>';
            }
        });
    }
</script>