<?php
$datosTipoProducto = $this->Model_tipo_producto->getData();

# $tipo_producto = 0;
# $producto = 0;

$detalle            = 0;
$tipo_producto      = 0;
$producto           = 0;
$moneda_tarifa      = 0;
$tarifa             = 0;
$moneda             = 0;
$valor_declarado    = 0;
$peso               = 0;
$unidad_medida      = 0;
$unidad_peso        = 0;
$largo              = 0;
$ancho              = 0;
$alto               = 0;
$peso_volumetrico   = 0;
$envios_domesticos  = 0;
$descripcion        = 0;

if (!empty($this->data["datosDetalle"])) {
    $detalle            = $this->data["datosDetalle"][0]->detalle;
    $tipo_producto      = $this->data["datosDetalle"][0]->tipo_producto;
    $producto           = $this->data["datosDetalle"][0]->producto;
    $moneda_tarifa      = $this->data["datosDetalle"][0]->moneda_tarifa;
    $tarifa             = $this->data["datosDetalle"][0]->tarifa;
    $moneda             = $this->data["datosDetalle"][0]->moneda;
    $valor_declarado    = $this->data["datosDetalle"][0]->valor_declarado;
    $peso               = $this->data["datosDetalle"][0]->peso;
    $unidad_medida      = $this->data["datosDetalle"][0]->unidad_medida;
    $unidad_peso        = $this->data["datosDetalle"][0]->unidad_peso;
    $largo              = $this->data["datosDetalle"][0]->largo;
    $ancho              = $this->data["datosDetalle"][0]->ancho;
    $alto               = $this->data["datosDetalle"][0]->alto;
    $peso_volumetrico   = $this->data["datosDetalle"][0]->peso_volumetrico;
    $envios_domesticos  = $this->data["datosDetalle"][0]->envios_domesticos;
    $descripcion        = $this->data["datosDetalle"][0]->descripcion;
}

?>

<div class="container p-0">
    <form id="formDetalle" class="form-horizontal group-border-dashed" method="post">
        <input type="hidden" id="orden" name="orden" value="<?= $orden; ?>" readonly>
        <input type="hidden" id="detalle" name="detalle" value="<?= $detalle; ?>">
        <div class="form-group row">
            <div class="col-sm-7">
                <h3 class="wizard-title">Detalle de envío</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <label><b>Tipo Producto</b></label>
                <select class="select2" name="tipo_producto" id="tipo_producto" onchange="cargarProducto(this.value);">
                    <option value="0">Seleccione una opción..</option>
                    <?php foreach ($datosTipoProducto as $key) : ?>
                        <?php if($tipo_producto == $key->tipo_producto): ?>
                            <option value="<?= $key->tipo_producto; ?>" selected><?= $key->nombre; ?></option>
                        <?php else : ?>
                            <option value="<?= $key->tipo_producto; ?>"><?= $key->nombre; ?></option>
                        <?php endif ?>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="col-lg-6">
                <label><b>Producto</b></label>
                <select class="select2" name="producto" id="producto" onchange="cargarTarifa(this.value, <?= $this->data["datosOrden"]->ciudad_destino ?>);">
                    <option value="0">Seleccione una opción..</option>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <label for="">Moneda Tarifa</label>
                <select name="moneda_tarifa" id="moneda_tarifa" class="form-control">

                    <?php if($moneda_tarifa == 0 && $detalle > 0):?>
                        <option value="0" selected>$ (dolar)</option>
                    <?php elseif($moneda_tarifa == 1 && $detalle > 0): ?>
                        <option value="1" selected>Q (quetzal)</option>e
                    <?php else: ?>
                         <option value="0">$ (dolar)</option>
                         <option value="1">Q (quetzal)</option>
                    <?php endif ?>
                </select>
            </div>
            <div class="col-lg-6">
                <label><b>Tarifa</b></label>
                <input type="number" class="form-control" id="tarifa" name="tarifa" value="<?= $tarifa; ?>">
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <label for="">Moneda Valor Declarado</label>
                <select name="moneda" id="moneda" class="form-control">

                    <?php if($moneda == 0 && $detalle > 0): ?>
                        <option value="0" selected>$ (dolar)</option>
                    <?php elseif($moneda == 1 && $detalle > 0): ?>
                        <option value="1" selected>Q (quetzal)</option>
                    <?php else: ?>
                        <option value="0">$ (dolar)</option>
                        <option value="1">Q (quetzal)</option>
                    <?php endif ?>
                </select>
            </div>
            <div class="col-lg-6">
                <label><b>Valor declarado</b></label>
                <input type="number" class="form-control" id="valor_declarado" name="valor_declarado" value="<?= $valor_declarado; ?>">
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <label><b>Peso</b></label>
                <input type="number" class="form-control" id="peso" name="peso" value="<?= $peso; ?>">
            </div>
            <div class="col-lg-6">
                <label><b>Unidad</b></label>
                <select name="unidad_peso" id="unidad_peso" class="form-control">
                    <option value="lb">Libras</option>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3">
                <label><b>Largo</b></label>
                <input type="number" class="form-control" id="largo" name="largo" onchange="calcVolWeight();" value="<?= $largo; ?>">
            </div>
            <div class="col-lg-3">
                <label><b>Ancho</b></label>
                <input type="number" class="form-control" id="ancho" name="ancho" onchange="calcVolWeight();" value="<?= $ancho; ?>">
            </div>
            <div class="col-lg-3">
                <label><b>Alto</b></label>
                <input type="number" class="form-control" id="alto" name="alto" onchange="calcVolWeight();" value="<?= $alto; ?>">
            </div>
            <div class="col-lg-3">
                <label for="">Unidad</label>
                <select name="unidad_medida" id="unidad_medida" class="form-control" onchange="calcVolWeight();">
                    <option value="in">pulgadas/libras</option>
                    <!-- <option value="cm">cm/kg</option> -->
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <label><b>Peso volumétrico</b></label> <span class="badge badge-primary" id="unidad_peso_vol"></span>
                <input type="number" class="form-control" id="peso_volumetrico" name="peso_volumetrico" value="<?= $peso_volumetrico;?>">
            </div>
            <div class="col-lg-6">
                <label><b>Envíos domésticos ($)</b></label>
                <input type="number" class="form-control" id="envios_domesticos" name="envios_domesticos" value="<?= $envios_domesticos; ?>">
            </div>
        </div>       

        <hr>
        <div class="row">
            <div class="col-lg-12">
                <label for="">Descripción</label>
                <textarea name="descripcion" id="descripcion" cols="30" rows="5" class="form-control" value="<?= $descripcion;?>"></textarea>
            </div>
        </div>

        <br>
        <button type="button" class="btn btn-primary btn-space" onclick="agregarItems();">Agregar</button>
        <div class="form-group row">
            <div class="col-sm-12">
                <button class="btn btn-secondary btn-space wizard-previous" data-wizard="#wizard1">Anterior</button>
                <?php if ($orden > 0) : ?>
                    <button class="btn btn-warning btn-space wizard-next" data-wizard="#wizard1"> Siguiente paso <span class="icon-class mdi mdi-arrow-right"></span></button>
                <?php endif; ?>
            </div>
        </div>
    </form>
    <div id="listaDetalleOrden">
        <?php $this->load->view('neg/orden/tags/detalle_lista', $orden); ?>
    </div>

</div>
<script src="<?= base_url('public/') ?>assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function() {
        cargarProducto('<?= $tipo_producto; ?>');
    });

    function cargarProducto(value) {
        let tipo_producto = '<?= $producto ?>';
        $('#tarifa').val(0);
        $.ajax({
            url: '<?= site_url('cla/productos/getProductos') ?>',
            method: "POST",
            dataType: "json",
            data: {
                tipo_producto: value
            },
            success: function(data) {
                $('#producto').empty();
                App.ajaxLoader();
                if (data == 0) {
                    $("#producto").append('<option value="0">No existen registros que mostrar..</option>');
                } else {
                    $("#producto").append('<option value>Seleccione una opción..</option>');
                    data.forEach(function(datosOpcion) {
                        if (datosOpcion.producto == tipo_producto) {
                            $("#producto").append('<option value="' + datosOpcion.producto + '" selected>' + datosOpcion.nombre + '</option>');
                        } else {
                            $("#producto").append('<option value="' + datosOpcion.producto + '">' + datosOpcion.nombre + '</option>');
                        }
                    });
                }
            },
            error: function(request, status, error) {
                notificacion("Notificación", "Error al cargar los datos.", "danger");
                $('#tarifa').val(0);
            }
        });
    }

    function cargarTarifa(producto, destino) {
        if (producto > 0) {
            $.ajax({
                url: '<?= site_url('cla/tarifas/getTarifas') ?>',
                method: "POST",
                dataType: "json",
                data: {
                    producto: producto,
                    destino: destino
                },
                success: function(data) {
                    console.log("data " + data);
                    App.ajaxLoader();
                    $('#tarifa').val(data);
                },
                error: function(request, status, error) {
                    notificacion("Notificación", "No existe configuración de tarifas", "danger");
                    $('#tarifa').val(0);
                }
            });
        }

    }

    function pesoVolumetrico() {
        let ancho = $('#ancho').val();
        let largo = $('#largo').val();
        let alto = $('#alto').val();

        let peso_volumetrico = (ancho * largo * alto) / 5000;
        let resultado = Math.round(peso_volumetrico);
    }

    function validaFormulario() {
        let orden = $('#orden').val();
        let tipo_producto = $('#tipo_producto').val();
        let producto = $('#producto').val();
        let tarifa = $('#tarifa').val();
        let moneda = $('#moneda').val();
        let valor_declarado = $('#valor_declarado').val();
        let largo = $('#largo').val();
        let ancho = $('#ancho').val();
        let alto = $('#alto').val();
        let unidad_medida = $('#unidad_medida').val();
        let peso = $('#peso').val();
        let unidad_peso = $('#unidad_peso').val();
        let peso_volumetrico = $('#peso_volumetrico').val();

        if (orden != '' && tipo_producto != '' && producto != '' && producto > 0 &&
            tarifa != '' && moneda != '' && valor_declarado != '' &&
            largo != '' && ancho != '' && alto != '' && unidad_medida != '' &&
            peso != '' && peso_volumetrico != '') {
            return true;
        } else {
            return false;
        }
    }

    function agregarItems() {
        let orden = $('#orden').val();
        if (validaFormulario()) {
            $.ajax({
                url: '<?= site_url('neg/Orden/agregarDetalle') ?>',
                method: "POST",
                dataType: "json",
                data: $('#formDetalle').serialize(),
                success: function(data) {
                    App.ajaxLoader();
                    notificacion("Notificación", "Item agregado", "primary");
                    detalleReresh(orden);
                    limpiarForm();
                    $.ajax({
                        url: '<?= site_url('neg/Orden/actualizarResumen') ?>',
                        method: "POST",
                        data: {
                            orden: orden
                        },
                        success: function(data) {
                            $('#ordenResumen').html(data);
                        }
                    });
                }
            });

        } else {
            notificacion("Notificación", "Faltan datos obligatorios", "danger");
        }

    }

    function detalleReresh(orden) {
        $.ajax({
            url: '<?= site_url('neg/Orden/mostrarDetalle') ?>',
            method: "POST",
            data: {
                orden: orden
            },
            success: function(data) {
                $('#listaDetalleOrden').html(data);
            }
        });
    }

    function limpiarForm() {
        $('#valor_declarado').val('');
        $('#largo').val('');
        $('#ancho').val('');
        $('#alto').val('');
        $('#peso').val('');
        $('#peso_volumetrico').val('');
        $('#descripcion').val('');
        $('#envios_domesticos').val('');
        $('#detalle'.val(''));
    }

    function calcVolWeight() {
        let length = $('#largo').val();
        let width = $('#ancho').val();
        let height = $('#alto').val();
        let weightDivisor = $("#unidad_medida").val() == "in" ? 139 : 5000;
        let roundFactor = $("#unidad_medida").val() == "in" ? 1 : 0.5;

        let units = $("#unidad_medida").val() == "in" ? 'lb' : 'kg';

        // get the volume first
        let originalVolume = length * width * height;
        // Calculate the volumetric weight
        let calcVolWeight = roundFactor * Math.ceil((originalVolume / weightDivisor) / roundFactor);
        $("#unidad_peso_vol").html(units);
        $("#peso_volumetrico").val(parseFloat(calcVolWeight));
    }
</script>