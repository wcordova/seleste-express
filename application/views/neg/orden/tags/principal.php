<?php
$orden = '';
$checked = '';

$origen_pais = 'GT';
$destino_pais = 'US';

$origen_id = '';
$origen_nombres = '';
$origen_apellidos = '';
$origen_ciudad = '';
$origen_direccion = '';
$origen_telefono = '';
$origen_correo = '';

$destino_id = '';
$destino_nombres = '';
$destino_apellidos = '';
$destino_ciudad = '';
$destino_direccion = '';
$destino_telefono = '';
$destino_correo = '';

$departamentos = $this->Model_ciudades->cargarCuidades($origen_pais);
$ciudades = $this->Model_ciudades->cargarCuidades($destino_pais);

$action = site_url('neg/orden/encabezado');

$existOrden = false;
$existOrigen = false;
$existDestino = false;

if ($datosOrden != '') {
    $existOrden = true;
    $action = site_url('neg/orden/encabezadoActualizar');
    $orden = $datosOrden->orden;
    $checked = $datosOrden->cobro_origen_destino == 1 ? 'checked' : '';

    if ($datosOrden->cliente_origen > 0) {
        $existOrigen = true;
        $clienteOrigen = $this->Model_cliente->getData($datosOrden->cliente_origen);
        if ($clienteOrigen) {
            $origen_id = $clienteOrigen->cliente;
            $origen_nombres = $clienteOrigen->nombres;
            $origen_apellidos = $clienteOrigen->apellidos;
            $origen_ciudad = $clienteOrigen->ciudad;
            $origen_direccion = $clienteOrigen->direccion1;
            $origen_telefono = $clienteOrigen->telefono;
            $origen_correo = $clienteOrigen->correo;
        }
    }

    if ($datosOrden->cliente_destino) {
        $existDestino = true;
        $clienteDestino = $this->Model_cliente->getData($datosOrden->cliente_destino);
        if ($clienteDestino) {
            $destino_id = $clienteDestino->cliente;
            $destino_nombres = $clienteDestino->nombres;
            $destino_apellidos = $clienteDestino->apellidos;
            $destino_ciudad = $clienteDestino->ciudad;
            $destino_direccion = $clienteDestino->direccion1;
            $destino_telefono = $clienteDestino->telefono;
            $destino_correo = $clienteDestino->correo;
        }
    }
}

?>

<div class="container p-0">
    <?php if ($this->session->flashdata('eok') <> '') : ?>
        <div class="alert alert-success alert-dismissible" role="alert">
            <div class="icon"><span class="mdi mdi-check"></span></div>
            <div class="message">
                <button class="close" type="button" data-dismiss="alert" aria-label="Close">
                    <span class="mdi mdi-close" aria-hidden="true"></span></button>
                <strong>Ok!</strong> <?php echo $this->session->flashdata('eok'); ?>
            </div>
        </div>
    <?php endif ?>

    <?php if ($this->session->flashdata('eerror') <> '') : ?>
        <div class="alert alert-danger alert-dismissible" role="alert">
            <div class="icon"><span class="mdi mdi-close-circle-o"></span></div>
            <div class="message">
                <button class="close" type="button" data-dismiss="alert" aria-label="Close">
                    <span class="mdi mdi-close" aria-hidden="true"></span></button>
                <strong>Error!</strong> <?php $this->session->flashdata('eerror'); ?>
            </div>
        </div>
    <?php endif ?>

    <form class="form-horizontal group-border-dashed" method="post" action="<?= $action; ?>" data-parsley-namespace="data-parsley-">
        <input type="hidden" id="orden" name="orden" value="<?= $orden; ?>" readonly>
        <div class="form-group row">
            <div class="col-sm-7">
                <h3 class="wizard-title">Información de envío <?php if($orden > 0){ echo "Guía #".$orden; }  ?></h3>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <label><b>Cobrar en destino</b></label>
                <div class="col-12 col-sm-8 col-lg-6 pt-1">
                    <div class="switch-button switch-button-success">
                        <input type="checkbox"  name="cobro_destino" id="swt5" <?= $checked ?> value="1"><span>
                            <label for="swt5"></label></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <h4>De</h4>
                <div id="coincideCliOrig"></div>
                <input type="hidden" type="text" id="origen_id" name="origen_id" value="<?= $origen_id ?>" readonly>

                <label><b>Nombres</b></label>
                <input class="form-control" type="text" onblur="buscarClientesOrigen();" id="origen_nombres" name="origen_nombres" value="<?= $origen_nombres ?>" placeholder="Ingrese los nombres" required>

                <label><b>Apellidos</b></label>
                <input class="form-control" type="text" onblur="buscarClientesOrigen();" id="origen_apellidos" name="origen_apellidos" value="<?= $origen_apellidos ?>" placeholder="Ingrese los apellidos" required>

                <input type="hidden" name="origen_pais" id="origen_pais" value="<?= $origen_pais; ?>" readonly>

                <label><b>Departamento</b></label> <img width="16px" src="<?= base_url('public/') ?>img/gt.ico" alt="GT">
                <select class="select2" name="origen_ciudad" id="origen_ciudad" required>
                    <option value="0">Seleccione una opción..</option>
                    <?php foreach ($departamentos as $key) : ?>
                        <?php if ($origen_ciudad == $key->ciudad) : ?>
                            <option value="<?= $key->ciudad; ?>" selected><?= $key->nombre; ?></option>
                        <?php else : ?>
                            <option value="<?= $key->ciudad; ?>"><?= $key->nombre; ?></option>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </select>

                <label><b>Dirección</b></label>
                <input class="form-control" type="text" id="origen_direccion" name="origen_direccion" value="<?= $origen_direccion; ?>" placeholder="Ingrese dirección principal" required>

                <label><b>Teléfono</b></label>
                <input class="form-control" type="text" id="origen_telefono" name="origen_telefono" value="<?= $origen_telefono; ?>" placeholder="Ingrese teléfono principal" required>

                <label><b>Correo</b></label>
                <input class="form-control" type="email" id="origen_correo" name="origen_correo" value="<?= $origen_correo; ?>" parsley-type="email" placeholder="Ingrese el e-mail">

                <?php if ($orden > 0 && $datosOrden->cliente_origen > 0) : ?>
                    <br>
                    <a href="#" onclick="ampliarDatosCliente(<?= $origen_id ?>);">Ampliar datos del cliente.</a>
                <?php endif; ?>
            </div>
            <div class="col-lg-6">
                <h4>A</h4>
                <div id="coincideCliDest"></div>
                <input type="hidden" type="text" id="destino_id" name="destino_id" value="<?= $destino_id; ?>" readonly>

                <label><b>Nombres</b></label>
                <input class="form-control" type="text" onblur="buscarClientesDestino();" id="destino_nombres" name="destino_nombres" value="<?= $destino_nombres; ?>" placeholder="Ingrese los nombres" required>

                <label><b>Apellidos</b></label>
                <input class="form-control" type="text" onblur="buscarClientesDestino();" id="destino_apellidos" name="destino_apellidos" value="<?= $destino_apellidos; ?>" placeholder="Ingrese los apellidos" required>

                <input type="hidden" name="destino_pais" id="destino_pais" value="<?= $destino_pais; ?>" readonly>

                <label><b>Ciudad</b></label> <img width="16px" src="<?= base_url('public/') ?>img/us.ico" alt="USA">
                <select class="select2" name="destino_ciudad" id="destino_ciudad" required>
                    <option value="0">Seleccione una opción..</option>
                    <?php foreach ($ciudades as $key) : ?>
                        <?php if ($destino_ciudad == $key->ciudad) : ?>
                            <option value="<?= $key->ciudad; ?>" selected><?= $key->nombre; ?></option>
                        <?php else : ?>
                            <option value="<?= $key->ciudad; ?>"><?= $key->nombre; ?></option>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </select>

                <label><b>Dirección</b></label>
                <input class="form-control" type="text" id="destino_direccion" name="destino_direccion" value="<?= $destino_direccion; ?>" placeholder="Ingrese dirección principal" required>

                <label><b>Teléfono</b></label>
                <input class="form-control" type="text" id="destino_telefono" name="destino_telefono" value="<?= $destino_telefono; ?>" placeholder="Ingrese teléfono principal" required>

                <label><b>Correo</b></label>
                <input class="form-control" type="email" id="destino_correo" name="destino_correo" value="<?= $destino_correo; ?>" parsley-type="email" placeholder="Ingrese el e-mail">

                <?php if ($orden > 0 && $datosOrden->cliente_destino > 0) : ?>
                    <br>
                    <a href="#" onclick="ampliarDatosCliente(<?= $destino_id; ?>);">Ampliar datos del cliente.</a>
                <?php endif; ?>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-12">
                <button type="button" onclick="location.href='<?= site_url('neg/orden'); ?>'" class="btn btn-secondary btn-space">Cancelar</button>
                <button class="btn btn-primary btn-space">Guardar</button>
                <?php if ($orden > 0 && $datosOrden->cliente_origen > 0 && $datosOrden->cliente_destino > 0) : ?>
                    <button class="btn btn-warning btn-space wizard-next" data-wizard="#wizard1"> Siguiente paso <span class="icon-class mdi mdi-arrow-right"></span></button>
                <?php endif; ?>
            </div>
        </div>

    </form>
</div>

<div class="modal fade colored-header colored-header-primary" id="form-cliente-origen" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div id="contenidoModal"></div>
        </div>
    </div>
</div>

<script type="text/javascript">

    function ampliarDatosCliente(cliente) {
        $.ajax({
            url: '<?= site_url('neg/orden/ampliarDatosCliente') ?>',
            method: "POST",
            data: {
                cliente: cliente
            },
            success: function(data) {
                $('#contenidoModal').html(data);
                $('#form-cliente-origen').modal('show');
            }

        });
    }

    function buscarClientesOrigen() {
        let nombres = $('#origen_nombres').val();
        let apellidos = $('#origen_apellidos').val();
        <?php if (!$existOrigen) : ?>
            $.ajax({
                url: '<?= site_url('neg/clientes/buscarClientesPorNombre') ?>',
                method: "POST",
                data: {
                    nombres: nombres,
                    apellidos: apellidos,
                    pais: "GT"
                },
                success: function(data) {
                    if (data > 0 && (nombres != "" || apellidos != "")) {
                        $("#coincideCliOrig").html('<button type="button" onclick="cargarCliente(1);" class="btn btn-rounded btn-space btn-secondary">' + data + ' Concidencias</button>');
                    } else {
                        $("#coincideCliOrig").html("");
                    }
                }

            });
        <?php endif; ?>
    }

    function buscarClientesDestino() {
        let nombres = $('#destino_nombres').val();
        let apellidos = $('#destino_apellidos').val();
        <?php if (!$existDestino) : ?>
            $.ajax({
                url: '<?= site_url('neg/clientes/buscarClientesPorNombre') ?>',
                method: "POST",
                data: {
                    nombres: nombres,
                    apellidos: apellidos,
                    pais: "US"
                },
                success: function(data) {
                    if (data > 0 && (nombres != "" || apellidos != "")) {
                        $("#coincideCliDest").html('<button type="button" onclick="cargarCliente(2);" class="btn btn-rounded btn-space btn-secondary">' + data + ' Concidencias</button>');
                    } else {
                        $("#coincideCliDest").html("");
                    }
                }

            });
        <?php endif; ?>
    }

    function cargarCliente(tipo) {

        let nombres, apellidos;
        let orden = "<?= $orden; ?>";

        if (tipo == 1) {
            nombres = $('#origen_nombres').val();
            apellidos = $('#origen_apellidos').val();
        } else {
            nombres = $('#destino_nombres').val();
            apellidos = $('#destino_apellidos').val();
        }

        $.ajax({
            url: '<?= site_url('neg/clientes/cargarCliente') ?>',
            method: "POST",
            data: {
                tipo: tipo,
                nombres: nombres,
                apellidos: apellidos,
                orden: orden
            },
            success: function(data) {
                $('#contenidoModal').html(data);
                $('#form-cliente-origen').modal('show');
            }

        });

    }
</script>