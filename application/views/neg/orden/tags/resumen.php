<?php
$de = $this->Model_cliente->getCliente($datosOrden->cliente_origen);
$para = $this->Model_cliente->getCliente($datosOrden->cliente_destino);

$detalle = $this->Model_orden->getDetalleOrden($orden);
?>
<div class="main-content container-fluid" id="orden_print">
    <div class="row">
        <div class="col-lg-12">
            <div class="invoice">
                <div class="row invoice-header">
                    <div class="col-sm-7">
                        <svg id="barcode"></svg>
                    </div>
                    <div class="col-sm-5 invoice-order"><span class="invoice-id">Pedido #<?= $orden; ?></span><span class="incoice-date"><?= fecha_larga(); ?></span></div>
                </div>
                <div class="row invoice-data">
                    <div class="col-sm-5 invoice-person"><span class="name"><?= $de->nombres . ' ' . $de->apellidos; ?></span><span><?= $de->telefono; ?></span><span><?= $de->direccion1; ?></span><span><?= $de->ciudad_nombre . ', ' . $de->pais_nombre; ?></span></div>
                    <div class="col-sm-2 invoice-payment-direction"><i class="icon mdi mdi-chevron-right"></i></div>
                    <div class="col-sm-5 invoice-person"><span class="name"><?= $para->nombres . ' ' . $para->apellidos; ?></span><span><?= $para->telefono; ?></span><span><?= $para->direccion1; ?></span><span><?= $para->ciudad_nombre . ', ' . $para->pais_nombre; ?></span></div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <table class="invoice-details">
                            <tr>
                                <th style="width:60%;">Descripción</th>
                                <th class="hours" style="width:17%;">Valor Declarado</th>
                                <th class="amount" style="width:15%;">Tarifa</th>
                            </tr>
                            <?php
                            $total = 0;

                            foreach ($detalle as $row) :
                                $moneda = '$';
                                if ($row->moneda != 0) {
                                    $moneda = 'Q';
                                }

                                $moneda_tarifa = '$';
                                if ($row->moneda_tarifa != 0) {
                                    $moneda_tarifa = 'Q';
                                }

                                $tarifa_final = 0;
                                if($row->peso_volumetrico > $row->peso){
                                    $tarifa_final = $row->tarifa * $row->peso_volumetrico;
                                }else{
                                    $tarifa_final = $row->tarifa * $row->peso;
                                }

                                $total = $total + $tarifa_final;
                            ?>
                                <tr>
                                    <td class="description">
                                        <?= $row->nombre_tipo_producto . ' - ' . $row->nombre_producto; ?>
                                    </td>
                                    <td class="amount">
                                        <?= $moneda . ' ' . number_format($row->valor_declarado, 2); ?>
                                    </td>
                                    <td class="amount">
                                        <?= $moneda_tarifa . ' ' . number_format($tarifa_final, 2); ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            <tr>
                                <td></td>
                                <td class="summary total">Total</td>
                                <td class="amount total-value"><?= '$' . number_format($total, 2); ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 invoice-payment-method">
                        <span class="title">Información de Pago</span>
                        <span>Esta orden se paga en destino.</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 invoice-message"><span class="title">Gracias por contar con nosotros.</span>
                        <p>Esperamos quede satisfecho con nuestros servicios, nuestro principal activo es usted.</p>
                    </div>
                </div>
                <div class="row invoice-company-info">
                    <!--div class="col-md-6 col-lg-2 logo"><svg id="barcode"></svg></div>-->
                    <div class="col-md-6 col-lg-4 summary"><span class="title">Seleste Express</span>
                        <p>Tus encomiendas en buenas manos. </p>
                    </div>
                    <div class="col-sm-6 col-lg-3 phone">
                        <ul class="list-unstyled">
                            <li>+502-5898 6317</li>
                            <li>+502-7843 4275</li>
                        </ul>
                    </div>
                    <div class="col-sm-6 col-lg-3 email">
                        <ul class="list-unstyled">
                            <li>celestegua2@gmail.com</li>
                        </ul>
                    </div>
                </div>
                <div class="row invoice-footer">
                    <div class="col-lg-12">
                        <a href="<?= site_url('neg/Reportes/printOrden/'.$orden); ?>" target="_blank">
                            <button type="button" class="btn btn-lg btn-space btn-secondary">Impresión corta</button>
                        </a>
                        <a href="<?= site_url('neg/Reportes/printOrden/'.$orden.'/1'); ?>" target="_blank">
                            <button type="button" class="btn btn-lg btn-space btn-secondary">Impresión Completa</button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?= base_url('public/') ?>js/JsBarcode.all.min.js" type="text/javascript"></script>

<script type="text/javascript">
    JsBarcode("#barcode", "<?= $datosOrden->origen.'-'.$datosOrden->anio.'-'.$orden; ?>");
</script>