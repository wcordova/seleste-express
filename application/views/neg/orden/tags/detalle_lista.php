<?php
$detalle = $this->Model_orden->getDetalleOrden($orden);
?>
<table class="table table-striped table-hover table-condensed">
    <thead>
        <tr>
            <th>Producto</th>
            <th>Tarifa x lb</th>
            <th>Valor Declarado</th>
            <th>Medidas</th>
            <th>Peso Volumetrico</th>
            <th>Peso</th>
            <th>Tarifa final</th>
            <th></th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($detalle as $row) :
            $moneda = '$';
            if ($row->moneda != 0) {
                $moneda = 'Q';
            }

            $moneda_tarifa = '$';
            if ($row->moneda_tarifa != 0) {
                $moneda_tarifa = 'Q';
            }

            $tarifa_final = 0;
            if($row->peso_volumetrico > $row->peso){
                $tarifa_final = $row->tarifa * $row->peso_volumetrico;
            }else{
                $tarifa_final = $row->tarifa * $row->peso;
            }
        ?>
            <tr>
                <td class="cell-detail">
                    <?= $row->nombre_tipo_producto . ' - ' . $row->nombre_producto; ?>
                    <span class="cell-detail-description"><?= $row->descripcion ?></span>
                </td>
                <td><?= $moneda_tarifa . ' ' . $row->tarifa; ?></td>
                <td><?= $moneda . ' ' . $row->valor_declarado; ?></td>
                <td><?= $row->alto . ' x ' . $row->ancho . ' x ' . $row->largo . ' ' . '(' . $row->unidad_medida . ')'; ?></td>
                <td><?= $row->peso_volumetrico . ' ' . $row->medida_peso_volumetrico; ?></td>
                <td><?= $row->peso . ' ' . $row->unidad_peso; ?></td>
                <td><?= $moneda.' '.$tarifa_final; ?></td>
                <td class="actions">
                    <a class="icon" href="<?= site_url("neg/orden/form/{$row->orden}/{$row->detalle}"); ?>">
                        <i class="mdi mdi-edit"></i>
                    </a>
                </td>
                <td class="actions">
                    <span style="color: red;" class="mdi mdi-delete" onclick="eliminarItemDetalle(<?= $row->detalle; ?>, <?= $row->orden; ?>);"></span>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<script type="text/javascript">
    <?php if ($this->session->flashdata('noti_descrip') <> '') : ?>
        notificacion("Notificación", '<?= $this->session->flashdata('noti_descrip'); ?>', "primary");
    <?php endif; ?>

    function eliminarItemDetalle(detalle, orden) {
        $.ajax({
            url: '<?= site_url('neg/Orden/eliminarItemDetalle') ?>',
            method: "POST",
            data: {
                orden: orden,
                detalle: detalle
            },
            success: function(data) {
                $('#listaDetalleOrden').html(data);
                $.ajax({
                    url: '<?= site_url('neg/Orden/actualizarResumen') ?>',
                    method: "POST",
                    data: {
                        orden: orden
                    },
                    success: function(data) {
                        $('#ordenResumen').html(data);
                    }
                });
            }
        });


    }
</script>