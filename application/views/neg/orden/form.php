<div class="main-content container-fluid">
    <div class="row wizard-row">
        <div class="col-md-12 fuelux">
            <div class="block-wizard">
                <div class="wizard wizard-ux" id="wizard1">
                    <div class="steps-container">
                        <ul class="steps">
                            <li class="<?= $detalle == 0 ? 'active' : 'complete' ?>" data-step="1">Paso 1<span class="chevron"></span></li>
                            <li class=" <?= $detalle > 0 ? 'active' : '' ?>" data-step="2">Paso 2<span class="chevron"></span></li>
                            <li data-step="3">Paso 3<span class="chevron"></span></li>
                        </ul>
                    </div>

                    <div class="step-content">
                        <div class="step-pane <?= $detalle == 0 ? 'active' : '' ?>" data-step="1">
                            <?php $this->load->view('neg/orden/tags/principal', $datosOrden); ?>
                        </div>
                        <?php if ($datosOrden) :  ?>
                            <div class="step-pane <?= $detalle > 0 ? 'active' : '' ?>" data-step="2">
                                <?php $this->load->view('neg/orden/tags/detalle', $this->data); ?>
                            </div>
                            <div class="step-pane" data-step="3">
                                <?php $this->load->view('neg/orden/tags/adicional', $datosOrden); ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?= base_url('public/') ?>assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function() {
        //-initialize the javascript
        App.wizard();
    });
</script>