<!DOCTYPE html>
<html>

<head>
	<style>
		body {
			width: 100%;
			margin: 0;
		}

		* {
			font-size: 12px;
			font-family: 'DejaVu Sans', serif;
			margin: 0;
			padding: 0;
		}

		h1 {
			font-size: 18px;
		}

		.ticket {
			margin: 8px;
		}

		td,
		th,
		tr,
		table {
			border-top: 1px solid black;
			border-collapse: collapse;
			margin: 0 auto;
		}

		td.tarifa {
			text-align: right;
			font-size: 11px;
		}

		td.nombre_producto {
			font-size: 11px;
			width: 10px;
		}

		td.valor_declarado {
			text-align: center;
		}

		th {
			text-align: center;
		}

		.centrado {
			text-align: center;
			align-content: center;
		}

		.derecha {
			text-align: right;
		}

		img {
			width: 100%;
		}
	</style>
</head>

<body>
	<div class="ticket">
		<div class="centrado">
			<h1>Guia #<?= $orden; ?></h1>
			<h2><?= date("d/m/Y H:i:s") ?></h2>
			<img src="data:image/png;base64,<?= $barCode ?>">
		</div>
		<div id="de">
			<b class="name"><?= $de->nombres . ' ' . $de->apellidos; ?></b> <br>
			<span><?= $de->telefono; ?></span> <br>
			<span><?= $de->direccion1; ?></span> <br>
			<span><?= $de->ciudad_nombre . ', ' . $de->pais_nombre; ?></span>
		</div>
		<div id="para" class="derecha">
			<b class="name"><?= $para->nombres . ' ' . $para->apellidos; ?></b> <br>
			<span><?= $para->telefono; ?></span> <br>
			<span><?= $para->direccion1; ?></span> <br>
			<span><?= $para->ciudad_nombre . ', ' . $para->pais_nombre; ?></span>
		</div>
		<br>
		<?php if ($tipo > 0) : ?>
			<table>
				<thead>
					<tr class="centrado">
						<th class="nombre_producto">Des.</th>
						<th class="valor_declarado">Valor</th>
						<th class="tarifa">Tarifa</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$total = 0;
					foreach ($detalle as $row) :
						$moneda = '$';
						if ($row->moneda != 0) {
							$moneda = 'Q';
						}

						$moneda_tarifa = '$';
						if ($row->moneda_tarifa != 0) {
							$moneda_tarifa = 'Q';
						}

						$tarifa_final = 0;
						if ($row->peso_volumetrico > $row->peso) {
							$tarifa_final = $row->tarifa * $row->peso_volumetrico;
						} else {
							$tarifa_final = $row->tarifa * $row->peso;
						}

						$total = $total + $tarifa_final;

					?>
						<tr>
							<td class="nombre_producto">
								<?= $row->nombre_tipo_producto . ' - ' . $row->nombre_producto; ?>
							</td>
							<td class="valor_declarado">
								<?= $moneda . $row->valor_declarado; ?>
							</td>
							<td class="tarifa">
								<?= $moneda_tarifa . number_format($tarifa_final, 2); ?>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
				<tr>
					<td class="nombre_producto"></td>
					<td class="valor_declarado">
						<strong>TOTAL</strong>
					</td>
					<td class="tarifa">
						$<?= number_format($total, 2) ?>
					</td>
				</tr>
			</table>
		<?php endif; ?>
		<div>
			<p>
				<b class="title">Información de Pago</b><br>
				<?php if ($datosOrden->cobro_origen_destino == 1) : ?>
					<span>Esta guía se paga en destino.</span>
				<?php else : ?>
					<span>Esta guía se pagó en origen.</span>
				<?php endif; ?>
			</p>
			<div class="centrado">
				- - - - - - - - - - - - - - - - - - - -
			</div>
			<div>
				<div class="centrado">
					<b>GRACIAS POR CONTAR CON NOSOTROS.</b>
				</div>
				<span>Esperamos quede satisfecho con nuestros servicios, nuestro principal activo es usted.</span>
			</div>
			<br>
			<div class="centrado">
				<h1>Seleste Express</h1>
				<p>Tus encomiendas en buenas manos. </p>
			</div>
			<br>
			<p class="contacto">
				<span>+502-5898 6317</span> <br>
				<span>+502-7843 4275</span> <br>
				<span>celestegua2@gmail.com</span>
			</p>
		</div>
	</div>
</body>

</html>