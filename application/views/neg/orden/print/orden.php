<html>

<head>
    <title>Seleste Express <?= $orden; ?></title>

    <!-- Cargar CSS -->
    <?php $this->load->view('layout/header'); ?>

</head>

<?php
?>

<body>
    <div class="main-content container-fluid" id="orden_print">
        <div class="row">
            <div class="col-lg-12">
                <div class="invoice">
                    <div class="row invoice-header">
                        <div class="col-sm-7">
                            <svg id="barcode"></svg>
                        </div>
                        <div class="col-sm-5 invoice-order"><span class="invoice-id">Pedido #<?= $orden; ?></span><span class="incoice-date"><?= fecha_larga(); ?></span></div>
                    </div>
                    <div class="row invoice-data">
                        <div class="col-sm-5 invoice-person"><span class="name"><?= $de->nombres . ' ' . $de->apellidos; ?></span><span><?= $de->telefono; ?></span><span><?= $de->direccion1; ?></span><span><?= $de->ciudad_nombre . ', ' . $de->pais_nombre; ?></span></div>
                        <div class="col-sm-2 invoice-payment-direction"><?php echo '<img src="data:image/png;base64,' . $barCode . '">' ?></div>
                        <div class="col-sm-5 invoice-person"></div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <table class="invoice-details">
                                <tr>
                                    <th style="width:60%;">Descripción</th>
                                    <th class="hours" style="width:17%;">Valor Declarado</th>
                                    <th class="amount" style="width:15%;">Tarifa</th>
                                </tr>
                                <?php
                                $total = 0;

                                foreach ($detalle as $row) :
                                    $moneda = '$';
                                    if ($row->moneda != 0) {
                                        $moneda = 'Q';
                                    }

                                    $moneda_tarifa = '$';
                                    if ($row->moneda_tarifa != 0) {
                                        $moneda_tarifa = 'Q';
                                    }

                                    $total = $total + $row->tarifa;
                                ?>
                                    <tr>
                                        <td class="description">
                                            <?= $row->nombre_tipo_producto . ' - ' . $row->nombre_producto; ?>
                                        </td>
                                        <td class="amount">
                                            <?= $moneda . ' ' . $row->valor_declarado; ?>
                                        </td>
                                        <td class="amount">
                                            <?= $moneda_tarifa . ' ' . $row->tarifa; ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                <tr>
                                    <td></td>
                                    <td class="summary total">Total</td>
                                    <td class="amount total-value"><?= '$' . $total; ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 invoice-payment-method">
                            <span class="title">Información de Pago</span>
                            <span>Esta orden se paga en destino.</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 invoice-message"><span class="title">Gracias por contar con nosotros.</span>
                            <p>Esperamos quede satisfecho con nuestros servicios, nuestro principal activo es usted.</p>
                        </div>
                    </div>
                    <div class="row invoice-company-info">
                        <div class="col-md-6 col-lg-4 summary"><span class="title">Seleste Express</span>
                            <p>Tus encomiendas en buenas manos. </p>
                        </div>
                        <div class="col-sm-6 col-lg-3 phone">
                            <ul class="list-unstyled">
                                <li>+502-5898 6317</li>
                                <li>+502-7843 4275</li>
                            </ul>
                        </div>
                        <div class="col-sm-6 col-lg-3 email">
                            <ul class="list-unstyled">
                                <li>celestegua2@gmail.com</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
