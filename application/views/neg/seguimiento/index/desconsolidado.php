<?php 
$countRecibidos = '<span class="badge badge-primary float-right">'.count($this->Model_consolidado->getOrdenesList(array(1))).'</span>';
$countDesconsolidar = '<span class="badge badge-warning float-right">'.count($this->Model_consolidado->getOrdenesList(array(2,9))).'</span>';

?>
<div class="card">
    <div class="card-header">Tabla de Consolidados (Proceso de Desconsolidación)</div>

    <div class="tab-container">
        <ul class="nav nav-tabs nav-tabs-success" role="tablist">
            <li class="nav-item"><a class="nav-link active" href="#home1" data-toggle="tab" role="tab">En ruta <?= $countRecibidos; ?></a></li>
            <li class="nav-item"><a class="nav-link" href="#profile1" data-toggle="tab" role="tab">Desconsolidar <?= $countDesconsolidar; ?></a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="home1" role="tabpanel">
                <?php $this->load->view('neg/seguimiento/consolidado/tabRecibido', $recibidos); ?>
            </div>
            <div class="tab-pane" id="profile1" role="tabpanel">
                <?php $this->load->view('neg/seguimiento/consolidado/tabDesconsolidado', $desconsolidar); ?>
            </div>
        </div>
    </div>
</div>



<script type="text/javascript">
    function cambiarEstadoConsolidado(consolidado, estado, regresa = 0) {
        let listConsolidados = <?= json_encode($recibidos); ?>;
        let resultado = listConsolidados.find(element => element.consolidado == consolidado);

        if (resultado != undefined) {
            $.ajax({
                url: '<?= site_url('neg/Seguimiento/cambiarEstadoConsolidado') ?>',
                method: "POST",
                data: {
                    consolidado: consolidado,
                    estado: estado,
                    regresa: regresa
                },
                success: function(data) {
                    window.location.href = '<?= site_url('neg/Seguimiento/desconsolidar') ?>';
                }
            });
        } else {
            alert("Consolidado no encontrado");
        }

    }

    function desconsolidarGuia(consolidado) {
        let listConsolidados = <?= json_encode($desconsolidar); ?>;
        let resultado = listConsolidados.find(element => element.consolidado == consolidado);
        if (resultado != undefined) {
            window.location.href = '<?= site_url('neg/Seguimiento/validarConsolidado/') ?>' + consolidado;
        } else {
            alert("Consolidado no encontrado");
        }
    }
</script>