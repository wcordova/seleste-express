<div class="card card-table">
    <div class="card-header">
        Recibidos
        <div class="row">
            <div class="col-xs-2">
                <input type="text" class="form-control" placeholder="Finalizar" onchange="cambiarEstadoOrden(this.value, 4);">
            </div>
        </div>
    </div>
    <div class="card-body">
        <table class="table table-striped table-hover table-fw-widget table3">
            <thead>
                <tr>
                    <th>No. Orden</th>
                    <th>Origen</th>
                    <th>De</th>
                    <th>Destino</th>
                    <th>A</th>
                    <th>Estado</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($recibido as $row) :
                    if ($row->cliente_origen > 0) {
                        $cliente_origen = $this->Model_cliente->getData($row->cliente_origen);
                        $ciudad_origen = $this->Model_ciudades->getData($row->ciudad_origen);
                    }
                    if ($row->cliente_destino > 0) {
                        $cliente_destino = $this->Model_cliente->getData($row->cliente_destino);
                        $ciudad_destino = $this->Model_ciudades->getData($row->ciudad_destino);
                    }

                ?>
                    <tr>
                        <td style="width:8%"><?= $row->orden; ?></td>
                        <td>
                            <?php if ($row->cliente_origen > 0 && $row->ciudad_origen > 0) {
                                echo $row->origen . ' - ' . $ciudad_origen->nombre;
                            } ?>
                        </td>
                        <td>
                            <?php if ($row->cliente_origen > 0) {
                                echo $cliente_origen->nombres . ' ' . $cliente_origen->apellidos;
                            } ?>
                        </td>
                        <td>
                            <?php if ($row->cliente_destino > 0 && $row->ciudad_destino > 0) {
                                echo $row->destino . ' - ' . $ciudad_destino->nombre;
                            } ?>
                        </td>
                        <td>
                            <?php if ($row->cliente_destino > 0) {
                                echo $cliente_destino->nombres . ' ' . $cliente_destino->apellidos;
                            } ?>

                        </td>
                        <td>
                            <?= ($row->estado == 3) ? 'Recibido' : 'Finalizado'; ?>
                        </td>
                        <td class="actions">
                            <button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown">Acciones <span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                            <div class="dropdown-menu" role="menu">
                                <a class="dropdown-item" onclick="cambiarEstadoOrden('<?= $row->orden; ?>', 2, 1);" href="#">Regresar</a>
                                <a class="dropdown-item" onclick="cambiarEstadoOrden('<?= $row->orden; ?>', 4);" href="#">Finalizar</a>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
<script type="text/javascript">
    function cambiarEstadoOrden(orden, estado, regresa = 0) {
        let listOrdenes = <?= json_encode($recibido); ?>;
        let resultado = listOrdenes.find(element => element.orden == orden);
        if (resultado != undefined) {
            $.ajax({
                url: '<?= site_url('neg/Seguimiento/cambiarEstadoOrden') ?>',
                method: "POST",
                data: {
                    orden: orden,
                    estado: estado,
                    regresa: regresa
                },
                success: function(data) {
                    window.location.href = '<?= site_url('neg/Seguimiento/recibidas') ?>';
                }
            });
        } else {
            alert("Orden no encontrada");
        }

    }
</script>