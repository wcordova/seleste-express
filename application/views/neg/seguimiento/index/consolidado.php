<?php
$countInit = '<span class="badge badge-default float-right">' . count($this->Model_consolidado->getOrdenesList(array(0))) . '</span>';
$countEnviados = '<span class="badge badge-primary float-right">' . count($this->Model_consolidado->getOrdenesList(array(1))) . '</span>';
?>
<div class="card">
    <div class="card-header">Tabla de Consolidados</div>
    Consolidados <button class="btn btn-space btn-primary mdi mdi-collection-plus" onclick="location.href='<?= site_url('neg/seguimiento/createConsolidado'); ?>'"> Crear Consolidado</button>

    <div class="tab-container">
        <ul class="nav nav-tabs nav-tabs-success" role="tablist">
            <li class="nav-item"><a class="nav-link active" href="#home1" data-toggle="tab" role="tab">Iniciados <?= $countInit; ?></a></li>
            <li class="nav-item"><a class="nav-link" href="#profile1" data-toggle="tab" role="tab">Enviados <?= $countEnviados; ?></a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="home1" role="tabpanel">
                <?php $this->load->view('neg/seguimiento/consolidado/tabInit', $iniciados); ?>
            </div>
            <div class="tab-pane" id="profile1" role="tabpanel">
                <?php $this->load->view('neg/seguimiento/consolidado/tabEnviado', $enviados); ?>
            </div>
        </div>
    </div>
</div>