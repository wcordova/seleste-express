<div class="card card-table">
    <div class="card-header">
    Tabla de Consolidados Creados <button class="btn btn-space btn-primary mdi mdi-collection-plus" onclick="location.href='<?= site_url('neg/seguimiento/createConsolidado'); ?>'"> Crear Consolidado</button> 
    <div></div>
        <div class="row">
            <div class="col-xs-2">
                <input type="text" class="form-control" placeholder="Enviar" onchange="cambiarEstadoConsolidado(this.value, 1, 0);">
            </div>
        </div>
    </div>
    <div class="card-body">
        <table class="table table-striped table-hover table-fw-widget table3">
            <thead>
                <tr>
                    <th>No. Consolidado</th>
                    <th>Año</th>
                    <th>No Referencia</th>
                    <th>Cantidad de guias</th>
                    <th>Fecha Creación</th>
                    <th>Estado</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($iniciados as $row) : ?>
                    <tr>
                        <td><a href="<?= site_url('neg/seguimiento/seguimientoConsolidado/' . $row->consolidado); ?>"><?= $row->consolidado; ?></a></td>
                        <td><?= $row->anio; ?></td>
                        <td><?= $row->no_referencia; ?></td>
                        <td><?= $row->cantidad_guias; ?></td>
                        <td><?= $row->fecha_creacion; ?></td>
                        <td><?= ($row->estado == 0) ? 'Iniciado' : 'Enviado'; ?></td>
                        <td class="actions">
                            <button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown">Acciones <span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                            <div class="dropdown-menu" role="menu">
                                <a class="dropdown-item" href="<?= site_url('neg/seguimiento/seguimientoConsolidado/' . $row->consolidado); ?>">Editar</a>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
<script type="text/javascript">
    function cambiarEstadoConsolidado(consolidado, estado, regresa = 0, tipo) {
        let listConsolidados = <?= json_encode($iniciados); ?>;
        let resultado = listConsolidados.find(element => element.consolidado == consolidado);

        if (resultado != undefined) {
            $.ajax({
                url: '<?= site_url('neg/Seguimiento/cambiarEstadoConsolidado') ?>',
                method: "POST",
                data: {
                    consolidado: consolidado,
                    estado: estado,
                    regresa: regresa
                },
                success: function(data) {
                    window.location.href = '<?= site_url('neg/Seguimiento/consolidadoInicializado') ?>';
                }
            });
        } else {
            alert("Consolidado no encontrado");
        }

    }
</script>