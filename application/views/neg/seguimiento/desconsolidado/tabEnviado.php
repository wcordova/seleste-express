<div class="card card-table">
    <div class="card-header">
    Tabla de Consolidados Enviados
        <div class="row">
            <div class="col-xs-2">
                <input type="text" class="form-control" placeholder="Regresar" onchange="cambiarEstadoConsolidado(this.value, 0, 1);">
            </div>
        </div>
    </div>
    <div class="card-body">
        <table class="table table-striped table-hover table-fw-widget table3">
            <thead>
                <tr>
                    <th>No. Consolidado</th>
                    <th>Año</th>
                    <th>No Referencia</th>
                    <th>Cantidad de guias</th>
                    <th>Fecha Creación</th>
                    <th>Estado</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($enviados as $row) : ?>
                    <tr>
                        <td><?= $row->consolidado; ?></td>
                        <td><?= $row->anio; ?></td>
                        <td><?= $row->no_referencia; ?></td>
                        <td><?= $row->cantidad_guias; ?></td>
                        <td><?= $row->fecha_creacion; ?></td>
                        <td><?= ($row->estado == 0) ? 'Iniciado' : 'Enviado'; ?></td>
                        <td class="actions">
                            <button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown">Acciones <span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                            <div class="dropdown-menu" role="menu">
                                <a class="dropdown-item" onclick="cambiarEstadoConsolidado(<?= $row->consolidado; ?>, 0, 1, 1);" href="#">Regresar</a>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
<script type="text/javascript">
    function cambiarEstadoConsolidado(consolidado, estado, regresa = 0) {
        let listConsolidados = <?= json_encode($enviados); ?>;
        let resultado = listConsolidados.find(element => element.consolidado == consolidado);

        if (resultado != undefined) {
            $.ajax({
                url: '<?= site_url('neg/Seguimiento/cambiarEstadoConsolidado') ?>',
                method: "POST",
                data: {
                    consolidado: consolidado,
                    estado: estado,
                    regresa: regresa
                },
                success: function(data) {
                    window.location.href = '<?= site_url('neg/Seguimiento/consolidadoEnviado') ?>';
                }
            });
        } else {
            alert("Consolidado no encontrado");
        }

    }
</script>