<div class="card card-table">
    <div class="card-header">
    Tabla de Consolidados en Ruta
        <div class="row">
            <div class="col-xs-2">
                <input type="text" class="form-control" placeholder="Recibir" onchange="cambiarEstadoConsolidado(this.value, 2);">
            </div>
        </div>
    </div>
    <div class="card-body">
        <table class="table table-striped table-hover table-fw-widget table3">
            <thead>
                <tr>
                    <th>No. Consolidado</th>
                    <th>Año</th>
                    <th>No Referencia</th>
                    <th>Cantidad de guias</th>
                    <th>Fecha Creación</th>
                    <th>Estado</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($recibidos as $row) : ?>
                    <tr>
                        <td><?= $row->consolidado; ?></td>
                        <td><?= $row->anio; ?></td>
                        <td><?= $row->no_referencia; ?></td>
                        <td><?= $row->cantidad_guias; ?></td>
                        <td><?= $row->fecha_creacion; ?></td>
                        <td><?= ($row->estado == 1) ? 'En ruta' : 'Recibido'; ?></td>
                        <td class="actions">
                            <button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown">Acciones <span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                            <div class="dropdown-menu" role="menu">
                                <a class="dropdown-item" href="<?= site_url('neg/seguimiento/seguimientoConsolidado/' . $row->consolidado); ?>"> Recibir</a>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
<script type="text/javascript">
    function cambiarEstadoConsolidado(consolidado, estado, regresa = 0) {
        let listConsolidados = <?= json_encode($recibidos); ?>;
        let resultado = listConsolidados.find(element => element.consolidado == consolidado);

        if (resultado != undefined) {
            $.ajax({
                url: '<?= site_url('neg/Seguimiento/cambiarEstadoConsolidado') ?>',
                method: "POST",
                data: {
                    consolidado: consolidado,
                    estado: estado,
                    regresa: regresa
                },
                success: function(data) {
                    window.location.href = '<?= site_url('neg/Seguimiento/consolidadoRecibido') ?>';
                }
            });
        } else {
            alert("Consolidado no encontrado");
        }

    }
</script>