<div class="card card-table">
    <div class="card-header">
    Tabla de Consolidados Recibidos
        <div class="row">
            <div class="col-xs-2">
                <input type="text" class="form-control" placeholder="Desconsolidar" onchange="desconsolidarGuia(this.value);">
            </div>
        </div>
    </div>
    <div class="card-body">
        <table class="table table-striped table-hover table-fw-widget table3">
            <thead>
                <tr>
                    <th>No. Consolidado</th>
                    <th>Año</th>
                    <th>No Referencia</th>
                    <th>Cantidad de guias</th>
                    <th>Fecha Creación</th>
                    <th>Estado</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($desconsolidar as $row) : ?>
                    <tr>
                        <td><?= $row->consolidado; ?></td>
                        <td><?= $row->anio; ?></td>
                        <td><?= $row->no_referencia; ?></td>
                        <td><?= $row->cantidad_guias; ?></td>
                        <td><?= $row->fecha_creacion; ?></td>
                        <td><?= ($row->estado == 2) ? 'Recibido' : 'Cerrado'; ?></td>
                        <td class="actions">
                            <button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown">Acciones <span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                            <div class="dropdown-menu" role="menu">
                                <?php
                                if ($row->estado == 2) {
                                ?>
                                    <a class="dropdown-item" href="<?= site_url('neg/seguimiento/validarConsolidado/' . $row->consolidado); ?>"> Desconsolidar</a>
                                    <a class="dropdown-item" href="<?= site_url('neg/seguimiento/validarConsolidado/' . $row->consolidado); ?>"> Cerrar</a>
                                <?php
                                } else {
                                ?>
                                    <a class="dropdown-item" href="<?= site_url('neg/seguimiento/validarConsolidado/' . $row->consolidado); ?>"> Ver</a>
                                <?php
                                }
                                ?>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
<script type="text/javascript">
    function desconsolidarGuia(consolidado) {
        let listConsolidados = <?= json_encode($desconsolidar); ?>;
        let resultado = listConsolidados.find(element => element.consolidado == consolidado);
        if (resultado != undefined) {
            window.location.href = '<?= site_url('neg/Seguimiento/validarConsolidado/') ?>' + consolidado;
        } else {
            alert("Consolidado no encontrado");
        }
    }
</script>