<?php

$listPendientes = $this->Model_orden->getOrdenesList(array(1));
$listAsignadas = $this->Model_consolidado->getOrdenesAsignadas($consolidado);

?>
<div class="card card-border-color card-border-color-primary">
    <div class="card-header card-header-divider"><?= $titulo; ?></div>

    <div class="card-body">
        <form action="<?= $action; ?>" method="post" data-parsley-validate novalidate>
            <input hidden id="consolidado" name="consolidado" value="<?= $consolidado ?>" readonly>

            <div class="row">
                <div class="col-lg-6">
                    <label><b>Embarque:</b></label>
                    <select class="select2" name="embarque" id="embarque">
                        <option value="0">Seleccione una opción..</option>
                        <?php foreach ($embarque as $key) : ?>
                            <?php if($key->embarque == $resultados->embarque): ?>
                                <option value="<?= $key->embarque; ?>" selected><?= "E-".$key->embarque; ?></option>
                            <?php else: ?>
                                <option value="<?= $key->embarque; ?>"><?= "E-".$key->embarque; ?></option>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>

            <label for="">No. Referencia:</label>
            <input readonly type="text" id="referencia" name="referencia" class="form-control" value="<?= $resultados->no_referencia; ?>">

            <label for="">Observaciones:</label>
            <textarea class="form-control" rows="3" id="observaciones" name="observaciones"><?= $resultados->observaciones; ?></textarea>

            <div class="row pt-3">
                <p class="text-right">
                    <button class="btn btn-space btn-primary" type="submit">Guardar</button>
                    <button onclick="location.href='<?= site_url('neg/seguimiento/consolidadoInicializado'); ?>'" type="button" class="btn btn-space btn-secondary">Cancel</button>
                </p>
            </div>
        </form>
    </div>

    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-lg-6">
                <div class="card-header">
                    Guias completadas(disponibles): <?= count($listPendientes); ?>
                </div>
                <div class="card-body">
                    <table class="table table-sm table-striped">
                        <tbody>
                            <?php foreach ($listPendientes as $row) :
                                if ($row->cliente_origen > 0) {
                                    $cliente_origen = $this->Model_cliente->getData($row->cliente_origen);
                                    $ciudad_origen = $this->Model_ciudades->getData($row->ciudad_origen);
                                }
                                if ($row->cliente_destino > 0) {
                                    $cliente_destino = $this->Model_cliente->getData($row->cliente_destino);
                                    $ciudad_destino = $this->Model_ciudades->getData($row->ciudad_destino);
                                }

                            ?>
                                <tr>
                                    <td>
                                        <?= $row->origen . '-' . $row->anio . '-' . $row->orden; ?>
                                    </td>
                                    <td>
                                        <?php if ($row->cliente_origen > 0 && $row->ciudad_origen > 0) {
                                            echo $row->origen . ' - ' . $ciudad_origen->nombre;
                                        } ?>
                                    </td>
                                    <td>
                                        <?php if ($row->cliente_origen > 0) {
                                            echo $cliente_origen->nombres . ' ' . $cliente_origen->apellidos;
                                        } ?>
                                    </td>
                                    <td>
                                        <span style="color: green;" class="mdi mdi-long-arrow-right" onclick="asignarGuia(<?= $consolidado; ?>, <?= $row->orden; ?>);"></span>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="card-header">
                    Guias asignadas: <?= count($listAsignadas); ?>
                </div>
                <div class="card-body">
                    <table class="table table-sm table-striped">
                        <tbody>
                            <?php foreach ($listAsignadas as $row) :
                                if ($row->cliente_origen > 0) {
                                    $cliente_origen = $this->Model_cliente->getData($row->cliente_origen);
                                    $ciudad_origen = $this->Model_ciudades->getData($row->ciudad_origen);
                                }
                                if ($row->cliente_destino > 0) {
                                    $cliente_destino = $this->Model_cliente->getData($row->cliente_destino);
                                    $ciudad_destino = $this->Model_ciudades->getData($row->ciudad_destino);
                                }

                                $guia_madre = "";
                                $tipo_icon = "mdi mdi-favorite-outline";
                                if ($row->guia_madre == 1) {
                                    $guia_madre = "color: red;";
                                    $tipo_icon = "mdi mdi-favorite";
                                }

                            ?>
                                <tr>
                                    <td>
                                        <span style="color: red;" class="mdi mdi-long-arrow-left" onclick="quitarGuia(<?= $consolidado; ?>, <?= $row->orden; ?>);"></span>
                                    </td>
                                    <td>
                                        <span style="<?= $guia_madre; ?>" class="<?= $tipo_icon ?>" onclick="asignarGuiaMadre(<?= $consolidado; ?>, <?= $row->orden; ?>, '<?= $row->origen . '-' . $row->anio . '-' . $row->orden; ?>');"></span>
                                    <td>
                                        <?php if ($row->cliente_origen > 0 && $row->ciudad_origen > 0) {
                                            echo $row->origen . ' - ' . $ciudad_origen->nombre;
                                        } ?>
                                    </td>
                                    <td>
                                        <?php if ($row->cliente_origen > 0) {
                                            echo $cliente_origen->nombres . ' ' . $cliente_origen->apellidos;
                                        } ?>
                                    </td>
                                    <td>
                                        <?= $row->origen . '-' . $row->anio . '-' . $row->orden; ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function asignarGuia(consolidado, orden) {
        $.ajax({
            url: '<?= site_url('neg/Seguimiento/asignarGuiaConsolidado') ?>',
            method: "POST",
            data: {
                consolidado: consolidado,
                orden: orden
            },
            success: function(data) {
                window.location.href = '<?= site_url('neg/Seguimiento/seguimientoConsolidado/') ?>' + consolidado;
            }
        });
    }

    function quitarGuia(consolidado, orden) {
        $.ajax({
            url: '<?= site_url('neg/Seguimiento/quitarGuiaConsolidado') ?>',
            method: "POST",
            data: {
                consolidado: consolidado,
                orden: orden
            },
            success: function(data) {
                window.location.href = '<?= site_url('neg/Seguimiento/seguimientoConsolidado/') ?>' + consolidado;
            }
        });
    }

    function asignarGuiaMadre(consolidado, orden, guia) {

        $.ajax({
            url: '<?= site_url('neg/Seguimiento/asignarGuiaMadre') ?>',
            method: "POST",
            data: {
                consolidado: consolidado,
                orden: orden,
                guia: guia
            },
            success: function(data) {
                window.location.href = '<?= site_url('neg/Seguimiento/seguimientoConsolidado/') ?>' + consolidado;
            }
        });
    }
</script>