<?php

$listOrdenConso = $this->Model_consolidado->getOrdenesByConsolidado($consolidado, 0);
$listOrdenDesco = $this->Model_consolidado->getOrdenesByConsolidado($consolidado, 1);

?>
<div class="card card-border-color card-border-color-primary">
    <div class="card-header card-header-divider"><?= $titulo; ?></div>

    <div class="card-body">
        <input hidden id="consolidado" name="consolidado" value="<?= $consolidado ?>" readonly>

        <label for="">No. Referencia:</label>
        <input readonly type="text" id="referencia" name="referencia" class="form-control" value="<?= $resultados->no_referencia; ?>">

        <label for="">Observaciones:</label>
        <textarea readonly class="form-control" rows="3" id="observaciones" name="observaciones"><?= $resultados->observaciones; ?></textarea>

        <div class="row pt-3">
            <p class="text-right">
                <button onclick="location.href='<?= site_url('neg/seguimiento/consolidadoDesconsolidar'); ?>'" type="button" class="btn btn-space btn-secondary">Regresar</button>
                <?php if (count($listOrdenConso) == 0 && $resultados->estado != 9) { ?>
                    <button onclick="cerrarConsolidado();" type="button" class="btn btn-space btn-primary">Cerrar Consolidado</button>
                <?php } ?>
            </p>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-xs-2">
                <input type="text" class="form-control" placeholder="Desconsolidar" onchange="desconsolidarOrdenString(this.value);">
            </div>
        </div>
    </div>

    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-lg-6">
                <div class="card-header">
                    Guias sin validar (consolidadas): <?= count($listOrdenConso); ?>
                </div>
                <div class="card-body">
                    <table class="table table-sm table-striped">
                        <tbody>
                            <?php foreach ($listOrdenConso as $row) :
                                if ($row->cliente_origen > 0) {
                                    $cliente_origen = $this->Model_cliente->getData($row->cliente_origen);
                                    $ciudad_origen = $this->Model_ciudades->getData($row->ciudad_origen);
                                }
                                if ($row->cliente_destino > 0) {
                                    $cliente_destino = $this->Model_cliente->getData($row->cliente_destino);
                                    $ciudad_destino = $this->Model_ciudades->getData($row->ciudad_destino);
                                }

                                $guia_madre = "";
                                $tipo_icon = "";
                                if ($row->guia_madre == 1) {
                                    $guia_madre = "color: red;";
                                    $tipo_icon = "mdi mdi-favorite";
                                }

                            ?>
                                <tr>
                                    <td>
                                        <?= $row->origen . '-' . $row->anio . '-' . $row->orden; ?>
                                    </td>
                                    <td>
                                        <?php if ($row->cliente_origen > 0 && $row->ciudad_origen > 0) {
                                            echo $row->origen . ' - ' . $ciudad_origen->nombre;
                                        } ?>
                                    </td>
                                    <td>
                                        <?php if ($row->cliente_origen > 0) {
                                            echo $cliente_origen->nombres . ' ' . $cliente_origen->apellidos;
                                        } ?>
                                    </td>
                                    <td>
                                        <span style="<?= $guia_madre; ?>" class="<?= $tipo_icon ?>"></span>
                                    </td>
                                    <td>
                                        <span style="color: green;" class="mdi mdi-check" onclick="desconsolidarOrden(<?= $consolidado; ?>, <?= $row->orden; ?>);"></span>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="card-header">
                    Guias validadas (desconsolidadas): <?= count($listOrdenDesco); ?>
                </div>
                <div class="card-body">
                    <table class="table table-sm table-striped">
                        <tbody>
                            <?php foreach ($listOrdenDesco as $row) :
                                if ($row->cliente_origen > 0) {
                                    $cliente_origen = $this->Model_cliente->getData($row->cliente_origen);
                                    $ciudad_origen = $this->Model_ciudades->getData($row->ciudad_origen);
                                }
                                if ($row->cliente_destino > 0) {
                                    $cliente_destino = $this->Model_cliente->getData($row->cliente_destino);
                                    $ciudad_destino = $this->Model_ciudades->getData($row->ciudad_destino);
                                }

                                $guia_madre = "";
                                $tipo_icon = "";
                                if ($row->guia_madre == 1) {
                                    $guia_madre = "color: red;";
                                    $tipo_icon = "mdi mdi-favorite";
                                }

                            ?>
                                <tr>
                                    <td>
                                        <span style="color: green;" class="mdi mdi-check-all"></span>
                                    </td>
                                    <td>
                                        <span style="<?= $guia_madre; ?>" class="<?= $tipo_icon ?>"></span>
                                    </td>
                                    <td>
                                        <?php if ($row->cliente_origen > 0 && $row->ciudad_origen > 0) {
                                            echo $row->origen . ' - ' . $ciudad_origen->nombre;
                                        } ?>
                                    </td>
                                    <td>
                                        <?php if ($row->cliente_origen > 0) {
                                            echo $cliente_origen->nombres . ' ' . $cliente_origen->apellidos;
                                        } ?>
                                    </td>
                                    <td>
                                        <?= $row->origen . '-' . $row->anio . '-' . $row->orden; ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function desconsolidarOrdenString(referencia) {
        let ref = referencia.split("-");
        desconsolidarOrden('<?= $consolidado ?>', ref[2]);
    }

    function desconsolidarOrden(consolidado, orden) {
        $.ajax({
            url: '<?= site_url('neg/Seguimiento/desconsolidarOrden') ?>',
            method: "POST",
            data: {
                consolidado: consolidado,
                orden: orden
            },
            success: function(data) {
                window.location.href = '<?= site_url('neg/Seguimiento/validarConsolidado/') ?>' + consolidado;
            }
        });
    }

    function cerrarConsolidado() {
        let consolidado = '<?= $consolidado ?>';
        $.ajax({
            url: '<?= site_url('neg/Seguimiento/cerrarConsolidado') ?>',
            method: "POST",
            data: {
                consolidado: consolidado
            },
            success: function(data) {
                window.location.href = '<?= site_url('neg/Seguimiento/validarConsolidado/') ?>' + consolidado;
            }
        });
    }
</script>