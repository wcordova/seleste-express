<div class="card card-table">
    <div class="card-header">Listado de Embarques <button class="btn btn-space btn-primary mdi mdi-collection-plus" onclick="location.href='<?= site_url('neg/embarque/form'); ?>'"> Nuevo Embarque</button>
    </div>
    <div class="card-body">
        <table class="table table-striped table-hover table-fw-widget table3">
            <thead>
                <tr>
                    <th>Codigo</th>
                    <th>Descripción</th>
                    <th>Cantidad de consolidados</th>
                    <th>Cantidad de ordenes</th>
                    <th>Usuario creación</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($resultados as $row) :
                    $usuario = $this->Model_usuarios->getData($row->user_add);
                ?>
                    <tr class="">
                        <td><?= "E" . $row->embarque; ?></td>
                        <td><?= $row->descripcion; ?></td>
                        <td><?= $row->cant_consolidados; ?></td>
                        <td><?= $row->cant_ordenes; ?></td>
                        <td><?= $usuario->name; ?></td>
                        <td class="actions">
                            <a class="icon" href="<?= site_url('neg/embarque/form/' . $row->embarque); ?>"><i class="mdi mdi-edit"></i></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>   
</div>
<script type="text/javascript">

</script>