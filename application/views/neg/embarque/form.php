<?php
$embarque = "";
$codigo = "";
$descripcion = "";
$consolidados = 0;
$ordenes = 0;
$envio_domesticos = 0.00;
$cobro_origen = 0.00;
$cobro_destino = 0.00;


if (!empty($resultado)) {
    $embarque = $resultado->embarque;
    $codigo = "E-" . $resultado->embarque;
    $descripcion = $resultado->descripcion;
    $consolidados = $resultado->cant_consolidados;
    $ordenes = $resultado->cant_ordenes;
    $envio_domesticos = $resultado->envio_domesticos;
    $cobro_origen = $resultado->cobro_origen;
    $cobro_destino = $resultado->cobro_destino;
}

?>

<div class="card card-border-color card-border-color-primary">
    <div class="card-header card-header-divider"><?= $titulo; ?><span class="card-subtitle"><?= $subTitulo; ?></span></div>

    <?php if ($this->session->flashdata('eok') <> '') : ?>
        <div class="alert alert-success alert-dismissible" role="alert">
            <div class="icon"><span class="mdi mdi-check"></span></div>
            <div class="message">
                <button class="close" type="button" data-dismiss="alert" aria-label="Close">
                    <span class="mdi mdi-close" aria-hidden="true"></span></button>
                <strong>Ok!</strong> <?php echo $this->session->flashdata('eok'); ?>
            </div>
        </div>
    <?php endif ?>

    <?php if ($this->session->flashdata('eerror') <> '') : ?>
        <div class="alert alert-danger alert-dismissible" role="alert">
            <div class="icon"><span class="mdi mdi-close-circle-o"></span></div>
            <div class="message">
                <button class="close" type="button" data-dismiss="alert" aria-label="Close">
                    <span class="mdi mdi-close" aria-hidden="true"></span></button>
                <strong>Error!</strong> <?php $this->session->flashdata('eerror'); ?>
            </div>
        </div>
    <?php endif ?>

    <div class="card-body">
        <form action="<?= $action; ?>" method="post" data-parsley-validate novalidate>
            <input hidden id="embarque" name="embarque" value="<?= $embarque ?>">

            <div class="form-group pt-2">
                <label for="nombre">Código Embarque</label>
                <input class="form-control" id="codigo" name="codigo" type="text" value="<?= $codigo; ?>" placeholder="Ingrese el codigo del embarque" readonly>
            </div>
            <div class="form-group pt-2">
                <label for="nombre">Descripción</label>
                <textarea class="form-control" id="descripcion" name="descripcion" type="text" placeholder="Ingrese la descripcion"><?= $descripcion; ?></textarea>
            </div>


            <div class="row pt-3">
                <p class="text-right">
                    <button class="btn btn-space btn-primary" type="submit">Guardar</button>
                    <button onclick="location.href='<?= site_url('neg/embarque'); ?>'" type="button" class="btn btn-space btn-secondary">Cancel</button>
                </p>
            </div>
        </form>
        <?php if (!empty($resultado)) : ?>
            <div class="container">
                <div class="row">
                    <div class="col-5">
                        <ul class="list-group">
                            <li class="list-group-item">Consolidados: <span class="float-right badge badge-pill badge-primary"><?= $consolidados; ?></span></li>
                            <li class="list-group-item">Ordenes: <span class="float-right badge badge-pill badge-primary"><?= $ordenes; ?></span></li>
                            <li class="list-group-item">Envio domesticos: <span class="float-right badge badge-pill badge-primary"><?= $envio_domesticos; ?></span></li>
                            <li class="list-group-item">Cobro en origen: <span class="float-right badge badge-pill badge-primary"><?= $cobro_origen; ?></span></li>
                            <li class="list-group-item">Cobro en destino: <span class="float-right badge badge-pill badge-primary"><?= $cobro_destino; ?></span></li>
                        </ul>
                    </div>
                </div>

            </div>
        <?php endif; ?>

    </div>
</div>
