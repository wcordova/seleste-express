<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="<?= base_url('public/') ?>assets/img/logo-s.png">
  <title>Seleste Express | Redirect</title>
  <link rel="stylesheet" type="text/css" href="<?= base_url('public/') ?>assets/lib/perfect-scrollbar/css/perfect-scrollbar.css"/>
  <link rel="stylesheet" type="text/css" href="<?= base_url('public/') ?>assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/>
  <link rel="stylesheet" href="<?= base_url('public/') ?>assets/css/app.css" type="text/css"/>
</head>
<body class="be-splash-screen">
  <div class="be-wrapper be-login">
    <div class="be-content">
      <div class="main-content container-fluid">
        <div class="splash-container">
          <div class="card card-border-color card-border-color-primary">
            <div class="card-header"><img class="logo-img" src="<?= base_url('public/') ?>assets/img/cargando.gif" alt="logo" width="350" height="350"></div>
            <div class="card-body" style="text-align: center;">
              <h2>Seleste Express</h2>
              <h3>Su sesión ha finalizado. Espere mientras lo rederigimos a la ventana de Acceso...</h3>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script src="<?= base_url('public/') ?>assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
  <script src="<?= base_url('public/') ?>assets/lib/perfect-scrollbar/js/perfect-scrollbar.min.js" type="text/javascript"></script>
  <script src="<?= base_url('public/') ?>assets/lib/bootstrap/dist/js/bootstrap.bundle.min.js" type="text/javascript"></script>
  <script src="<?= base_url('public/') ?>assets/js/app.js" type="text/javascript"></script>

  <!-- libreria de encriptacion -->
  <script src="<?= base_url('public/js/encriptB64.js') ?> "></script>
  <script type="text/javascript">
    setTimeout(function () {
      $.ajax({
        success: function (data) {
          parent.window.location = "<?= site_url('Login') ?>";
        }
      });
    }, 4000);
  </script>
</body>
</html>