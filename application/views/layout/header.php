<link rel="stylesheet" type="text/css" href="<?= base_url('public/') ?>assets/lib/perfect-scrollbar/css/perfect-scrollbar.css"/>
<link rel="stylesheet" type="text/css" href="<?= base_url('public/') ?>assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/>
<link rel="stylesheet" type="text/css" href="<?= base_url('public/') ?>assets/lib/datatables/datatables.net-bs4/css/dataTables.bootstrap4.css"/>
<link rel="stylesheet" type="text/css" href="<?= base_url('public/') ?>assets/lib/datatables/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css"/>
<link rel="stylesheet" type="text/css" href="<?= base_url('public/') ?>assets/lib/mprogress/css/mprogress.min.css"/>
<link rel="stylesheet" type="text/css" href="<?= base_url('public/') ?>assets/lib/select2/css/select2.min.css"/>
<link rel="stylesheet" type="text/css" href="<?= base_url('public/') ?>assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css"/>
<link rel="stylesheet" type="text/css" href="<?= base_url('public/') ?>assets/lib/bootstrap-slider/css/bootstrap-slider.min.css"/>
<link rel="stylesheet" type="text/css" href="<?= base_url('public/') ?>assets/lib/jquery.gritter/css/jquery.gritter.css"/>
<link rel="stylesheet" href="<?= base_url('public/') ?>assets/css/app.css" type="text/css"/>