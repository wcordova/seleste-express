<!DOCTYPE html>
<html lang="es">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <link rel="shortcut icon" href="<?= base_url('public/') ?>assets/img/logo-fav.png">
        <title>Seleste Express | <?= $titulo; ?></title>

        <!-- Cargar CSS -->
        <?php $this->load->view('layout/header'); ?>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
                <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.3/html5shiv.js"></script>
                <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
            <![endif]-->
    </head>

    <body>
        <div class="be-wrapper be-fixed-sidebar">
            <?php $this->load->view('layout/head'); ?>
            <?php $this->load->view('layout/menu'); ?>
            <div class="be-content">
                <?php if (!empty($view)): ?>
                    <?php $this->load->view($view); ?>
                <?php endif ?>
            </div>
            <?php $this->load->view('layout/sidebar'); ?>
        </div>
        <!-- Cargar JS -->
        <?php $this->load->view('layout/footer'); ?>

    </body>

</html>