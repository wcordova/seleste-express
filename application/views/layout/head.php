<?php $nombre = $this->session->userdata('nombre'); ?>
<nav class="navbar navbar-expand fixed-top be-top-header">
    <div class="container-fluid">
        <div class="be-navbar-header"><a class="navbar-brand" href="#"></a>
        </div>
        <div class="be-right-navbar">
            <ul class="nav navbar-nav float-right be-user-nav">
                <li class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" role="button" aria-expanded="false"><img src="<?= base_url('public/') ?>assets/img/avatar.png" alt="Avatar"><span class="user-name">Nombre Usuario</span></a>
                    <div class="dropdown-menu" role="menu">
                        <div class="user-info">
                            <div class="user-name"><?php echo $nombre; ?></div>
                            <div class="user-position online">Activo</div>
                        </div><a class="dropdown-item" href="#"><span class="icon mdi mdi-face"></span>Cuenta</a><a class="dropdown-item" href="#"><span class="icon mdi mdi-settings"></span>Configuración</a><a class="dropdown-item" href="<?= site_url('home/init/salir') ?>"><span class="icon mdi mdi-power"></span>Salir</a>
                    </div>
                </li>
            </ul>
            <div class="page-title"><span>Dashboard</span></div>
            <ul class="nav navbar-nav float-right be-icons-nav">
                <li class="nav-item dropdown"><a class="nav-link be-toggle-right-sidebar" href="#" role="button" aria-expanded="false"><span class="icon mdi mdi-settings"></span></a></li>
                <li class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" role="button" aria-expanded="false"><span class="icon mdi mdi-notifications"></span><span class="indicator"></span></a>
                    <ul class="dropdown-menu be-notifications">
                        <li>
                            <div class="title">Notificaciones<span class="badge badge-pill">0</span></div>
                            <div class="list">
                                <div class="be-scroller-notifications">
                                    <div class="content">
                                        <ul>
                                            <!--
                                            <li class="notification notification-unread"><a href="#">
                                                    <div class="image"><img src="assets/img/avatar2.png" alt="Avatar"></div>
                                                    <div class="notification-info">
                                                        <div class="text"><span class="user-name">Jessica Caruso</span> accepted your invitation to join the team.</div><span class="date">2 min ago</span>
                                                    </div>
                                                </a>
                                            </li>   
                                            -->                            
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="footer"> <a href="#">No tiene Notificaciones</a></div>
                        </li>
                    </ul>
                </li>
                <li class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" role="button" aria-expanded="false"><span class="icon mdi mdi-apps"></span></a>
                    <ul class="dropdown-menu be-connections">
                        <li>
                            <div class="list">
                                <div class="content">
                                    <div class="row">
                                        <div class="col"><a class="connection-item" href="#"><img src="<?= base_url('public/') ?>assets/img/github.png" alt="Github"><span>GitHub</span></a></div>
                                        <div class="col"><a class="connection-item" href="#"><img src="<?= base_url('public/') ?>assets/img/bitbucket.png" alt="Bitbucket"><span>Bitbucket</span></a></div>
                                    </div>                                   
                                </div>
                            </div>
                            <div class="footer"> <a href="#">Sitios de Interes</a></div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>