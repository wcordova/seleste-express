<script src="<?= base_url('public/') ?>assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>assets/lib/perfect-scrollbar/js/perfect-scrollbar.min.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>assets/lib/bootstrap/dist/js/bootstrap.bundle.min.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>assets/js/app.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>assets/lib/mprogress/js/mprogress.min.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>assets/lib/prettify/prettify.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>assets/js/app-ajax-loader.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>assets/lib/parsley/parsley.min.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>assets/lib/datatables/datatables.net/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>assets/lib/datatables/datatables.net-bs4/js/dataTables.bootstrap4.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>assets/lib/datatables/datatables.net-buttons/js/dataTables.buttons.min.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>assets/lib/datatables/datatables.net-buttons/js/buttons.flash.min.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>assets/lib/datatables/jszip/jszip.min.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>assets/lib/datatables/pdfmake/pdfmake.min.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>assets/lib/datatables/pdfmake/vfs_fonts.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>assets/lib/datatables/datatables.net-buttons/js/buttons.colVis.min.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>assets/lib/datatables/datatables.net-buttons/js/buttons.print.min.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>assets/lib/datatables/datatables.net-buttons/js/buttons.html5.min.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>assets/lib/datatables/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>assets/lib/datatables/datatables.net-responsive/js/dataTables.responsive.min.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>assets/lib/datatables/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>assets/js/app-tables-datatables.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>assets/lib/select2/js/select2.min.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>assets/lib/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>assets/js/app-form-elements.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>assets/lib/bootstrap-slider/bootstrap-slider.min.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>assets/lib/jquery.maskedinput/jquery.maskedinput.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>assets/js/app-form-masks.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>assets/lib/fuelux/js/wizard.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>assets/js/app-form-wizard.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>assets/lib/jquery.gritter/js/jquery.gritter.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>assets/js/app-ui-notifications.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function() {
        //-initialize the javascript
        App.init();
        App.formElements();
        App.ajaxLoader();
        $('form').parsley();
        //-Runs prettify
        prettyPrint();
        //dataTables
        App.dataTables();

        $("#tablePaises").dataTable({
            "order": [
                [2, "asc"]
            ]
        });

        App.masks();

    });

    function notificacion(titulo, texto, clase) {
        $.gritter.add({
            title: titulo,
            text: texto,
            class_name: clase
        });
    }
</script>

<?php if ($this->session->flashdata('noti_titulo') <> '' && $this->session->flashdata('noti_titulo') <> '') : ?>
    <script type="text/javascript">
        $(document).ready(function() {
            $.gritter.add({
                title: '<?= $this->session->flashdata('noti_titulo'); ?>',
                text: '<?= $this->session->flashdata('noti_descrip'); ?>',
                class_name: '<?= $this->session->flashdata('noti_alert'); ?>'
            });

        });
    </script>
<?php endif; ?>