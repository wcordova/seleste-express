<?php
    $pendientes = count($this->Model_orden->getOrdenesList(array(1)));
    $colorPendientes = "primary";
    if($pendientes > 0) {
        $colorPendientes = "danger";
    }
    $countPendientes = '<span class="badge badge-'.$colorPendientes.' float-right">'.$pendientes.'</span>';
    $countEnviadas = '<span class="badge badge-primary float-right">'.count($this->Model_orden->getOrdenesList(array(2))).'</span>';
    $countRecibidas = '<span class="badge badge-success float-right">'.count($this->Model_orden->getOrdenesList(array(3))).'</span>';
    
    $countDesconsolidados = '<span class="badge badge-default float-right">'.count($this->Model_consolidado->getOrdenesList(array(2))).'</span>';
    $countConsolidados = '<span class="badge badge-default float-right">'.count($this->Model_consolidado->getOrdenesList(array(0))).'</span>';
    $countConsolidadosEnviados = '<span class="badge badge-primary float-right">'.count($this->Model_consolidado->getOrdenesList(array(1))).'</span>';
?>

<div class="be-left-sidebar">
    <div class="left-sidebar-wrapper"><a class="left-sidebar-toggle" href="#">Dashboard</a>
        <div class="left-sidebar-spacer">
            <div class="left-sidebar-scroll">
                <div class="left-sidebar-content">
                    <ul class="sidebar-elements">
                        <li class="divider">Menu </li>
                        <li class="<?php if(!empty($menu) && $menu == 'init'){ echo "active"; } ?>"><a href="<?= site_url('home/init/principal') ?>"><i class="icon mdi mdi-home"></i><span>Inicio</span></a>
                        </li>
                        <?php if($this->session->userdata('rol') == 1): ?>
                            <li class="parent <?php if(!empty($menu) && $menu == 'core'){ echo "active"; } ?>"><a href="#"><i class="icon mdi mdi-face"></i><span>Core</span></a>
                                <ul class="sub-menu">
                                    <li class="<?php if(!empty($sub_menu) && $sub_menu == 'per'){ echo "active"; } ?>"><a href="<?= site_url('core/personas') ?>">Personas</a>
                                    </li>  
                                    <li class="<?php if(!empty($sub_menu) && $sub_menu == 'user'){ echo "active"; } ?>"><a href="<?= site_url('core/usuarios') ?>">Usuarios</a>
                                    </li>  
                                    <li class="<?php if(!empty($sub_menu) && $sub_menu == 'rol'){ echo "active"; } ?>"><a href="<?= site_url('core/roles') ?>">Roles</a>
                                    </li>  
                                    <li class="<?php if(!empty($sub_menu) && $sub_menu == 'cliente') echo "active"; ?>"><a href="<?= site_url('core/cliente') ?>">Clientes</a>
                                    </li>                       
                                </ul>
                            </li>
                            <li class="parent <?php if(!empty($menu) && $menu == 'cla'){ echo "active"; } ?>"><a href="#"><i class="icon mdi mdi-assignment-o"></i><span>Clasificadores</span></a>
                                <ul class="sub-menu">
                                    <li class="<?php if(!empty($sub_menu) && $sub_menu == 'tipoProd'){ echo "active"; } ?>"><a href="<?= site_url('cla/tipo_producto') ?>">Tipo Productos</a>
                                    </li>  
                                    <li class="<?php if(!empty($sub_menu) && $sub_menu == 'producto'){ echo "active"; } ?>"><a href="<?= site_url('cla/productos') ?>">Productos</a>
                                    </li>
                                    <li class="<?php if(!empty($sub_menu) && $sub_menu == 'pais'){ echo "active"; } ?>"><a href="<?= site_url('cla/paises') ?>">Destinos</a>
                                    </li> 
                                    <li class="<?php if(!empty($sub_menu) && $sub_menu == 'tarifa'){ echo "active"; } ?>"><a href="<?= site_url('cla/tarifas') ?>">Tarifas</a>
                                    </li>                          
                                </ul>
                            </li>
                            <li class="parent <?php if(!empty($menu) && $menu == 'neg'){ echo "active"; } ?>"><a href="#"><i class="icon mdi mdi-folder"></i><span>Negocio</span></a>
                                <ul class="sub-menu">
                                    <li class="<?php if(!empty($sub_menu) && $sub_menu == 'emb'){ echo "active"; } ?>"><a href="<?=  site_url('neg/embarque') ?>"></i>Embarque</a>
                                    </li>
                                    <!--
                                    <li class="<?php if(!empty($sub_menu) && $sub_menu == 'cli'){ echo "active"; } ?>"><a href="#"></i>Clientes</a>
                                    </li>
                                    <li class="<?php if(!empty($sub_menu) && $sub_menu == 'his'){ echo "active"; } ?>"><a href="#">Historial</a>
                                    </li>
                                    <li class="<?php if(!empty($sub_menu) && $sub_menu == 'rep'){ echo "active"; } ?>"><a href="#">Reportes</a>
                                    </li>   
                                    -->                         
                                </ul>
                            </li>
                        <?php endif; ?> 
                        <li class="parent <?php if(!empty($menu) && $menu == 'ord'){ echo "active"; } ?>"><a href="#"><i class="icon mdi mdi-folder"></i><span>Guías</span></a>
                            <ul class="sub-menu">
                                <?php if($this->session->userdata('rol') == 1): ?>                             
                                    <li class="<?php if(!empty($sub_menu) && $sub_menu == 'orden'){ echo "active"; } ?>"><a href="<?=  site_url('neg/orden') ?>"></i>Iniciadas</a>
                                    </li>                                
                                    <li class="<?php if(!empty($sub_menu) && $sub_menu == 'seg'){ echo "active"; } ?>"><a href="<?=  site_url('neg/seguimiento/pendientes') ?>"><?= $countPendientes ?>Pendientes</a>
                                    </li>   
                                <?php endif; ?>  
                                <?php if($this->session->userdata('rol') == 4 || $this->session->userdata('rol') == 1): ?>                             
                                    <li class="<?php if(!empty($sub_menu) && $sub_menu == 'env'){ echo "active"; } ?>"><a href="<?=  site_url('neg/seguimiento/enviadas') ?>"><?= $countEnviadas ?>En ruta</a>
                                    </li>
                                    <li class="<?php if(!empty($sub_menu) && $sub_menu == 'rec'){ echo "active"; } ?>"><a href="<?=  site_url('neg/seguimiento/recibidas') ?>"><?= $countRecibidas ?>Recibidas</a>
                                    </li>   
                                <?php endif; ?>                       
                            </ul>
                        </li>
                        <li class="parent <?php if(!empty($menu) && $menu == 'con'){ echo "active"; } ?>"><a href="#"><i class="icon mdi mdi-folder-star"></i><span>Consolidados</span></a>
                            <ul class="sub-menu">
                            <?php if($this->session->userdata('rol') == 1): ?>
                                <li class="<?php if(!empty($sub_menu) && $sub_menu == 'init'){ echo "active"; } ?>"><a href="<?=  site_url('neg/seguimiento/consolidadoInicializado') ?>"><?= $countConsolidados ?>Inicializados</a>
                                </li> 
                                <li class="<?php if(!empty($sub_menu) && $sub_menu == 'sen'){ echo "active"; } ?>"><a href="<?=  site_url('neg/seguimiento/consolidadoEnviado') ?>"><?= $countConsolidadosEnviados ?>Enviados</a>
                                </li> 
                            <?php endif; ?>    
                            <?php if($this->session->userdata('rol') == 4 || $this->session->userdata('rol') == 1): ?>
                                <li class="<?php if(!empty($sub_menu) && $sub_menu == 'desc'){ echo "active"; } ?>"><a href="<?=  site_url('neg/seguimiento/consolidadoRecibido') ?>"><?= $countConsolidadosEnviados ?>En ruta</a>
                                </li> 
                                <li class="<?php if(!empty($sub_menu) && $sub_menu == 'desf'){ echo "active"; } ?>"><a href="<?=  site_url('neg/seguimiento/consolidadoDesconsolidar') ?>"><?= $countDesconsolidados ?>Desconsolidar</a>
                                </li>  
                            <?php endif; ?>                             
                            </ul>
                        </li>

                        <li><a href="#"><i class="icon mdi mdi-book"></i><span>Documentation</span></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="progress-widget">
            <div class="progress-data"><span class="progress-value">30%</span><span class="name">Construcción..</span></div>
            <div class="progress">
                <div class="progress-bar progress-bar-primary" style="width: 30%;"></div>
            </div>
        </div>
    </div>
</div>