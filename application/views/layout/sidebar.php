<nav class="be-right-sidebar">
    <div class="sb-content">
        <div class="tab-navigation">
            <ul class="nav nav-tabs nav-justified" role="tablist">
                <li class="nav-item" role="presentation"><a class="nav-link" href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">Actividad</a></li>
                <li class="nav-item" role="presentation"><a class="nav-link" href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab">Hoy</a></li>
                <li class="nav-item" role="presentation"><a class="nav-link active" href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab">Configuraciones</a></li>
            </ul>
        </div>
        <div class="tab-panel">
            <div class="tab-content">
                <div class="tab-pane tab-chat" id="tab1" role="tabpanel">
                    <div class="chat-contacts">
                        <div class="chat-sections">
                            <div class="be-scroller-chat">
                                <div class="content">
                                    <h2>Actividad Reciente</h2>
                                    <div class="contact-list contact-list-recent">
                                                          
                                    </div>
                                    <h2>Historial Bitacora</h2>
                                    <div class="contact-list">
                                                         
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>  
                </div>
                <div class="tab-pane tab-todo" id="tab2" role="tabpanel">
                    <div class="todo-container">
                        <div class="todo-wrapper">
                            <div class="be-scroller-todo">
                                <div class="todo-content"><span class="category-title">Hoy</span>
                                    <ul class="todo-list">
                                        <li>
                                            <label class="custom-checkbox custom-control custom-control-sm"><span class="delete mdi mdi-delete"></span>
                                                <input class="custom-control-input" type="checkbox" checked=""><span class="custom-control-label">En construcción..</span>
                                            </label>
                                        </li>

                                    </ul><span class="category-title">Mañana</span>
                                    <ul class="todo-list">
                                        <li>
                                            <label class="custom-checkbox custom-control custom-control-sm"><span class="delete mdi mdi-delete"></span>
                                                <input class="custom-control-input" type="checkbox"><span class="custom-control-label">En construcción.. </span>
                                            </label>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="bottom-input">
                            <input type="text" placeholder="Create new task..." name="q"><span class="mdi mdi-plus"></span>
                        </div>
                    </div>
                </div>
                <div class="tab-pane tab-settings active" id="tab3" role="tabpanel">
                    <div class="settings-wrapper">
                        <div class="settings-wrapper">
                            <div class="be-scroller-settings"><span class="category-title">General</span>
                                <ul class="settings-list">
                                    <li>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <h4>Cambio al día</h4>
                                            </div>
                                            <div class="col-lg-6">
                                                <h4>Q. <?= $this->session->userdata('cambio'); ?></h4>
                                            </div>                              
                                        </div>
                                    </li>
                                <!--
                                </ul><span class="category-title">Otra categoria</span>
                                <ul class="settings-list">
                                    <li>
                                        <div class="switch-button switch-button-sm">
                                            <input type="checkbox" name="st4" id="st4"><span>
                                                <label for="st4"></label></span>
                                        </div><span class="name">En construcción..</span>
                                    </li>
                                </ul>
                                -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</nav>