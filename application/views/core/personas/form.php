<?php
$persona = 0;
$nombres = "";
$apellidos = "";
$cui = "";
$email = "";
$telefono = "";
$sexo = "";
$inactivo = 0;

if (!empty($resultado)) {
    $persona = $resultado->persona;
    $nombres = $resultado->nombres;
    $apellidos = $resultado->apellidos;
    $cui = $resultado->cui;
    $email = $resultado->email;
    $telefono = $resultado->telefono;
    $sexo = $resultado->sexo;
    $inactivo = $resultado->inactivo;
}

?>

<div class="card card-border-color card-border-color-primary">
    <div class="card-header card-header-divider"><?= $titulo; ?><span class="card-subtitle"><?= $subTitulo; ?></span></div>
    
    <?php if ($this->session->flashdata('eok')<>''): ?>
		<div class="alert alert-success alert-dismissible" role="alert">
            <div class="icon"><span class="mdi mdi-check"></span></div>
            <div class="message">
                <button class="close" type="button" data-dismiss="alert" aria-label="Close">
                    <span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <strong>Ok!</strong> <?php echo $this->session->flashdata('eok');?>
            </div>
        </div>
	<?php endif ?>

	<?php if ($this->session->flashdata('eerror')<>''): ?>
        <div class="alert alert-danger alert-dismissible" role="alert">
            <div class="icon"><span class="mdi mdi-close-circle-o"></span></div>
            <div class="message">
                <button class="close" type="button" data-dismiss="alert" aria-label="Close">
                    <span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <strong>Error!</strong> <?php $this->session->flashdata('eerror'); ?>
            </div>
        </div>
	<?php endif ?>
    
    <div class="card-body">
        <form action="<?= $action; ?>" method="post" data-parsley-validate novalidate>
            <input type="text" id="persona" name="persona" value="<?= $persona; ?>" hidden>
            <div class="form-group pt-2">
                <label for="nombre">Nombres</label>
                <input class="form-control" id="nombres" name="nombres" type="text" value="<?= $nombres; ?>" placeholder="Ingrese el nombre" required>
            </div>
            <div class="form-group">
                <label for="apellido">Apellidos</label>
                <input class="form-control" id="apellidos" name="apellidos" value="<?= $apellidos; ?>" type="text" placeholder="Ingrese el apellido" required>
            </div>
            <div class="form-group">
                <label for="cui">DPI/CUI/Pasaporte</label>
                <input type="text" class="form-control" id="cui" name="cui" value="<?= $cui; ?>" placeholder="Ingrese el número de identificación">
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" class="form-control" id="email" name="email" value="<?= $email; ?>" placeholder="Ingrese el correo electronico" required>
            </div>
            <div class="form-group">
                <label for="telefono">Teléfono</label>
                <input type="text" class="form-control" id="telefono" name="telefono" value="<?= $telefono; ?>" placeholder="Ingrese el número de teléfono" required>
            </div>
            <div class="form-group">
                <label for="sexo">Sexo</label>
                <div class="form-check mt-1">
                    <label class="custom-control custom-radio">
                        <input class="custom-control-input" type="radio" id="sexo" name="sexo" value="M" <?php if($sexo == 'M'){ echo "checked"; } ?>><span class="custom-control-label">Masculino</span>
                    </label>
                    <label class="custom-control custom-radio">
                        <input class="custom-control-input" type="radio" id="sexo" name="sexo" value="F" <?php if($sexo == 'F'){ echo "checked"; } ?>><span class="custom-control-label">Femenino</span>
                    </label>
                </div>
            </div>
            <?php if($persona > 0): ?>
                <div class="form-group">
                    <label for="sexo">Estado</label>
                    <div class="form-check mt-1">
                        <label class="custom-control custom-radio">
                            <input class="custom-control-input" type="radio" id="inactivo" name="inactivo" value="0" <?php if($inactivo == '0'){ echo "checked"; } ?>><span class="custom-control-label">Activo</span>
                        </label>
                        <label class="custom-control custom-radio">
                            <input class="custom-control-input" type="radio" id="inactivo" name="inactivo" value="1" <?php if($inactivo == '1'){ echo "checked"; } ?>><span class="custom-control-label">Inactivo</span>
                        </label>
                    </div>
                </div>
            <?php endif; ?>
            <div class="row pt-3">
                    <p class="text-right">
                        <button class="btn btn-space btn-primary" type="submit">Guardar</button>
                        <button onclick="location.href='<?= site_url('core/personas'); ?>'" type="button" class="btn btn-space btn-secondary">Cancel</button>
                    </p>
            </div>
        </form>
    </div>
</div>