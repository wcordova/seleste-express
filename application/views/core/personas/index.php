<div class="card card-table">
    <div class="card-header">Listado de Personas <button class="btn btn-space btn-primary mdi mdi-collection-plus" onclick="location.href='<?= site_url('core/personas/form'); ?>'"> Agregar</button>
    </div>
    <div class="card-body">
        <table class="table table-striped table-hover table-fw-widget table3">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Nombre</th>
                    <th>Email</th>
                    <th>Teléfono</th>
                    <th>Sexo</th>
                    <th>Estado</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($resultados as $row): ?>
                    <tr class="<?php if($row->inactivo == 0){ echo "primary"; }else{ echo "danger"; } ?>">
                        <td><?= $row->persona; ?></td>
                        <td><?= $row->nombres.' '.$row->apellidos; ?></td>
                        <td><?= $row->email; ?></td>
                        <td class=""><?= $row->telefono; ?></td>
                        <td><?= $row->sexo; ?></td>
                        <td><?php if($row->inactivo == 0){ echo "Activo"; }else{ echo "Inactivo"; } ?></td>
                        <td class="actions">
                            <a class="icon" href="<?= site_url('core/personas/form/'.$row->persona); ?>"><i class="mdi mdi-edit"></i></a>                          
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>