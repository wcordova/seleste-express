<?php
	$cliente 	= 0;
	$cui 		= '';
	$nombre 	= '';
	$apellido 	= '';
	$correo 	= '';
	$direccion 	= '';
	$cpostal 	= '';
	$nit 		= '';
	$telefono   = '';
	$pais       = '';
	$ciudad     = '';
	$inactivo   = '';

	if (!empty($resultado)) {
		$cliente   = $resultado->cliente;
		$cui       = $resultado->cui;
		$nombre    = $resultado->nombres;
		$apellido  = $resultado->apellidos;
		$correo    = $resultado->correo;
		$direccion = $resultado->direccion1;
		$cpostal   = $resultado->codigo_postal;
		$nit 	   = $resultado->nit;
		$telefono  = $resultado->telefono;
		$pais      = $resultado->pais;
		$ciudad    = $resultado->ciudad;
		if($resultado->inactivo == 1){
			$inactivo = "checked";
		}
	}

?>


<div class="card card-border-color card-border-color-primary">
	<div class="card-header card-header-divider"><?= $titulo;?> <span class="card-subtitle"><?= $subTitulo?></span> </div>

	<?php if($this->session->flashdata('eok') <> ''): ?>
		<div class="alert alert-success alert-dismissible" role="alert">
			<div class="icon"><span class="mdi mdi-check"></span></div>
				<div class="message">
					<button class="close" type="button" data-dismiss="alert" aria-label="Close">
						<span class="mdi mdi-close" aria-hidden="true"></span>
					</button>
					<strong>Ok!</strong><?php echo $this->session->flashdata('eok'); ?>
				</div>
		</div>
	<?php endif ?>

	<?php if($this->session->flashdata('eerror') <> ''): ?>
		<div class="alert alert-danger alert-dismissible" role="alert">
			<div class="icon"><span class="mdi mdi-close-circle-o"></span></div>
			<div class="message">
				<button class="close" type="button" data-dismiss="alert" aria-label="Close">
					<span class="mdi mdi-close" aria-hidden="true"></span>
				</button>
				<strong>Error!</strong><?php echo $this->session->flashdata('eerror'); ?>
			</div>
		</div>

	<?php endif ?>

	<div class="card-body">
		<form action="<?= $action; ?>" method="post" data-parsley-validate novalidate>
			<input type="text" id="cliente" name="cliente" value="<?= $cliente; ?>" hidden>
			<div class="form-group pt-2">
				<label for="nombre">Nombre</label>
				<input class="form-control" id="nombre" name="nombre" type="text" value="<?= $nombre; ?>" placeholder="Ingrese el nombre" required>
			</div>
			<div class="form-group pt-2">
				<label for="apellido">Apellido</label>
				<input class="form-control" id="apellido" name="apellido" type="text" value="<?= $apellido; ?>" placeholder="Ingrese el apellido" required>
			</div>
			<div class="form-group pt-2">
				<label for="correo">Correo</label>
				<input class="form-control" id="correo" name="correo" type="email" value="<?= $correo; ?>" placeholder=""Ingrese el correo" required>
			</div>
			<div class="form-group pt-2">
				<label for="direccion">Dirección</label>
				<input class="form-control" id="direccion" name="direccion" type="text" value="<?= $direccion; ?>" placeholder="Ingrese la direccion" required>
			</div>

			<div class="row">
				<div class="col-lg-6">
					<div class="form-group pt-2">
						<label for="cui">Cui</label>
						<input class="form-control" id="cui" name="cui" type="text" value="<?= $cui; ?>" placeholder="Ingrese el Cui" required>
					</div> 
				</div> 
				<div class="col-lg-6">
					<div class="form-group pt-2">
						<label for="nit">Nit</label>
						<input class="form-control" id="nit" name="nit" type="text" value="<?= $nit ?>" placeholder="Ingrese el nit">
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-lg-6">
					<div class="form-group pt-2">
						<label for="cPostal">Codigo Postal</label>
						<input class="form-control" id="cpostal" name="cpostal" type="number" value="<?= $cpostal; ?>" placeholder="Ingrese el codigo postal">
					</div>
				</div>
				<div class="col-lg-6">
					<div class="form-group pt-2">
						<label for="telefono">Telefono</label>
						<input class="form-control" id="telefono" name="telefono" type="text" value="<?= $telefono ?>" placeholder="Ingrese el numero de Telefono">
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-lg-6">
					<div class="form-group pt-2">
						<label for="pais">Pais</label>
						<select name="pais" id="pais" class="select2" onchange="" required>
							<option>Seleccine un pais...</option>
							<?php foreach ($paises as $row): 
								if ($pais == $row->pais): ?>
							 		<option value="<?= $row->pais ?>" selected><?= $row->nombre; ?></option>
							 	<?php else: ?>
									<option value="<?= $row->pais ?>"><?= $row->nombre; ?> </option>	
							<?php endif; endforeach; ?>
						</select>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="form-group pt-2">
						<label for="ciudad">ciudad</label>
						<select name="ciudad" id="ciudad" class="select2">
							<option>Seleccione una ciudad...</option>
						</select>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="form-check mt-1">
					<input class="form-check-input" type="checkbox" id="inactivo" name="inactivo" <?= $inactivo; ?>>
					<label class="form-check-label" for="flexCheckChecked">
						Inactivo
					</label>
				</div>
			</div>


			<div class="row pt-3">
				<p class="text-right">
					<button class="btn btn-space btn-primary" type="submit">Guardar</button>
					<button onclick="location.href='<?= site_url('core/cliente'); ?>'" type="button" class="btb btn-space btn-secondary">Cancelar</button>
				</p>
			</div>
		</form>
	</div>
</div>


<script src="<?= base_url('public/') ?>assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function() {
		cargarCiudad('<?= $pais; ?>');
	})
	
	function cargarCiudad(value) {
		let ciudad = '<?= $ciudad ?>';
		$.ajax({
			url: '<?= site_url('cla/ciudades/getCiudades') ?>',
			method: "POST",
			dataType: "json",
			data: {pais, value},
			success: function (data) {
				$('#ciudad').empty();
				App.ajaxLoader();
				if(data == 0) {
					$("#ciudad").append('<option value>No existen registros que mostrar...</option>');
				} else {
					$("#ciudad").append('<option value>Seleccione una ciudad... </option>');
					data.forEach(function(datosOpcion) {
						if (datosOpcion.ciudad == ciudad) {
							$("#ciudad").append('<option value="'+datosOpcion.ciudad+'" selected>'+datosOpcion.nombre+'</option>');
						} else {
							$("#ciudad").append('<option value="'+datosOpcion.ciudad+'">'+datosOpcion.nombre+'</option>');
						}
					});
				}
			}
		})
	}
</script>