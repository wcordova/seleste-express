<div class="card card-table">
	<div class="card-header"> Lista de cliente <button class="btn btn-space btn-primary mdi mdi-collection-plus" onclick="location.href='<?= site_url('core/cliente/form'); ?>'"> Agregar</button>
	</div>
	<div class="card-body">
		<table class="table table-striped table-hover table-fw-widget table3">
				<thead>
					<th>Id</th>
					<th>Nombre</th>
					<th>Nit</th>
					<th>Cui</th>
					<th>Telefono</th>
					<th>correo</th>
					<th>Activo</th>
					<th></th>
				</thead>
				<tbody>
					<?php $id = 1; ?>
					<?php foreach($resultado as $row): ?>
						<tr>
							<td><?= $id++; ?></td>
							<td><?= $row->nombres.' '.$row->apellidos; ?></td>
							<td><?= $row->nit; ?> </td>
							<td><?= $row->cui; ?></td>
							<td><?= $row->telefono; ?></td>
							<td><?= $row->correo; ?></td>
							<td>
								<?php if($row->inactivo == 1): ?>
									<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-lg" viewBox="0 0 16 16">
									  <path d="M1.293 1.293a1 1 0 0 1 1.414 0L8 6.586l5.293-5.293a1 1 0 1 1 1.414 1.414L9.414 8l5.293 5.293a1 1 0 0 1-1.414 1.414L8 9.414l-5.293 5.293a1 1 0 0 1-1.414-1.414L6.586 8 1.293 2.707a1 1 0 0 1 0-1.414z"/>
									</svg>

								<?php else: ?>
									<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-check-lg" viewBox="0 0 16 16">
									  <path d="M13.485 1.431a1.473 1.473 0 0 1 2.104 2.062l-7.84 9.801a1.473 1.473 0 0 1-2.12.04L.431 8.138a1.473 1.473 0 0 1 2.084-2.083l4.111 4.112 6.82-8.69a.486.486 0 0 1 .04-.045z"/>
									</svg>
								<?php endif; ?> 
							</td>
							<td class="actions">
								<a class="icon" href="<?= site_url('core/cliente/form/'.$row->cliente); ?>"><i class="mdi mdi-edit"></i></a>
							</td>
							
						</tr>
					<?php endforeach; ?>
				</tbody>
		</table>
	</div>
</div>