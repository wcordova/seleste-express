<?php
$usuario = 0;
$persona = 0;
$name = "";
$pass = "";
$fecha_expira = date("d/m/Y");
$inactivo = 0;
$rol = 0;

if (!empty($resultado)) {
    $usuario = $resultado->usuario;
    $persona = $resultado->persona;
    $name = $resultado->name;
    $pass = $resultado->pass;
    $inactivo = $resultado->inactivo;
    $fecha = strtotime($resultado->fecha_expira);
    $fecha_expira = date("d/m/Y", $fecha);
    $rol = $resultado->rol;
}

?>

<div class="card card-border-color card-border-color-primary">
    <div class="card-header card-header-divider"><?= $titulo; ?><span class="card-subtitle"><?= $subTitulo; ?></span></div>

    <?php if ($this->session->flashdata('eok') <> '') : ?>
        <div class="alert alert-success alert-dismissible" role="alert">
            <div class="icon"><span class="mdi mdi-check"></span></div>
            <div class="message">
                <button class="close" type="button" data-dismiss="alert" aria-label="Close">
                    <span class="mdi mdi-close" aria-hidden="true"></span></button>
                <strong>Ok!</strong> <?php echo $this->session->flashdata('eok'); ?>
            </div>
        </div>
    <?php endif ?>

    <?php if ($this->session->flashdata('eerror') <> '') : ?>
        <div class="alert alert-danger alert-dismissible" role="alert">
            <div class="icon"><span class="mdi mdi-close-circle-o"></span></div>
            <div class="message">
                <button class="close" type="button" data-dismiss="alert" aria-label="Close">
                    <span class="mdi mdi-close" aria-hidden="true"></span></button>
                <strong>Error!</strong> <?php $this->session->flashdata('eerror'); ?>
            </div>
        </div>
    <?php endif ?>

    <div class="card-body">
        <form action="<?= $action; ?>" method="post" data-parsley-validate novalidate>
            <input type="text" id="usuario" name="usuario" value="<?= $usuario; ?>" hidden>

            <div class="form-group">
                <label for="apellido">Seleccione una persona</label>
                <select name="persona" id="persona" class="select2" required>
                    <?php foreach ($personas as $row) {
                        if ($persona == $row->persona) { ?>
                            <option value="<?= $row->persona; ?>" selected><?= $row->nombres . ' ' . $row->apellidos; ?></option>
                        <?php
                        } else {
                        ?>
                            <option value="<?= $row->persona; ?>"><?= $row->nombres . ' ' . $row->apellidos; ?></option>
                    <?php
                        }
                    } ?>
                </select>
            </div>
            <div class="form-group">
                <label for="apellido">Seleccione un rol</label>
                <select name="rol" id="rol" class="select2" required>
                    <?php foreach ($roles as $row) {
                        if ($rol == $row->rol) { ?>
                            <option value="<?= $row->rol; ?>" selected><?= $row->nombre; ?></option>
                        <?php
                        } else {
                        ?>
                            <option value="<?= $row->rol; ?>"><?= $row->nombre; ?></option>
                    <?php
                        }
                    } ?>
                </select>
            </div>
            <div class="form-group pt-2">
                <label for="name">User name</label>
                <input class="form-control" id="name" name="name" type="text" value="<?= $name; ?>" placeholder="Ingrese el name" required>
            </div>
            <?php if ($usuario == 0) : ?>
                <div class="form-group">
                    <label for="apellido">Contraseña</label>
                    <input class="form-control" id="pass" name="pass" value="<?= $pass; ?>" type="text" placeholder="Ingrese una Contraseña" required>
                </div>
            <?php endif; ?>
            <div class="form-group">
                <label for="apellido">Fecha expiración</label>
                <div class="col-12 col-sm-7 col-md-5 col-lg-4 col-xl-3">
                    <div class="input-group date datetimepicker" data-min-view="2" data-date-format="dd/mm/yyyy">
                        <input class="form-control" id="fecha_expira" name="fecha_expira" type="text" value="<?= $fecha_expira; ?>">
                        <div class="input-group-append">
                            <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
                        </div>
                    </div>
                </div>
            </div>

            <?php if ($usuario > 0) : ?>
                <div class="form-group">
                    <label for="sexo">Estado</label>
                    <div class="form-check mt-1">
                        <label class="custom-control custom-radio">
                            <input class="custom-control-input" type="radio" id="inactivo" name="inactivo" value="0" <?php if ($inactivo == '0') { echo "checked"; } ?>><span class="custom-control-label">Activo</span>
                        </label>
                        <label class="custom-control custom-radio">
                            <input class="custom-control-input" type="radio" id="inactivo" name="inactivo" value="1" <?php if ($inactivo == '1') { echo "checked"; } ?>><span class="custom-control-label">Inactivo</span>
                        </label>
                    </div>
                </div>
            <?php endif; ?>
            <div class="row pt-3">
                <p class="text-right">
                    <button class="btn btn-space btn-primary" type="submit">Guardar</button>
                    <button onclick="location.href='<?= site_url('core/usuarios'); ?>'" type="button" class="btn btn-space btn-secondary">Cancel</button>
                </p>
            </div>
        </form>
    </div>
</div>
<script src="<?= base_url('public/') ?>assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url("public/js/generarPass.js") ?>"></script>
<script type="text/javascript">
    $(document).ready(function() {
        let valor = generar_contrasena();
        $("#pass").val(valor);
    });
</script>