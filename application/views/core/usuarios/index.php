<div class="card card-table">
    <div class="card-header">Listado de usuarios <button class="btn btn-space btn-primary mdi mdi-collection-plus" onclick="location.href='<?= site_url('core/usuarios/form'); ?>'"> Agregar</button>
    </div>
    <div class="card-body">
        <table class="table table-striped table-hover table-fw-widget table3">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Persona</th>
                    <th>Rol</th>
                    <th>Fecha expira</th>
                    <th>Estado</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($resultados as $row): ?>
                    <tr class="<?php if($row->inactivo == 0){ echo "primary"; }else{ echo "danger"; } ?>">
                        <td><?= $row->usuario; ?></td>
                        <td><?= $row->name; ?></td>
                        <td><?= $row->nombres.' '.$row->apellidos; ?></td>
                        <td><?= $row->nombreRol; ?></td>
                        <td><?= date("d/m/Y",strtotime($row->fecha_expira)); ?></td>
                        <td><?php if($row->inactivo == 0){ echo "Activo"; }else{ echo "Inactivo"; } ?></td>
                        <td class="actions">
                            <a class="icon" href="<?= site_url('core/usuarios/form/'.$row->usuario); ?>"><i class="mdi mdi-edit"></i></a>                          
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>