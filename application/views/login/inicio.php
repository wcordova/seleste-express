<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="<?= base_url('public/') ?>assets/img/logo-s.png">
  <title>Seleste Express | Inicio de Sesión</title>
  <link rel="stylesheet" type="text/css" href="<?= base_url('public/') ?>assets/lib/perfect-scrollbar/css/perfect-scrollbar.css"/>
  <link rel="stylesheet" type="text/css" href="<?= base_url('public/') ?>assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/>
  <link rel="stylesheet" href="<?= base_url('public/') ?>assets/css/app.css" type="text/css"/>
</head>
<body class="be-splash-screen">
  <div class="be-wrapper be-login">
    <div class="be-content">
      <div class="main-content container-fluid">
        <div class="splash-container">
          <div class="card card-border-color card-border-color-primary">
            <div class="card-header"><img class="logo-img" src="<?= base_url('public/') ?>assets/img/logo.jpg" alt="logo" width="200" height="200"><span class="splash-description">Por favor ingrese su información.</span></div>
            <div align="center">
              <h4 style="color: red"><?= $this->session->flashdata('ilogin'); ?></h4>
            </div>
            <div class="card-body">
              <form action="<?= site_url('home/init/login') ?>" method="post">
                <div class="form-group">
                  <input class="form-control" id="username" name="username" type="text" placeholder="Usuario" autocomplete="off">
                </div>
                <div class="form-group">
                  <input class="form-control" id="password" name="password" type="password" placeholder="Contraseña">
                </div>
                <div class="form-group row login-tools">
                  <div class="col-6 login-remember">
                    <label class="custom-control custom-checkbox">
                      <input class="custom-control-input" type="checkbox"><span class="custom-control-label">Recordarme</span>
                    </label>
                  </div>
                  <div class="col-6 login-forgot-password"><a href="pages-forgot-password.html">¿Olvide mi contraseña?</a></div>
                </div>
                <div class="form-group login-submit"><button class="btn btn-primary btn-xl" type="submit" data-dismiss="modal" onclick="encriptar();">Ingresar</button></div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script src="<?= base_url('public/') ?>assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
  <script src="<?= base_url('public/') ?>assets/lib/perfect-scrollbar/js/perfect-scrollbar.min.js" type="text/javascript"></script>
  <script src="<?= base_url('public/') ?>assets/lib/bootstrap/dist/js/bootstrap.bundle.min.js" type="text/javascript"></script>
  <script src="<?= base_url('public/') ?>assets/js/app.js" type="text/javascript"></script>
  
  <!-- libreria de encriptacion -->
  <script src="<?= base_url('public/js/encriptB64.js') ?> "></script>
  
  <script type="text/javascript">
    $(document).ready(function(){
      	//-initialize the javascript
      	App.init();
      });

    function encriptar() {
      var dato = $("#password").val();
      $("#password").val(Base64.encode(dato));
    }      
  </script>
</body>
</html>