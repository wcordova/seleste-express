<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * libreria para el manejo de usuarios, roles y seciones
 */
class Usuarios {

    function __construct() {
        $this->ci = & get_instance();
        $this->ci->load->library('session');
    }
    
    
    /**
     * [crearSession description]
     * @param  [type] $nombre  [nombre completo del usuario]
     * @param  [type] $email   [correo electronico usuario]
     * @param  [type] $usuario [id de usuario]
     * @param  [type] $rol     [id de rol]
     * @return [type]          [description]
     */
	public function crearSession($nombre, $email, $usuario, $rol, $cambio)
	{
        $datos = array(
            'nombre'  		=> $nombre,
            'email'   		=> $email,
            'usuario' 		=> $usuario,
            'rol'			=> $rol,
            'session' 		=> true,
            'cambio'        => $cambio
        );
        
        return $this->ci->session->set_userdata( $datos );        	
	}

    /**
     * elimina la session
     */
    function borrarSession(){
        //borrar sesion
        $datos['session'] = null;
        return $this->ci->session->set_userdata( $datos );
    }
    
    /**
     * funciones para validacion de session
     */
	function hay_usuario()
	{ // valida si hay usuario logiado
    	$login = $this->ci->session->userdata('session');
		if($login){ // si esta logiado
			return true;
		} else {
			redirect('Principal/sesionInvalida');
		}
    }  
   

}
