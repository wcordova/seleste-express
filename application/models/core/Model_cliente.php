<?php 
/**
 * 
 */
class Model_cliente extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function getData($cliente = 0, $nit = '') {
		if (!empty($cliente) || !empty($nit)){

			if ($cliente) {
				$this->db->where('cliente', $cliente);
			}

			if ($nit) {
				$this->db->where('nit', $nit);
			}

			return $this->db 
						->get('neg_cliente')
						->row();
		} else {

			return $this->db
					->get('neg_cliente')
					->result();
		}
	}

	public function create($datos) {
		if ($datos) {
			if ($this->db->insert('neg_cliente', $datos)) {
				return $this->db->insert_id();
			} else  {
				return false;
			}

		}
	}

	public function edit($cliente, $datos) {

		if ($datos) {
			$this->db->where("cliente", $cliente);
			if ($this->db->update('neg_cliente', $datos)) {
				return true;
			} else {
				return false;
			}
		}
	}
}

?>