<?php

/**
 * summary
 */
class Model_roles extends CI_Model
{
    /**
     * summary
     */
    function __construct()
    {
        parent::__construct();
    }

    public function getData($rol = 0)
    {
        if ($rol > 0) {
            return $this->db
                        ->where('rol', $rol)
                        ->get('seg_roles')
                        ->row();
        } else {
            return $this->db
                        ->get('seg_roles')
                        ->result();
        }
        
    }

    public function create($datos)
    {
        if ($datos) {
            if ($this->db->insert('seg_roles', $datos)) {
                return $this->db->insert_id();                
            } else {
                return false;
            }
        }
    }

    public function edit($rol,$datos)
    {
        if ($datos) {
            $this->db->where('rol', $rol);            
            if ($this->db->update('seg_roles', $datos)) {
                return true;             
            } else {
                return false;
            }
        } 
    }

}
