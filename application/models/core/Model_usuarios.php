<?php

/**
 * summary
 */
class Model_usuarios extends CI_Model
{
    /**
     * summary
     */
    function __construct()
    {
        parent::__construct();
    }

    public function getData($usuario = 0)
    {
        if ($usuario > 0) {
            return $this->db
                ->where('usuario', $usuario)
                ->get('seg_usuarios')
                ->row();
        } else {
            return $this->db
                        ->get('seg_usuarios')
                        ->result();
        }
        
    }

    public function create($datos)
    {
        if ($datos) {
            if ($this->db->insert('seg_usuarios', $datos)) {
                return $this->db->insert_id();                
            } else {
                return false;
            }
        }
    }

    public function edit($usuario,$datos)
    {
        if ($datos) {
            $this->db->where('usuario', $usuario);            
            if ($this->db->update('seg_usuarios', $datos)) {
                return true;             
            } else {
                return false;
            }
        } 
    }

    public function validar($data)
    {
        return $this->db
                    ->select('usu.usuario, per.nombres, per.apellidos, per.email, usu.rol')
                    ->join('seg_personas per','usu.persona = per.persona','inner')
                    ->where($data, false)
                    ->get('seg_usuarios usu')
                    ->row();
    }

    public function infoUser()
    {
        return $this->db
                    ->select('usu.*, per.nombres, per.apellidos, per.email, usu.rol, rol.nombre as nombreRol')
                    ->join('seg_personas per','usu.persona = per.persona','inner')
                    ->join('seg_roles rol','rol.rol = usu.rol','inner')
                    ->get('seg_usuarios usu')
                    ->result();
    }

}
