<?php

/**
 * summary
 */
class Model_personas extends CI_Model
{
    /**
     * summary
     */
    function __construct()
    {
        parent::__construct();
    }

    public function getData($persona = 0)
    {
        if ($persona > 0) {
            return $this->db
                ->where('persona', $persona)
                ->get('seg_personas')
                ->row();
        } else {
            return $this->db
                        ->get('seg_personas')
                        ->result();
        }
        
    }

    public function create($datos)
    {
        if ($datos) {
            if ($this->db->insert('seg_personas', $datos)) {
                return $this->db->insert_id();                
            } else {
                return false;
            }
        }
    }

    public function edit($persona,$datos)
    {
        if ($datos) {
            $this->db->where('persona', $persona);            
            if ($this->db->update('seg_personas', $datos)) {
                return true;             
            } else {
                return false;
            }
        } 
    }

}
