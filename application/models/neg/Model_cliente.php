<?php

/**
 * summary
 */
class Model_cliente extends CI_Model
{
    /**
     * summary
     */
    function __construct()
    {
        parent::__construct();
    }

    public function getData($cliente = "")
    {
        if ($cliente != "") {
            return $this->db
                        ->where('cliente', $cliente)
                        ->get('neg_cliente')
                        ->row();
        } else {
            return $this->db
                        ->get('neg_cliente')
                        ->result();
        }
        
    }

    public function getCliente($cliente)
    {
        return $this->db
                    ->select('a.*, b.nombre as pais_nombre, c.nombre as ciudad_nombre')
                    ->join('cla_paises b','b.pais = a.pais','inner')
                    ->join('cla_ciudades c','c.ciudad = a.ciudad','inner')
                    ->where('a.cliente', $cliente)
                    ->get('neg_cliente a')
                    ->row();
        
    }

    public function create($datos)
    {
        if ($datos) {
            if ($this->db->insert('neg_cliente', $datos)) {
                return $this->db->insert_id();          
            } else {
                return false;
            }
        }
    }

    public function edit($cliente,$datos)
    {
        if ($datos) {
            $this->db->where('cliente', $cliente);            
            if ($this->db->update('neg_cliente', $datos)) {
                return true;             
            } else {
                return false;
            }
        } 
    }

    public function buscarClientesPorNombre($n, $a, $p = "") 
    {
        if($n != null && $n != ""){
            $this->db->like('nombres',$n);
        }
        if($a != null && $a != ""){
            $this->db->like('apellidos',$a);
        }     
        if($p != null && $p != ""){
            $this->db->where('pais', $p);
        }
                
        $query = $this->db->get('neg_cliente');
        return $query->result();
    }

}
