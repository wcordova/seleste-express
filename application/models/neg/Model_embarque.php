<?php

/**
 * summary
 */
class Model_embarque extends CI_Model
{
    /**
     * summary
     */
    function __construct()
    {
        parent::__construct();
    }

    public function getData($embarque = "")
    {
        if ($embarque != "") {
            return $this->db
                        ->distinct('')
                        ->select('a.*, count(b.consolidado) as cant_consolidados, count(c.orden) as cant_ordenes, 
                        SUM(case when e.cobro_origen_destino = 0 then d.tarifa else 0 end) as cobro_origen, 
                        SUM(case when e.cobro_origen_destino = 1 then d.tarifa else 0 end) as cobro_destino,
                        SUM(case when d.envios_domesticos > 0 then d.envios_domesticos else 0 end) as envio_domesticos')
                        ->join('neg_consolidado b','b.embarque = a.embarque','left')
                        ->join('neg_consolidado_orden c','c.consolidado = b.consolidado','left')
                        ->join('neg_detalle_orden d','d.orden = c.orden','left')
                        ->join('neg_orden e','e.orden = c.orden','left')
                        ->group_by('a.embarque')
                        ->get('neg_embarque a')
                        ->row();
        } else {
            return $this->db
                        ->distinct('')
                        ->select('a.*, count(b.consolidado) as cant_consolidados, count(c.orden) as cant_ordenes, 
                        SUM(case when e.cobro_origen_destino = 0 then d.tarifa else 0 end) as cobro_origen, 
                        SUM(case when e.cobro_origen_destino = 1 then d.tarifa else 0 end) as cobro_destino,
                        SUM(case when d.envios_domesticos > 0 then d.envios_domesticos else 0 end) as envio_domesticos')
                        ->join('neg_consolidado b','b.embarque = a.embarque','left')
                        ->join('neg_consolidado_orden c','c.consolidado = b.consolidado','left')
                        ->join('neg_detalle_orden d','d.orden = c.orden','left')
                        ->join('neg_orden e','e.orden = c.orden','left')
                        ->group_by('a.embarque')
                        ->get('neg_embarque a')
                        ->result();
        }
    }

    public function getEmbarques($estado)
    {
        return $this->db
                    ->where('estado', $estado)
                    ->get('neg_embarque')
                    ->result();
    }


    public function create($datos)
    {
        if ($this->db->insert('neg_embarque', $datos)) {
            return $this->db->insert_id();
            ;
        } else {
            return false;
        }
    }

    public function edit($embarque, $datos)
    {
        $this->db->where('embarque', $embarque);
        if ($this->db->update('neg_embarque', $datos)) {
            return true;
        } else {
            return false;
        }
    }
}
