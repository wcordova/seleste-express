<?php

/**
 * summary
 */
class Model_consolidado extends CI_Model
{
    /**
     * summary
     */
    function __construct()
    {
        parent::__construct();
    }

    public function getConsolidado($consolidado)
    {
        return $this->db
            ->where('consolidado', $consolidado)
            ->get('neg_consolidado')
            ->row();
    }

    public function asignarGuia($datos)
    {
        if ($this->db->insert('neg_consolidado_orden', $datos)) {
            return true;
        } else {
            return false;
        }
    }

    public function quitarGuia($consolidado, $orden)
    {
        $this->db->where('consolidado', $consolidado);
        $this->db->where('orden', $orden);
        if ($this->db->delete('neg_consolidado_orden')) {
            return true;
        } else {
            return false;
        }
    }

    public function crearNuevo($datos)
    {
        if ($this->db->insert('neg_consolidado', $datos)) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    public function getOrdenesList($list)
    {
        return $this->db
            ->where_in('estado', $list)
            ->get('neg_consolidado')
            ->result();
    }

    public function editarConsolidado($consolidado, $datos)
    {
        $this->db->where('consolidado', $consolidado);
        if ($this->db->update('neg_consolidado', $datos)) {
            return true;
        } else {
            return false;
        }
    }

    public function getOrdenesAsignadas($consolidado)
    {
        return $this->db 
                    ->select('b.*, a.guia_madre')
                    ->where('a.consolidado',$consolidado)
                    ->join('neg_orden b', 'a.orden = b.orden','inner')
                    ->get('neg_consolidado_orden a')
                    ->result();
    }

    public function getOrdenesByConsolidado($consolidado, $desc)
    {
        return $this->db 
                    ->select('b.*, a.guia_madre')
                    ->where('a.consolidado',$consolidado)
                    ->where('a.desconsolidado', $desc)
                    ->join('neg_orden b', 'a.orden = b.orden','inner')
                    ->get('neg_consolidado_orden a')
                    ->result();
    }

    public function editarOrdenConsolidado($consolidado, $orden, $datos) 
    {
        $this->db->where('consolidado', $consolidado);
        $this->db->where('orden', $orden);
        if ($this->db->update('neg_consolidado_orden',$datos)) {
            return true;
        } else {
            return false;
        }
    }

    public function asignarGuiaMadre($consolidado, $orden)
    {
        $this->db->where('consolidado', $consolidado);
        $this->db->where('orden', $orden);
        $datos = array('guia_madre' => 1);
        if ($this->db->update('neg_consolidado_orden',$datos)) {
            return true;
        } else {
            return false;
        }
        
    }

    public function limpiarGuiaMadre($consolidado, $orden)
    {
        $this->db->where('consolidado', $consolidado);
        $datos = array('guia_madre' => 0);
        if ($this->db->update('neg_consolidado_orden',$datos)) {
            return true;
        } else {
            return false;
        }
        
    }
    

}
