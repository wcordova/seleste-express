<?php

/**
 * summary
 */
class Model_orden extends CI_Model
{
    /**
     * summary
     */
    function __construct()
    {
        parent::__construct();
    }

    public function getData($orden = "")
    {
        if ($orden != "") {
            return $this->db
                ->where('orden', $orden)
                ->get('neg_orden')
                ->row();
        } else {
            return $this->db
                ->get('neg_orden')
                ->result();
        }
    }

    public function getOrdenesList($list)
    {
        return $this->db
                    ->where_in('estado', $list)
                    ->get('neg_orden')
                    ->result();
    }

    public function create($datos)
    {
        if ($datos) {
            if ($this->db->insert('neg_orden', $datos)) {
                return $this->db->insert_id();
            } else {
                return false;
            }
        }
    }

    public function edit($orden, $datos)
    {
        if ($datos) {
            $this->db->where('orden', $orden);
            if ($this->db->update('neg_orden', $datos)) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function agregarOrdenDetalle($datos)
    {
        if ($this->db->insert('neg_detalle_orden', $datos)) {
            return true;
        } else {
            return false;
        }
    }

    public function getDetalleOrden($orden, $detalle = 0)
    {

        if (isset($detalle) && $detalle > 0) {
            $this->db->where('a.detalle', $detalle);
        }
        return $this->db
            ->select('a.*, b.nombre as nombre_producto, c.nombre as nombre_tipo_producto')
            ->join('cla_productos b', 'b.producto = a.producto', 'inner')
            ->join('cla_tipo_producto c', 'c.tipo_producto = a.tipo_producto', 'inner')
            ->where('a.orden', $orden)
            ->get('neg_detalle_orden a')
            ->result();
    }

    public function eliminarItemDetalle($detalle)
    {
        $this->db->where('detalle', $detalle);
        if ($this->db->delete('neg_detalle_orden')) {
            return true;
        } else {
            return false;
        }
    }

    public function actualizarItemDetalle($datos, $detalle)
    {
        $this->db->where('detalle', $detalle);
        if ($this->db->update('neg_detalle_orden', $datos)) {
           return true;
        } else {
            return false;
        }
    }

    public function actualizarOrden($orden, $datos)
    {
        $this->db->where('orden', $orden);
        if ($this->db->update('neg_orden', $datos)) {
            return true;
        } else {
            return false;
        }
    }
}
