<?php

/**
 * summary
 */
class Model_tarifas extends CI_Model
{
    /**
     * summary
     */
    function __construct()
    {
        parent::__construct();
    }

    public function getData($tarifa = "")
    {
        if ($tarifa != "") {
            return $this->db
                        ->where('tarifa', $tarifa)
                        ->get('cla_tarifas')
                        ->row();
        } else {
            return $this->db
                        ->get('cla_tarifas')
                        ->result();
        }
        
    }

    public function create($datos)
    {
        if ($datos) {
            if ($this->db->insert('cla_tarifas', $datos)) {
                return true;          
            } else {
                return false;
            }
        }
    }

    public function edit($tarifa,$datos)
    {
        if ($datos) {
            $this->db->where('tarifa', $tarifa);            
            if ($this->db->update('cla_tarifas', $datos)) {
                return true;             
            } else {
                return false;
            }
        } 
    }

    public function getDatosPorPais($pais)
    {
        return $this->db 
                    ->select('a.*, b.nombre as des_tipo_producto, c.nombre as des_producto, d.nombre as des_ciudad')
                    ->join('cla_tipo_producto b','b.tipo_producto = a.tipo_producto','inner')
                    ->join('cla_productos c','c.producto = a.producto','inner')
                    ->join('cla_ciudades d','d.ciudad = a.ciudad','left')
                    ->where('a.pais',$pais)
                    ->order_by('a.tipo_producto, a.producto')
                    ->get('cla_tarifas a')
                    ->result();

    }

    public function getTarifas($producto,$destino)
    {
        return $this->db 
                    ->select('monto')
                    ->where('producto',$producto)
                    ->where('ciudad',$destino)
                    ->where('inactivo',0)
                    ->get('cla_tarifas')
                    ->row();
    }

}
