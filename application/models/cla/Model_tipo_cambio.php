<?php

/**
 * summary
 */
class Model_tipo_cambio extends CI_Model
{
    /**
     * summary
     */
    function __construct()
    {
        parent::__construct();
    }

    public function getData()
    {
        return $this->db
                    ->limit(1)
                    ->order_by('tipo_cambio','desc')
                    ->get('cla_tipo_cambio')
                    ->row();
    }

    public function create($datos)
    {
        if ($datos) {
            if ($this->db->insert('cla_tipo_cambio', $datos)) {
                return true;          
            } else {
                return false;
            }
        }
    }

    public function existeCambioDelDia($fecha)
    {
        $sql = "SELECT * FROM selesteexdb.cla_tipo_cambio
                    WHERE date_add LIKE '%".$fecha."%' OR fecha_cambio = '".$fecha."' ORDER BY tipo_cambio DESC LIMIT 1";
        $query = $this->db->query($sql);
        return $query->row();
    }

}
