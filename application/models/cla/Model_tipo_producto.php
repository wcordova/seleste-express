<?php

/**
 * summary
 */
class Model_tipo_producto extends CI_Model
{
    /**
     * summary
     */
    function __construct()
    {
        parent::__construct();
    }

    public function getData($tipo_producto = 0)
    {
        if ($tipo_producto > 0) {
            return $this->db
                        ->where('tipo_producto', $tipo_producto)
                        ->get('cla_tipo_producto')
                        ->row();
        } else {
            return $this->db
                        ->get('cla_tipo_producto')
                        ->result();
        }
        
    }

    public function create($datos)
    {
        if ($datos) {
            if ($this->db->insert('cla_tipo_producto', $datos)) {
                return $this->db->insert_id();                
            } else {
                return false;
            }
        }
    }

    public function edit($tipo_producto,$datos)
    {
        if ($datos) {
            $this->db->where('tipo_producto', $tipo_producto);            
            if ($this->db->update('cla_tipo_producto', $datos)) {
                return true;             
            } else {
                return false;
            }
        } 
    }

}
