<?php

/**
 * summary
 */
class Model_productos extends CI_Model
{
    /**
     * summary
     */
    function __construct()
    {
        parent::__construct();
    }

    public function getData($producto = "")
    {
        if ($producto != "") {
            return $this->db
                        ->where('producto', $producto)
                        ->get('cla_productos')
                        ->row();
        } else {
            return $this->db
                        ->get('cla_productos')
                        ->result();
        }
        
    }

    public function create($datos)
    {
        if ($datos) {
            if ($this->db->insert('cla_productos', $datos)) {
                return true;          
            } else {
                return false;
            }
        }
    }

    public function edit($producto,$datos)
    {
        if ($datos) {
            $this->db->where('producto', $producto);            
            if ($this->db->update('cla_productos', $datos)) {
                return true;             
            } else {
                return false;
            }
        } 
    }

    public function cargarProductos($tipo_producto)
    {
        return $this->db 
                    ->where('tipo_producto',$tipo_producto)
                    ->where('inactivo',0)
                    ->get('cla_productos')
                    ->result();
    }

}
