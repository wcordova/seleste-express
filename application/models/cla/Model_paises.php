<?php

/**
 * summary
 */
class Model_paises extends CI_Model
{
    /**
     * summary
     */
    function __construct()
    {
        parent::__construct();
    }

    public function getData($pais = "")
    {
        if ($pais != "") {
            return $this->db
                        ->where('pais', $pais)
                        ->get('cla_paises')
                        ->row();
        } else {
            return $this->db
                        ->get('cla_paises')
                        ->result();
        }
        
    }

    public function getPaises()
    {
        return $this->db
                    ->where('inactivo',0)
                    ->get('cla_paises')
                    ->result();
    }

    public function create($datos)
    {
        if ($datos) {
            if ($this->db->insert('cla_paises', $datos)) {
                return true;          
            } else {
                return false;
            }
        }
    }

    public function edit($pais,$datos)
    {
        if ($datos) {
            $this->db->where('pais', $pais);            
            if ($this->db->update('cla_paises', $datos)) {
                return true;             
            } else {
                return false;
            }
        } 
    }

}
