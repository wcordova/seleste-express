<?php

/**
 * summary
 */
class Model_ciudades extends CI_Model
{
    /**
     * summary
     */
    function __construct()
    {
        parent::__construct();
    }

    public function getData($ciudad = 0)
    {
        if ($ciudad != 0) {
            return $this->db
                        ->where('ciudad', $ciudad)
                        ->get('cla_ciudades')
                        ->row();
        } else {
            return $this->db
                        ->get('cla_ciudades')
                        ->result();
        }
        
    }

    public function estadoActual($ciudad)
    {
        return $this->db 
                    ->where('ciudad',$ciudad)
                    ->get('cla_ciudades')
                    ->row('inactivo');
    }

    public function getCiudades($pais)
    {
        return $this->db
                    ->where('pais',$pais)
                    ->get('cla_ciudades')
                    ->result();
    }

    public function cargarCuidades($pais)
    {
        return $this->db                    
                    ->where('pais',$pais)
                    ->where('inactivo',0)
                    ->get('cla_ciudades')
                    ->result();
    }

    public function create($datos)
    {
        if ($datos) {
            if ($this->db->insert('cla_ciudades', $datos)) {
                return true;          
            } else {
                return false;
            }
        }
    }

    public function edit($ciudad,$datos)
    {
        if ($datos) {
            $this->db->where('ciudad', $ciudad);            
            if ($this->db->update('cla_ciudades', $datos)) {
                return true;             
            } else {
                return false;
            }
        } 
    }

}
