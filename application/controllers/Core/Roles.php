<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Roles extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();
		hay_usuario();
		$model = array('core/Model_roles');		

		$this->load->model($model);
	}

	public function index()
	{
		$this->data['titulo'] = "Mantenimiento de Roles";
		$this->data['view'] = "core/roles/index";
		$this->data['menu'] = "core";
		$this->data['sub_menu'] = "rol";
		$this->data['resultados'] = $this->Model_roles->getData();
		
		$this->load->view('layout/content', $this->data);
	}
	
	public function form($rol = 0)
	{
		$this->data['titulo'] = "Mantenimiento de Roles";
		$this->data['menu'] = "core";
		$this->data['sub_menu'] = "rol";
		$this->data['view'] = "core/roles/form";
		$this->data['titulo'] = "Formulario de Rol.";
		$this->data['subTitulo'] = "Este es el formulario para crear una nuevo rol.";
		$this->data['action'] = site_url('core/roles/create');
		$this->data['resultado'] = null;

		if ($rol > 0) {
			$this->data['action'] = site_url('core/roles/edit');
			$this->data['resultado'] = $this->Model_roles->getData($rol);
		}
		
		$this->load->view('layout/content', $this->data);
	}

	public function create()
	{
		if($_POST){

			$nombre = $this->input->post('nombre');			
			$descripcion = $this->input->post('descripcion');			

			$datos = array(
				'nombre' => $nombre,
				'descripcion' => $descripcion,
				'user_add' => $this->session->userdata('usuario')
			);

			$result = $this->Model_roles->create($datos);
			if ($result != false) {
				$this->session->set_flashdata('eok', 'Registro creado satisfactoriamente');
			} else {
				$this->session->set_flashdata('eerror', 'Ocurrio un error al intentar crear el registro');
			}
			

		}else{
			$this->session->set_flashdata('eerror', 'Error al guardar el registro, contacte al administrador');
		}
		redirect("core/roles/form");
	}

	public function edit()
	{
		if($_POST){

			$rol = $this->input->post('rol');			
			$nombre = $this->input->post('nombre');			
			$descripcion = $this->input->post('descripcion');	
			$inactivo = $this->input->post('inactivo');					

			$datos = array(
				'nombre' => $nombre,
				'descripcion' => $descripcion,
				'inactivo' => $inactivo,
				'user_update' => $this->session->userdata('usuario'),
                'date_update' => date("Y-m-d H:i:s")
			);


			if ($this->Model_roles->edit($rol,$datos)) {
				$this->session->set_flashdata('eok', 'Registro editado satisfactoriamente');
				redirect("core/roles/form/".$rol);
			} else {
				$this->session->set_flashdata('eerror', 'Ocurrio un error al intentar editar el registro');
				redirect("core/roles/form/".$rol);
			}
			

		}else{
			$this->session->set_flashdata('eerror', 'Error al guardar el registro, contacte al administrador');
			redirect("core/roles/form");
		}
		
	}	
	
	
    


}