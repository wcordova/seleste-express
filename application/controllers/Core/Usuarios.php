<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();
		hay_usuario();
		$model = array('core/Model_usuarios','core/Model_personas','core/Model_roles');		

		$this->load->model($model);
	}

	public function index()
	{
		$this->data['titulo'] = "Mantenimiento de usuarios";
		$this->data['view'] = "core/usuarios/index";
		$this->data['menu'] = "core";
		$this->data['sub_menu'] = "user";
		$this->data['resultados'] = $this->Model_usuarios->infoUser();
		
		$this->load->view('layout/content', $this->data);
	}
	
	public function form($usuario = 0)
	{
		$this->data['titulo'] = "Mantenimiento de usuarios";
		$this->data['menu'] = "core";
		$this->data['sub_menu'] = "user";
		$this->data['view'] = "core/usuarios/form";
		$this->data['titulo'] = "Formulario de Persona.";
		$this->data['subTitulo'] = "Este es el formulario para crear una nueva persona.";
		$this->data['action'] = site_url('core/usuarios/create');
		$this->data['resultado'] = null;
		$this->data['personas'] = $this->Model_personas->getData();
		$this->data['roles'] = $this->Model_roles->getData();

		if ($usuario > 0) {
			$this->data['action'] = site_url('core/usuarios/edit');
			$this->data['resultado'] = $this->Model_usuarios->getData($usuario);
		}
		
		$this->load->view('layout/content', $this->data);
	}

	public function create()
	{
		if($_POST){

			$persona = $this->input->post('persona');		
			$rol = $this->input->post('rol');		
			$name = $this->input->post('name');			
			$pass = $this->input->post('pass');								
			$fecha_expira = $this->input->post('fecha_expira');		
			$_date = str_replace('/', '-', $fecha_expira);
			$timestamp = date('Y-m-d', strtotime($_date));	

			$datos = array(
					'persona' => $persona,
					'rol' => $rol,
					'name' => $name,
					'pass' => sha1($pass),
					'fecha_expira' => $timestamp,
					'user_add' => $this->session->userdata('usuario')
			);

			$result = $this->Model_usuarios->create($datos);
			if ($result != false) {
				$this->session->set_flashdata('eok', 'Registro creado satisfactoriamente');
			} else {
				$this->session->set_flashdata('eerror', 'Ocurrio un error al intentar crear el registro');
			}
			

		}else{
			$this->session->set_flashdata('eerror', 'Error al guardar el registro, contacte al administrador');
		}
		redirect("core/usuarios/form");
	}

	public function edit()
	{
		if($_POST){

			$usuario = $this->input->post('usuario');			
			$persona = $this->input->post('persona');
			$rol = $this->input->post('rol');				
			$name = $this->input->post('name');			
			$pass = $this->input->post('pass');			
			$fecha_expira = $this->input->post('fecha_expira');		
			$_date = str_replace('/', '-', $fecha_expira);
			$timestamp = date('Y-m-d', strtotime($_date));	
			$inactivo = $this->input->post('inactivo');

			$datos = array(
					'persona' => $persona,
					'rol' => $rol,
					'name' => $name,
					'pass' => sha1($pass),
					'fecha_expira' => $timestamp,
					'user_update' => $this->session->userdata('usuario'),
                	'date_update' => date("Y-m-d H:i:s"),
					'inactivo' => $inactivo
			);

			if ($this->Model_usuarios->edit($usuario,$datos)) {
				$this->session->set_flashdata('eok', 'Registro editado satisfactoriamente');
				redirect("core/usuarios/form/".$persona);
			} else {
				$this->session->set_flashdata('eerror', 'Ocurrio un error al intentar editar el registro');
				redirect("core/usuarios/form/".$persona);
			}
			

		}else{
			$this->session->set_flashdata('eerror', 'Error al guardar el registro, contacte al administrador');
			redirect("core/usuarios/form");
		}
		
	}	
	
	
    


}