<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	/**
	 * 
	 */
	class Cliente extends CI_Controller
	{
		
		public function __construct() {
			parent::__construct();
			hay_usuario();
			$model = array('core/Model_cliente', 'cla/Model_paises');		

			$this->load->model($model);
		}


		public function index() {
			$this->data['titulo'] = "Mantenimiento de Cliente";
			$this->data['view'] = "core/cliente/index";
			$this->data['menu'] = "core";
			$this->data['sub_menu'] = "cliente";
			$this->data['resultado'] = $this->Model_cliente->getData();
			$this->load->view('layout/content', $this->data);
		}

		public function form($cliente = 0) {
			$this->data['menu'] 	  = "core";
			$this->data['sub_menu']   = "cliente";
			$this->data['view'] 	  = "core/cliente/form";
			$this->data['titulo']     = "Formulario de Cliente.";
			$this->data['subTitulo']  = "Este es el formulario para crear un nueva cliente.";
			$this->data['action'] 	  = site_url('core/cliente/create');
			$this->data['paises']	  = $this->Model_paises->getPaises();
			$this->data['resultado']  = null;

			if ($cliente > 0) {
				$this->data['action']    = site_url("core/cliente/edit");
				$this->data['resultado'] = $this->Model_cliente->getData($cliente);
			}

			$this->load->view("layout/content", $this->data);
		}

		public function create(){
			if ($_POST) {
				$cui 		= $this->input->post("cui");
				$nombre 	= $this->input->post("nombre");
				$apellido 	= $this->input->post("apellido");
				$correo 	= $this->input->post("correo'");
				$direccion 	= $this->input->post("direccion");
				$cpostal 	= $this->input->post("cpostal");
				$nit 		= $this->input->post("nit");
				$telefono   = $this->input->post("telefono");
				$pais       = $this->input->post("pais");
				$ciudad     = $this->input->post("ciudad");
				$inactivo   = $this->input->post("inactivo");

				$datos = array(
					'cui' 			=> $cui,
					'nombres' 		=> $nombre,
					'apellidos' 	=> $apellido,
					'correo' 		=> $correo,
					'direccion1' 	=> $direccion,
					'codigo_postal' => $cpostal,
					'nit' 			=> $nit,
					'telefono'      => $telefono,
					'pais'          => $pais,
					'ciudad'        => $ciudad,
					'inactivo'      => $inactivo ? 1 : 0,
 					'user_add'		=> $this->session->userdata('usuario'),
					'date_add'		=> date("Y-m-d H:i:s")
				);

				$datoExiste = $this->Model_cliente->getData(0,$nit); 
				if (count($datoExiste) > 0)  {
					$this->session->set_flashdata("eerror", " Ya existe el cliente: <b>{$datoExiste->nombres}.{$datoExiste->apellidos} </b> con el mismo nit.");
				} else {
					$result = $this->Model_cliente->create($datos);
					if ($result != false) {
						$this->session->set_flashdata("eok", " Registro creado con exito.");
					} else {
						$this->session->set_flashdata("eerror", " Ocurrio un error al intentar crear el registro.");
					}
				}

			} else {
				$this->session->set_flashdata("eerror", " Error al guardar el registro, contacte al administrador.");
			}

			redirect("core/cliente/form");
		}

		public function edit(){

			if ($_POST) {

				$cliente   = $this->input->post("cliente");
				$cui       = $this->input->post("cui");
				$nombre    = $this->input->post("nombre");
				$apellido  = $this->input->post("apellido");
				$correo    = $this->input->post("correo");
				$direccion = $this->input->post("direccion");
				$cpostal   = $this->input->post("cpostal");
				$nit       = $this->input->post("nit");
				$telefono  = $this->input->post("telefono");
				$pais      = $this->input->post("pais");
				$ciudad    = $this->input->post("ciudad");
				$inactivo  = $this->input->post("inactivo");

				$datos = array(
						"cui" 			=> $cui,
						"nombres" 		=> $nombre,
						"apellidos" 	=> $apellido,
						"correo" 		=> $correo,
						"direccion1" 	=> $direccion,
						"codigo_postal" => $cpostal,
						"nit" 			=> $nit,
						"telefono"      => $telefono,
						"pais"			=> $pais,
						"ciudad"        => $ciudad,
						"inactivo"      => $inactivo ? 1 : 0,
						"user_update"   => $this->session->userdata('usuario'),
						"date_update"   => date("Y-m-d H:i:s")
				);
				

				$result = $this->Model_cliente->edit($cliente, $datos);
				if ($result) {
					$this->session->set_flashdata('eok', 'Actualización realido con exito.');
					redirect("core/cliente/form/".$cliente);
				} else {
					$this->session->set_flashdata('eerror', 'Ocurrio un errro al actualizar el registro.');
					redirect("core/cliente/form/".$cliente);
				}

			} else {
				$this->session->set_flashdata('eerror', 'Error al guardar actualizar, contacte al administrador.');
				redirect("core/cliente/form");
			}
		}

	}
?>