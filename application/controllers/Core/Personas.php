<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Personas extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();
		hay_usuario();
		$model = array('core/Model_personas');		

		$this->load->model($model);
	}

	public function index()
	{
		$this->data['titulo'] = "Mantenimiento de Personas";
		$this->data['view'] = "core/personas/index";
		$this->data['menu'] = "core";
		$this->data['sub_menu'] = "per";
		$this->data['resultados'] = $this->Model_personas->getData();
		
		$this->load->view('layout/content', $this->data);
	}
	
	public function form($persona = 0)
	{
		$this->data['titulo'] = "Mantenimiento de Personas";
		$this->data['menu'] = "core";
		$this->data['sub_menu'] = "per";
		$this->data['view'] = "core/personas/form";
		$this->data['titulo'] = "Formulario de Persona.";
		$this->data['subTitulo'] = "Este es el formulario para crear una nueva persona.";
		$this->data['action'] = site_url('core/personas/create');
		$this->data['resultado'] = null;

		if ($persona > 0) {
			$this->data['action'] = site_url('core/personas/edit');
			$this->data['resultado'] = $this->Model_personas->getData($persona);
		}
		
		$this->load->view('layout/content', $this->data);
	}

	public function create()
	{
		if($_POST){

			$nombres = $this->input->post('nombres');			
			$apellidos = $this->input->post('apellidos');			
			$cui = $this->input->post('cui');			
			$email = $this->input->post('email');			
			$telefono = $this->input->post('telefono');			
			$sexo = $this->input->post('sexo');

			$datos = array(
				'nombres' => $nombres,
				'apellidos' => $apellidos,
				'cui' => $cui,
				'email' => $email,
				'telefono' => $telefono,
				'sexo' => $sexo,
				'user_add' => $this->session->userdata('usuario')
			);

			$result = $this->Model_personas->create($datos);
			if ($result != false) {
				$this->session->set_flashdata('eok', 'Registro creado satisfactoriamente');
			} else {
				$this->session->set_flashdata('eerror', 'Ocurrio un error al intentar crear el registro');
			}
			

		}else{
			$this->session->set_flashdata('eerror', 'Error al guardar el registro, contacte al administrador');
		}
		redirect("core/personas/form");
	}

	public function edit()
	{
		if($_POST){

			$persona = $this->input->post('persona');			
			$nombres = $this->input->post('nombres');			
			$apellidos = $this->input->post('apellidos');			
			$cui = $this->input->post('cui');			
			$email = $this->input->post('email');			
			$telefono = $this->input->post('telefono');			
			$sexo = $this->input->post('sexo');
			$inactivo = $this->input->post('inactivo');

			$datos = array(
				'nombres' => $nombres,
				'apellidos' => $apellidos,
				'cui' => $cui,
				'email' => $email,
				'telefono' => $telefono,
				'sexo' => $sexo,
				'user_update' => $this->session->userdata('usuario'),
                'date_update' => date("Y-m-d H:i:s"),
				'inactivo' => $inactivo
			);

			if ($this->Model_personas->edit($persona,$datos)) {
				$this->session->set_flashdata('eok', 'Registro editado satisfactoriamente');
				redirect("core/personas/form/".$persona);
			} else {
				$this->session->set_flashdata('eerror', 'Ocurrio un error al intentar editar el registro');
				redirect("core/personas/form/".$persona);
			}
			

		}else{
			$this->session->set_flashdata('eerror', 'Error al guardar el registro, contacte al administrador');
			redirect("core/personas/form");
		}
		
	}	
	
	
    


}