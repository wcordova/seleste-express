<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Paises extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();
		hay_usuario();
		$model = array('cla/Model_paises','cla/Model_ciudades');		

		$this->load->model($model);
	}

	public function index()
	{
		$this->data['titulo'] = "Mantenimiento de Paises";
		$this->data['view'] = "cla/paises/index";
		$this->data['menu'] = "cla";
		$this->data['sub_menu'] = "pais";
		$this->data['resultados'] = $this->Model_paises->getData();
		
		$this->load->view('layout/content', $this->data);
	}
	
	public function form($pais = "")
	{
		$this->data['titulo'] = "Mantenimiento de Paises";
		$this->data['view'] = "cla/paises/form";
		$this->data['menu'] = "cla";
		$this->data['sub_menu'] = "pais";
		$this->data['subTitulo'] = "Este es el formulario para crear un nuevo pais.";
		$this->data['action'] = site_url('cla/paises/create');
		$this->data['resultado'] = null;

		if ($pais != "") {
			$this->data['action'] = site_url('cla/paises/edit');
			$this->data['resultado'] = $this->Model_paises->getData($pais);
		}
		
		$this->load->view('layout/content', $this->data);
	}

	public function create()
	{
		if($_POST){

            $nombre = $this->input->post('nombre');	
            $pais = $this->input->post('pais');	

			$datos = array(
                'pais' => $pais,
				'nombre' => $nombre,
				'user_add' => $this->session->userdata('usuario')
			);

			$result = $this->Model_paises->create($datos);
			if ($result != false) {
				$this->session->set_flashdata('eok', 'Registro creado satisfactoriamente');
			} else {
				$this->session->set_flashdata('eerror', 'Ocurrio un error al intentar crear el registro');
			}
			

		}else{
			$this->session->set_flashdata('eerror', 'Error al guardar el registro, contacte al administrador');
		}
		redirect("cla/paises/form");
	}

	public function edit()
	{
		if($_POST){

			$pais = $this->input->post('pais');			
			$nombre = $this->input->post('nombre');			
			$inactivo = $this->input->post('inactivo');					

			$datos = array(
				'nombre' => $nombre,
				'inactivo' => $inactivo,
                'user_update' => $this->session->userdata('usuario'),
                'date_update' => date("Y-m-d H:i:s")
			);


			if ($this->Model_paises->edit($pais,$datos)) {
				$this->session->set_flashdata('eok', 'Registro editado satisfactoriamente');
				redirect("cla/paises/form/".$pais);
			} else {
				$this->session->set_flashdata('eerror', 'Ocurrio un error al intentar editar el registro');
				redirect("cla/paises/form/".$pais);
			}
			

		}else{
			$this->session->set_flashdata('eerror', 'Error al guardar el registro, contacte al administrador');
			redirect("cla/paises/form");
		}
		
	}	
	
	
    


}