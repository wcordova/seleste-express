<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tarifas extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();
		hay_usuario();
		$model = array('cla/Model_tarifas','cla/Model_tipo_producto','cla/Model_productos','cla/Model_paises','cla/Model_ciudades');		

		$this->load->model($model);
	}

	public function index()
	{
		$this->data['titulo'] = "Mantenimiento de tarifas";
		$this->data['view'] = "cla/tarifas/index";
		$this->data['menu'] = "cla";
		$this->data['sub_menu'] = "tarifa";
		$this->data['paises'] = $this->Model_paises->getPaises();
		
		$this->load->view('layout/content', $this->data);
	}

	public function form($tarifa = "")
	{
		$this->data['titulo'] = "Mantenimiento de tarifas";
		$this->data['view'] = "cla/tarifas/form";
		$this->data['menu'] = "cla";
		$this->data['sub_menu'] = "tarifa";
		$this->data['subTitulo'] = "Este es el formulario para crear una nueva tarifa.";
		$this->data['action'] = site_url('cla/tarifas/create');
		$this->data['resultado'] = null;
		$this->data['tipo_productos'] = $this->Model_tipo_producto->getData();
		$this->data['paises'] = $this->Model_paises->getPaises();

		if ($tarifa != "") {
			$this->data['action'] = site_url('cla/tarifas/edit');
			$this->data['resultado'] = $this->Model_tarifas->getData($tarifa);
		}
		
		$this->load->view('layout/content', $this->data);
	}

	public function create()
	{
		if($_POST){
			  
			$producto = $this->input->post('producto');			  
			$tipo_producto = $this->input->post('tipo_producto');			  
			$pais = $this->input->post('pais');			  
			if ($this->input->post('ciudad')) {
				$ciudad = $this->input->post('ciudad');	
			}else{
				$ciudad = null;
			}					  
			$monto = $this->input->post('monto');			  		  
			$descripcion = $this->input->post('descripcion');			  		  

			$datos = array(
                'producto' => $producto,
				'tipo_producto' => $tipo_producto,
				'pais' => $pais,
				'ciudad' => $ciudad,
				'monto' => $monto,
				'descripcion' => $descripcion,
				'user_add' => $this->session->userdata('usuario')
			);

			$result = $this->Model_tarifas->create($datos);
			if ($result != false) {
				$this->session->set_flashdata('eok', 'Registro creado satisfactoriamente');
			} else {
				$this->session->set_flashdata('eerror', 'Ocurrio un error al intentar crear el registro');
			}
			

		}else{
			$this->session->set_flashdata('eerror', 'Error al guardar el registro, contacte al administrador');
		}
		redirect("cla/tarifas/form");
	}

	public function edit()
	{
		if($_POST){

			$tarifa = $this->input->post('tarifa');			
			$producto = $this->input->post('producto');			  
			$tipo_producto = $this->input->post('tipo_producto');			  
			$pais = $this->input->post('pais');			  
			$ciudad = $this->input->post('ciudad');			  
			$monto = $this->input->post('monto');			  		  
			$descripcion = $this->input->post('descripcion');	
			$inactivo = $this->input->post('inactivo');				  		  

			$datos = array(
                'producto' => $producto,
				'tipo_producto' => $tipo_producto,
				'pais' => $pais,
				'ciudad' => $ciudad,
				'monto' => $monto,
				'descripcion' => $descripcion,
				'inactivo' => $inactivo,
                'user_update' => $this->session->userdata('usuario'),
                'date_update' => date("Y-m-d H:i:s")
			);

			if ($this->Model_tarifas->edit($tarifa,$datos)) {
				$this->session->set_flashdata('eok', 'Registro editado satisfactoriamente');
				redirect("cla/tarifas/form/".$producto);
			} else {
				$this->session->set_flashdata('eerror', 'Ocurrio un error al intentar editar el registro');
				redirect("cla/tarifas/form/".$producto);
			}
			

		}else{
			$this->session->set_flashdata('eerror', 'Error al editar el registro, contacte al administrador');
			redirect("cla/tarifas/form");
		}
		
	}

	public function getTarifas()
	{
		$producto = $this->input->post('producto');
		$destino = $this->input->post('destino');
		$resultado = "";

		if($producto > 0 && $destino > 0){
			$resultado = $this->Model_tarifas->getTarifas($producto,$destino); 

			if ($resultado->monto > 0) {
				echo $resultado->monto;
			} else {
				echo 0;
			}
		}
		
	}

	
	

}