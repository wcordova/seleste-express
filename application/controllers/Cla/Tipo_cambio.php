<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tipo_cambio extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	

	public function __construct()
	{
		parent::__construct();
		hay_usuario();
		$model = array('cla/Model_tipo_cambio');

		$this->load->model($model);
	}
	
	public function actualizarTipoCambio()
	{
		if ($_POST) {
			$cambio = $this->input->post('cambio');
			echo "cambio " + $cambio;
			$fecha = date('Y-m-d');
			
			$datos = array(
					'cambio' => $cambio, 
					'fecha_cambio' => $fecha,
					'user_add' => $this->session->userdata('usuario')
				);

			if ($this->Model_tipo_cambio->create($datos)) {
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
		
	}


    

}