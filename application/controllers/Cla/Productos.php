<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Productos extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();
		hay_usuario();
		$model = array('cla/Model_tipo_producto','cla/Model_productos');		

		$this->load->model($model);
	}

	public function index()
	{
		$this->data['titulo'] = "Mantenimiento de productos";
		$this->data['view'] = "cla/productos/index";
		$this->data['menu'] = "cla";
		$this->data['sub_menu'] = "producto";
		$this->data['resultados'] = $this->Model_productos->getData();
		
		$this->load->view('layout/content', $this->data);
	}
	
	public function form($producto = "")
	{
		$this->data['titulo'] = "Mantenimiento de productos";
		$this->data['view'] = "cla/productos/form";
		$this->data['menu'] = "cla";
		$this->data['sub_menu'] = "producto";
		$this->data['subTitulo'] = "Este es el formulario para crear un nuevo producto.";
		$this->data['action'] = site_url('cla/productos/create');
		$this->data['resultado'] = null;
		$this->data['tipo_productos'] = $this->Model_tipo_producto->getData();

		if ($producto != "") {
			$this->data['action'] = site_url('cla/productos/edit');
			$this->data['resultado'] = $this->Model_productos->getData($producto);
		}
		
		$this->load->view('layout/content', $this->data);
	}

	public function create()
	{
		if($_POST){

            $nombre = $this->input->post('nombre');	
			$producto = $this->input->post('producto');	
			$tipo_producto = $this->input->post('tipo_producto');
			$descripcion = $this->input->post('descripcion');

			$datos = array(
                'producto' => $producto,
				'nombre' => $nombre,
				'tipo_producto' => $tipo_producto,
				'descripcion' => $descripcion,
				'user_add' => $this->session->userdata('usuario')
			);

			$result = $this->Model_productos->create($datos);
			if ($result != false) {
				$this->session->set_flashdata('eok', 'Registro creado satisfactoriamente');
			} else {
				$this->session->set_flashdata('eerror', 'Ocurrio un error al intentar crear el registro');
			}
			

		}else{
			$this->session->set_flashdata('eerror', 'Error al guardar el registro, contacte al administrador');
		}
		redirect("cla/productos/form");
	}

	public function edit()
	{
		if($_POST){

			$producto = $this->input->post('producto');			
			$nombre = $this->input->post('nombre');		
			$tipo_producto = $this->input->post('tipo_producto');
			$descripcion = $this->input->post('descripcion');	
			$inactivo = $this->input->post('inactivo');					

			$datos = array(
				'nombre' => $nombre,
				'tipo_producto' => $tipo_producto,
				'descripcion' => $descripcion,
				'inactivo' => $inactivo,
                'user_update' => $this->session->userdata('usuario'),
                'date_update' => date("Y-m-d H:i:s")
			);


			if ($this->Model_productos->edit($producto,$datos)) {
				$this->session->set_flashdata('eok', 'Registro editado satisfactoriamente');
				redirect("cla/productos/form/".$producto);
			} else {
				$this->session->set_flashdata('eerror', 'Ocurrio un error al intentar editar el registro');
				redirect("cla/productos/form/".$producto);
			}
			

		}else{
			$this->session->set_flashdata('eerror', 'Error al guardar el registro, contacte al administrador');
			redirect("cla/productos/form");
		}
		
	}	

	public function getProductos()
	{
        $tipo_producto = $this->input->post('tipo_producto');
        $productos = $this->Model_productos->cargarProductos($tipo_producto);
        echo json_encode($productos, JSON_PRETTY_PRINT);        
	}
	
	
    


}