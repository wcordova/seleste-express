<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ciudades extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
        parent::__construct();
        hay_usuario();
		$model = array('cla/Model_ciudades');		

		$this->load->model($model);
    }
    
    public function cambiarEstado() 
    {
        $ciudad = $this->input->post('ciudad');
        $estado = 0;
        $estadoActual = $this->Model_ciudades->estadoActual($ciudad);
        if ($estadoActual == 0) {
            $estado = 1;
        }

        $datos = array(
            'inactivo' => $estado,
            'user_update' => $this->session->userdata('usuario'),
            'date_update' => date("Y-m-d H:i:s")
        );

        if ($this->Model_ciudades->edit($ciudad,$datos)) {
            echo "true";
        }else{
            echo "false";
        }

    }

    public function getCiudades()
	{
        $pais = $this->input->post('pais');
        $ciudades = $this->Model_ciudades->cargarCuidades($pais);
        echo json_encode($ciudades, JSON_PRETTY_PRINT);        
	}


    


}