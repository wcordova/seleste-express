<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tipo_producto extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();
		hay_usuario();
		$model = array('cla/Model_tipo_producto');		

		$this->load->model($model);
	}

	public function index()
	{
		$this->data['titulo'] = "Mantenimiento de Tipo Producto";
		$this->data['view'] = "cla/tipo_producto/index";
		$this->data['menu'] = "cla";
		$this->data['sub_menu'] = "tipoProd";
		$this->data['resultados'] = $this->Model_tipo_producto->getData();
		
		$this->load->view('layout/content', $this->data);
	}
	
	public function form($tipo_producto = 0)
	{
		$this->data['titulo'] = "Mantenimiento de Tipo Producto";
		$this->data['view'] = "cla/tipo_producto/form";
		$this->data['menu'] = "cla";
		$this->data['sub_menu'] = "tipoProd";
		$this->data['subTitulo'] = "Este es el formulario para crear una nuevo tipo de producto.";
		$this->data['action'] = site_url('cla/tipo_producto/create');
		$this->data['resultado'] = null;

		if ($tipo_producto > 0) {
			$this->data['action'] = site_url('cla/tipo_producto/edit');
			$this->data['resultado'] = $this->Model_tipo_producto->getData($tipo_producto);
		}
		
		$this->load->view('layout/content', $this->data);
	}

	public function create()
	{
		if($_POST){

			$nombre = $this->input->post('nombre');					

			$datos = array(
				'nombre' => $nombre,
				'user_add' => $this->session->userdata('usuario')
			);

			$result = $this->Model_tipo_producto->create($datos);
			if ($result != false) {
				$this->session->set_flashdata('eok', 'Registro creado satisfactoriamente');
			} else {
				$this->session->set_flashdata('eerror', 'Ocurrio un error al intentar crear el registro');
			}
			

		}else{
			$this->session->set_flashdata('eerror', 'Error al guardar el registro, contacte al administrador');
		}
		redirect("cla/tipo_producto/form");
	}

	public function edit()
	{
		if($_POST){

			$tipo_producto = $this->input->post('tipo_producto');			
			$nombre = $this->input->post('nombre');			
			$inactivo = $this->input->post('inactivo');					

			$datos = array(
				'nombre' => $nombre,
				'inactivo' => $inactivo,
                'user_update' => $this->session->userdata('usuario'),
                'date_update' => date("Y-m-d H:i:s")
			);


			if ($this->Model_tipo_producto->edit($tipo_producto,$datos)) {
				$this->session->set_flashdata('eok', 'Registro editado satisfactoriamente');
				redirect("cla/tipo_producto/form/".$tipo_producto);
			} else {
				$this->session->set_flashdata('eerror', 'Ocurrio un error al intentar editar el registro');
				redirect("cla/tipo_producto/form/".$tipo_producto);
			}
			

		}else{
			$this->session->set_flashdata('eerror', 'Error al guardar el registro, contacte al administrador');
			redirect("cla/tipo_producto/form");
		}
		
	}	
	
	
    


}