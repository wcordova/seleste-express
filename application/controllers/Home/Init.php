<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Init extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	

	public function __construct()
	{
		parent::__construct();

		$model = array('core/Model_usuarios','cla/Model_tipo_cambio');
		$this->load->model($model);

		$lib = array('usuarios','Nusoap_library');
		$this->load->library($lib);
	}

	public function index()
	{
		$this->load->view('login/inicio');
	}

	function login()
	{
		$name = $this->input->post('username');
		$pass = base64_decode($this->input->post('password'), TRUE);

		$data = array(
			'usu.name'     => $name,
			'usu.pass'	 => sha1($pass),
			'usu.inactivo' => 0,
			'usu.fecha_expira >=' => date("Y-m-d")
		);

		$resultados = $this->Model_usuarios->validar($data);

		if($resultados <>""){	

			$cambio = 'Q. 0.00';
			
			$nombre		= $resultados->nombres.' '.$resultados->apellidos;
			$email 		= $resultados->email;
			$usuario 	= $resultados->usuario;
			$rol 		= $resultados->rol;
			
			$this->usuarios->crearSession($nombre, $email, $usuario, $rol, $cambio);

			$cambioDelDia = $this->Model_tipo_cambio->existeCambioDelDia(date("Y-m-d"));
			
			if ($cambioDelDia == null) {
				$resultado = $this->actualizarTipoCambio();
				if ($resultado == "Error") {
					$cambioVigente =  $this->Model_tipo_cambio->getData();
					$noti_titulo = 'Error. En vigencia!';
					$cambio = $cambioVigente->cambio;
					$this->session->set_flashdata('noti_alert', 'color danger');
				} else {
					$noti_titulo = 'Actualización!';
					$cambio = $resultado;
					$this->session->set_flashdata('noti_alert', 'color success');
				}				
				
			}else{
				$noti_titulo = 'Vigente!';
				$cambio = $cambioDelDia->cambio;
				$this->session->set_flashdata('noti_alert', 'color primary');
			}
			

			$this->session->set_userdata('cambio', $cambio);
			$this->session->set_flashdata('noti_titulo', $noti_titulo);
			$this->session->set_flashdata('noti_descrip', 'Cambio al día: Q '.$cambio);
			
			redirect(site_url('home/init/principal'));
		}else{
			$this->session->set_flashdata('ilogin', 'Credenciales Invalidas');
			redirect(site_url('login'));
		}	
		
	}

	function principal()
	{
		hay_usuario();
		$this->data['menu'] = "init";
		$this->data['titulo'] = "Bienvido";
		$this->load->view('layout/content',$this->data);
	}

	function salir()
	{
		$this->usuarios->borrarSession();
		session_destroy();
		redirect(site_url('login'));
	}

	public function construccion()
	{        
		hay_usuario();
		$this->data['vista'] = "en_construccion";

		$this->load->view('layout/content');
	}

	public function actualizarTipoCambio()
	{
		$client = new nusoap_client(
			'http://www.banguat.gob.gt/variables/ws/TipoCambio.asmx?WSDL',
			'wsdl'
		);
		$isResult = false;

		$err = $client->getError();
		if ($err) {
			echo 'Error: '.$err;
		}

		$param = array();
		$result = $client->call('TipoCambioDia', array('parameters' => $param), '', '', false, true);

		if ($client->fault) {
			print_r($result);
		} else {
			// Check for errors
			$err = $client->getError();
			if ($err) {
				echo $err;
			} else {
				$isResult = true;
				$resultado = $this->establecerTipoCambio($result);
			}
		}

		if ($isResult && $resultado != "false") {
			return $resultado;
		} else {
			return "Error";
		}
		

	}

	public function establecerTipoCambio($TipoCambioDiaResult)
	{
		$cambio = 0.00;
		$fecha = "";

		foreach ((array)$TipoCambioDiaResult as $CambioDolar) {
			foreach ((array)$CambioDolar as $VarDolar) {
				$fechaws = $VarDolar['VarDolar']['fecha'];
				$cambio = $VarDolar['VarDolar']['referencia'];
				$comentario = json_encode($VarDolar);
			break;
			}
		}

		if ($cambio != 0.00 && $fechaws != "") {
			$_date = str_replace('/', '-', $fechaws);
			$fecha = date('Y-m-d', strtotime($_date));
			
			$datos = array(
					'cambio' => $cambio, 
					'fecha_cambio' => $fecha,
					'user_add' => $this->session->userdata('usuario'),
					'comentario' => $comentario
				);

			if ($this->Model_tipo_cambio->create($datos)) {
				return $cambio;
			}else{
				return "false";
			}
		}else{
			return "false";
		}



	}


}
