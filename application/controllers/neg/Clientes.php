<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Clientes extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */


	public function __construct()
	{
		parent::__construct();
		hay_usuario();
		$model = array(
			'neg/Model_cliente',
			'cla/Model_ciudades',
			'neg/Model_orden'
		);

		$this->load->model($model);
	}

	public function buscarClientesPorNombre()
	{
		$nombres = $this->input->post('nombres');
		$apellidos = $this->input->post('apellidos');
		$pais = $this->input->post('pais');

		$result = $this->Model_cliente->buscarClientesPorNombre($nombres, $apellidos, $pais);
		if ($result) {
			echo count($result);
		} else {
			echo 0;
		}
	}

	public function cargarCliente()
	{
		$tipo = $this->input->post('tipo');
		$nombres = $this->input->post('nombres');
		$apellidos = $this->input->post('apellidos');
		$orden = $this->input->post('orden');
		$pais = "GT";

		if ($tipo != 1) {
			$pais = "US";
		}

		$resultados = $this->Model_cliente->buscarClientesPorNombre($nombres, $apellidos, $pais);

		$this->data['titulo'] = "Coincidencias con clientes existentes";
		$this->data['resultado'] = $resultados;
		$this->data['tipo'] = $tipo;
		$this->data['pais'] = $pais;
		$this->data['orden'] = $orden;

		$this->load->view('neg/orden/modal/cliente_existente', $this->data);
	}

	public function clienteExistenteNuevaOrden()
	{
		$cliente = $this->input->post('cliente');
		$tipo = $this->input->post('tipo');
		$orden = $this->input->post('orden');
		$pais = $this->input->post('pais');
		$ciudad = $this->input->post('ciudad');		
		$existOrden = false;

		if ($tipo == 1) {
			$ordenEncabezado = array(
				'anio' => date("Y"),
				'origen' => $pais,
				'ciudad_origen' => $ciudad,
				'cliente_origen' => $cliente,
				'user' => $this->session->userdata('usuario')
			);
		} else {
			$ordenEncabezado = array(
				'anio' => date("Y"),
				'destino' => $pais,
				'ciudad_destino' => $ciudad,
				'cliente_destino' => $cliente,
				'user' => $this->session->userdata('usuario')
			);
		}

		if ($orden != null && $orden != "") {
			$this->Model_orden->edit($orden, $ordenEncabezado);
			$idOrden = $orden;
			$existOrden = true;
		} else {
			$idOrden = $this->Model_orden->create($ordenEncabezado);
		}


		if ($idOrden > 0) {
			if (!$existOrden) {
				$this->session->set_flashdata('eok', 'Se creo la orden #' . $idOrden);
			}

			echo $idOrden;
		} else {
			$this->session->set_flashdata('eerror', 'No se pudo registrar la orden');
			echo 0;
		}
	}
}
