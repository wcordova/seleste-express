<?php
defined('BASEPATH') or exit('No direct script access allowed');

require "vendor/autoload.php";

class Orden extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */


	public function __construct()
	{
		parent::__construct();
		hay_usuario();
		$model = array(
			'neg/Model_cliente',
			'neg/Model_orden',
			'cla/Model_ciudades',
			'cla/Model_tipo_producto',
			'cla/Model_tarifas'
		);
		$helper = array('fechas_helper');

		$this->load->helper($helper);
		$this->load->model($model);
	}

	public function index()
	{
		$this->data['titulo'] =  "Ordenes de Envío";
		$this->data['view'] = "neg/orden/index";
		$this->data['menu'] = "ord";
		$this->data['sub_menu'] = "orden";
		$this->data['resultados'] = $this->Model_orden->getOrdenesList(array(0, 1));

		$this->load->view('layout/content', $this->data);
	}

	public function form($orden = 0, $detalle = 0)
	{
		$this->data['titulo']   =  "Ordenes de Envío";
		$this->data['view']     = "neg/orden/form";
		$this->data['menu']     = "ord";
		$this->data['sub_menu'] = "orden";
		$this->data['detalle']  =  $detalle;

		$datosOrden = '';
		if ($orden > 0) {
			$datosOrden = $this->Model_orden->getData($orden);
		}

		$datosDetalle = '';

		if ($detalle > 0) {
			$datosDetalle = $this->Model_orden->getDetalleOrden($orden, $detalle);
		}

		$this->data['datosOrden']   = $datosOrden;
		$this->data['datosDetalle'] = $datosDetalle;

		$this->load->view('layout/content', $this->data);
	}

	public function encabezado()
	{
		if ($_POST) {
			$origen_nombres = $this->input->post('origen_nombres');
			$origen_apellidos = $this->input->post('origen_apellidos');
			$origen_pais = $this->input->post('origen_pais');
			$origen_ciudad = $this->input->post('origen_ciudad');
			$origen_correo = $this->input->post('origen_correo');
			$origen_direccion = $this->input->post('origen_direccion');
			$origen_telefono = $this->input->post('origen_telefono');

			$destino_nombres = $this->input->post('destino_nombres');
			$destino_apellidos = $this->input->post('destino_apellidos');
			$destino_pais = $this->input->post('destino_pais');
			$destino_ciudad = $this->input->post('destino_ciudad');
			$destino_correo = $this->input->post('destino_correo');
			$destino_direccion = $this->input->post('destino_direccion');
			$destino_telefono = $this->input->post('destino_telefono');

			$cobro_destino = !is_null($this->input->post('cobro_destino')) ? 1 : 0;

			$clienteOrigen = array(
				'nombres' => $origen_nombres,
				'apellidos' => $origen_apellidos,
				'pais' => $origen_pais,
				'ciudad' => $origen_ciudad,
				'correo' => $origen_correo,
				'direccion1' => $origen_direccion,
				'telefono' => $origen_telefono,
				'user_add' => $this->session->userdata('usuario')
			);

			$clienteDestino = array(
				'nombres' => $destino_nombres,
				'apellidos' => $destino_apellidos,
				'pais' => $destino_pais,
				'ciudad' => $destino_ciudad,
				'correo' => $destino_correo,
				'direccion1' => $destino_direccion,
				'telefono' => $destino_telefono,
				'user_add' => $this->session->userdata('usuario')
			);

			$idCliOrigen = $this->Model_cliente->create($clienteOrigen);
			if ($idCliOrigen > 0) {
				$idCliDestino = $this->Model_cliente->create($clienteDestino);
				if ($idCliDestino > 0) {
					$ordenEncabezado = array(
						'anio' => date("Y"),
						'origen' => $origen_pais,
						'ciudad_origen' => $origen_ciudad,
						'destino' => $destino_pais,
						'ciudad_destino' => $destino_ciudad,
						'cliente_origen' => $idCliOrigen,
						'cliente_destino' => $idCliDestino,
						'cobro_origen_destino ' => $cobro_destino,
						'user' => $this->session->userdata('usuario')
					);

					$idOrden = $this->Model_orden->create($ordenEncabezado);

					if ($idOrden > 0) {
						$this->session->set_flashdata('eok', 'Datos registrados con éxito.');
						redirect("neg/orden/form/" . $idOrden);
					} else {
						$this->session->set_flashdata('eerror', 'Error al insertar orden.');
					}
				} else {
					$this->session->set_flashdata('eerror', 'Error al insertar cliente de destino.');
				}
			} else {
				$this->session->set_flashdata('eerror', 'Error al insertar cliente de origen.');
			}
		}
	}

	public function encabezadoActualizar()
	{
		if ($_POST) {
			$orden = $this->input->post('orden');

			$origen_id = $this->input->post('origen_id');
			$origen_nombres = $this->input->post('origen_nombres');
			$origen_apellidos = $this->input->post('origen_apellidos');
			$origen_pais = $this->input->post('origen_pais');
			$origen_ciudad = $this->input->post('origen_ciudad');
			$origen_correo = ($this->input->post('origen_correo') != '') ? $this->input->post('origen_correo') : null;
			$origen_direccion = $this->input->post('origen_direccion');
			$origen_telefono = $this->input->post('origen_telefono');

			$destino_id = $this->input->post('destino_id');
			$destino_nombres = $this->input->post('destino_nombres');
			$destino_apellidos = $this->input->post('destino_apellidos');
			$destino_pais = $this->input->post('destino_pais');
			$destino_ciudad = $this->input->post('destino_ciudad');
			$destino_correo = ($this->input->post('destino_correo') != '') ? $this->input->post('destino_correo') : null;
			$destino_direccion = $this->input->post('destino_direccion');
			$destino_telefono = $this->input->post('destino_telefono');

			$cobro_destino = !is_null($this->input->post('cobro_destino')) ? 1 : 0;

			$clienteOrigen = array(
				'nombres' => $origen_nombres,
				'apellidos' => $origen_apellidos,
				'pais' => $origen_pais,
				'ciudad' => $origen_ciudad,
				'correo' => $origen_correo,
				'direccion1' => $origen_direccion,
				'telefono' => $origen_telefono,
				'user_update' => $this->session->userdata('usuario'),
				'date_update' => date("Y-m-d H:i:s")
			);

			$clienteDestino = array(
				'nombres' => $destino_nombres,
				'apellidos' => $destino_apellidos,
				'pais' => $destino_pais,
				'ciudad' => $destino_ciudad,
				'correo' => $destino_correo,
				'direccion1' => $destino_direccion,
				'telefono' => $destino_telefono,
				'user_update' => $this->session->userdata('usuario'),
				'date_update' => date("Y-m-d H:i:s")
			);

			if ($origen_id > 0) {
				$this->Model_cliente->edit($origen_id, $clienteOrigen);
			} else {
				$origen_id = $this->Model_cliente->create($clienteOrigen);
			}
			if ($destino_id > 0) {
				$this->Model_cliente->edit($destino_id, $clienteDestino);
			} else {
				$destino_id = $this->Model_cliente->create($clienteDestino);
			}

			$ordenEncabezado = array(
				'origen' => $origen_pais,
				'ciudad_origen' => $origen_ciudad,
				'destino' => $destino_pais,
				'ciudad_destino' => $destino_ciudad,
				'cliente_origen' => $origen_id,
				'cliente_destino' => $destino_id,
				'cobro_origen_destino ' => $cobro_destino,
				'user' => $this->session->userdata('usuario')
			);

			if ($this->Model_orden->edit($orden, $ordenEncabezado)) {
				$this->session->set_flashdata('eok', 'Datos actualizados con éxito.');
			} else {
				$this->session->set_flashdata('eerror', 'Error al actualizar orden.');
			}


			redirect("neg/orden/form/" . $orden);
		}
	}

	public function ampliarDatosCliente()
	{
		$cliente = $this->input->post('cliente');
		$resultados = $this->Model_cliente->getData($cliente);

		$titulo = 'Error en consulta.';
		if (!empty($resultados)) {
			$titulo = "Ampliar datos del cliente: " . $resultados->nombres . ' ' . $resultados->apellidos;
		}

		$this->data['titulo'] = $titulo;
		$this->data['resultado'] = $resultados;

		$this->load->view('neg/orden/modal/cliente', $this->data);
	}

	public function guardarForm()
	{
		if ($_POST) {
			$cliente = $this->input->post('cliente');
			$cui = ($this->input->post('cui') != '') ? $this->input->post('cui') : null;
			$nit = ($this->input->post('nit') != '') ? $this->input->post('nit') : null;
			$codigo_postal = ($this->input->post('codigo_postal') != '') ? $this->input->post('codigo_postal') : null;
			$direccion2 = ($this->input->post('direccion2') != '') ? $this->input->post('direccion2') : null;
			$direccion3 = ($this->input->post('direccion3') != '') ? $this->input->post('direccion3') : null;

			$datos = array(
				'cui' => $cui,
				'nit' => $nit,
				'codigo_postal' => $codigo_postal,
				'direccion2' => $direccion2,
				'direccion3' => $direccion3
			);

			if ($this->Model_cliente->edit($cliente, $datos)) {
				return true;
			} else {
				return false;
			}
		}
	}

	public function agregarDetalle()
	{
		if ($_POST) {
			$orden = $this->input->post('orden');
			$detalle = $this->input->post('detalle');
			$producto = $this->input->post('producto');		
			$largo = $this->input->post('largo');
			$ancho = $this->input->post('ancho');
			$alto = $this->input->post('alto');
			$peso = $this->input->post('peso');
			$peso_volumetrico = $this->input->post('peso_volumetrico');
			$unidad_medida = $this->input->post('unidad_medida');
			$unidad_peso = $this->input->post('unidad_peso');
			$tipo_producto = $this->input->post('tipo_producto');
			$descripcion = $this->input->post('descripcion');
			$medida_peso_volumetrico = ($unidad_medida != 'in') ? 'kg' : 'lb';
			$envios_domesticos = $this->input->post('envios_domesticos');			 						

			$moneda_tarifa = $this->input->post('moneda_tarifa');
			$tarifa = $this->input->post('tarifa');
			$moneda = $this->input->post('moneda');
			$valor_declarado = $this->input->post('valor_declarado');
			$cambio = floatval($this->session->userdata('cambio'));

			if($moneda_tarifa == 1) {
				$tarifa =  $tarifa / $cambio;
				$moneda_tarifa = 0;
			}

			if($moneda == 1) {
				$valor_declarado = $valor_declarado / $cambio;
				$moneda = 0;
			}

			$datos = array(
				'orden' => $orden,
				'producto' => $producto,
				'moneda_tarifa' => $moneda_tarifa,
				'tarifa' => $tarifa,
				'moneda' => $moneda,
				'valor_declarado' => $valor_declarado,
				'largo' => $largo,
				'ancho' => $ancho,
				'alto' => $alto,
				'unidad_medida' => $unidad_medida,
				'peso' => $peso,
				'unidad_peso' => $unidad_peso,
				'peso_volumetrico' => $peso_volumetrico,
				'tipo_producto' => $tipo_producto,
				'descripcion' => $descripcion,
				'medida_peso_volumetrico' => $medida_peso_volumetrico,
				'cambio' => $cambio,
				'envios_domesticos' => $envios_domesticos
			);

			if ($detalle > 0) {
				if ($this->Model_orden->actualizarItemDetalle($datos, $detalle)) {
					echo true;
				} else {
					echo false;
				}
			} else {
				if ($this->Model_orden->agregarOrdenDetalle($datos)) {
					echo true;
				} else {
					echo false;
				}
			}
		} else {
			echo false;
		}
	}

	public function mostrarDetalle()
	{
		$orden = $this->input->post('orden');
		$this->data['orden'] = $orden;

		$this->load->view('neg/orden/tags/detalle_lista', $this->data);
	}



	function editarItemDetalle(){
		$this->data['datosOrden'] = '';

		$this->load->view('neg/orden/tags/detalle', $this->data);
	}

	public function eliminarItemDetalle()
	{
		$orden = $this->input->post('orden');
		$detalle = $this->input->post('detalle');
		$this->data['orden'] = $orden;
		$this->session->set_flashdata('noti_descrip', 'Item eliminado');

		$this->Model_orden->eliminarItemDetalle($detalle);

		$this->load->view('neg/orden/tags/detalle_lista', $this->data);
	}

	public function actualizarResumen()
	{
		$orden = $this->input->post('orden');
		$datosOrden = $this->Model_orden->getData($orden);

		$this->data['orden'] = $orden;
		$this->data['datosOrden'] = $datosOrden;

		$this->load->view('neg/orden/tags/resumen', $this->data);
	}

	public function completarOrden()
	{
		$orden = $this->input->post('orden');

		$datos = array('estado' => 1);
		if ($this->Model_orden->actualizarOrden($orden, $datos)) {
			return true;
		} else {
			return false;
		}
	}

	public function cancelarOrden()
	{
		$orden = $this->input->post('orden');

		$datos = array('estado' => 9);
		if ($this->Model_orden->actualizarOrden($orden, $datos)) {
			return true;
		} else {
			return false;
		}
	}
}
