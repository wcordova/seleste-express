<?php
defined('BASEPATH') or exit('No direct script access allowed');

require "vendor/autoload.php";

use Dompdf\Dompdf;

class Reportes extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		hay_usuario();
		$model = array(
			'neg/Model_cliente',
			'neg/Model_orden',
			'cla/Model_ciudades',
			'cla/Model_tipo_producto',
			'cla/Model_tarifas'
		);
		$helper = array('fechas_helper');

		$this->load->helper($helper);
		$this->load->model($model);

		$this->generar_pdf = true;
	}

	public function printOrden($orden, $tipo = 0)
	{
		//$orden = $this->input->post('orden');
		$datosOrden = $this->Model_orden->getData($orden);
		$de = $this->Model_cliente->getCliente($datosOrden->cliente_origen);
		$para = $this->Model_cliente->getCliente($datosOrden->cliente_destino);
		$detalle = $this->Model_orden->getDetalleOrden($orden);
		$codigo = $datosOrden->origen . '-' . $datosOrden->anio . '-' . $orden;

		$generator = new Picqer\Barcode\BarcodeGeneratorPNG();
		$barCode = base64_encode($generator->getBarcode($codigo, $generator::TYPE_CODE_128));

		$data = array(
			'orden' => $orden,
			'datosOrden' => $datosOrden,
			'de' => $de,
			'para' => $para,
			'detalle' => $detalle,
			'barCode' => $barCode,
			'tipo' => $tipo
		);

		$nombre = "Orden #" . $orden;

		// falta incrementar el largo dependiendo de la cantidad de registros

		$largo = 200; // mm de alto del tiket

		$html = $this->load->view('neg/orden/print/orden2', $data, true);

		$dompdf = new DOMPDF();
		//hoja personalizada de 50mm por 10cm en puntos tipograficos
		$dompdf->set_paper(array(0, 0, 141, ($largo * 2.8346)));
		$dompdf->load_html($html, 'UTF-8');
		$dompdf->render();
		$dompdf->stream($nombre, array("Attachment" => 0));
	}
}
