<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Seguimiento extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */


	public function __construct()
	{
		parent::__construct();
		hay_usuario();
		$model = array(
			'neg/Model_cliente',
			'neg/Model_orden',
			'neg/Model_consolidado',
			'cla/Model_ciudades',
			'cla/Model_tipo_producto',
			'cla/Model_tarifas',
			'neg/Model_embarque'
		);
		$helper = array('fechas_helper', 'notificacion_helper');

		$this->load->helper($helper);
		$this->load->model($model);
	}

	public function pendientes()
	{

		$this->data['titulo'] =  "Listado de Ordenes";
		$this->data['view'] = "neg/seguimiento/index/pendientes";
		$this->data['menu'] = "ord";
		$this->data['sub_menu'] = "seg";
		$this->data['pendiente'] = $this->Model_orden->getOrdenesList(array(1));

		$this->load->view('layout/content', $this->data);
	}

	public function consolidadoInicializado()
	{
		$this->data['titulo'] =  "Listado de Ordenes";
		$this->data['view'] = "neg/seguimiento/desconsolidado/tabInit";
		$this->data['menu'] = "con";
		$this->data['sub_menu'] = "init";
		$this->data['iniciados'] = $this->Model_consolidado->getOrdenesList(array(0));

		$this->load->view('layout/content', $this->data);
	}

	public function consolidadoEnviado()
	{
		$this->data['titulo'] =  "Listado de Ordenes";
		$this->data['view'] = "neg/seguimiento/desconsolidado/tabEnviado";
		$this->data['menu'] = "con";
		$this->data['sub_menu'] = "sen";
		$this->data['enviados'] = $this->Model_consolidado->getOrdenesList(array(1));

		$this->load->view('layout/content', $this->data);
	}

	public function consolidadoRecibido()
	{
		$this->data['titulo'] =  "Listado de Ordenes";
		$this->data['view'] = "neg/seguimiento/desconsolidado/tabRecibido";
		$this->data['menu'] = "con";
		$this->data['sub_menu'] = "desc";
		$this->data['recibidos'] = $this->Model_consolidado->getOrdenesList(array(1));

		$this->load->view('layout/content', $this->data);
	}

	public function consolidadoDesconsolidar()
	{
		$this->data['titulo'] =  "Listado de Ordenes";
		$this->data['view'] = "neg/seguimiento/desconsolidado/tabDesconsolidado";
		$this->data['menu'] = "con";
		$this->data['sub_menu'] = "desf";
		$this->data['desconsolidar'] = $this->Model_consolidado->getOrdenesList(array(2));

		$this->load->view('layout/content', $this->data);
	}

	public function enviadas()
	{

		$this->data['titulo'] =  "Listado de Ordenes";
		$this->data['view'] = "neg/seguimiento/index/enviados";
		$this->data['menu'] = "ord";
		$this->data['sub_menu'] = "env";
		$this->data['enviado'] = $this->Model_orden->getOrdenesList(array(2));

		$this->load->view('layout/content', $this->data);
	}

	public function recibidas()
	{

		$this->data['titulo'] =  "Listado de Ordenes";
		$this->data['view'] = "neg/seguimiento/index/recibidos";
		$this->data['menu'] = "ord";
		$this->data['sub_menu'] = "rec";
		$this->data['recibido'] = $this->Model_orden->getOrdenesList(array(3));

		$this->load->view('layout/content', $this->data);
	}

	public function createConsolidado()
	{
		$datos = array('anio' => date("Y"), 'user' => $this->session->userdata('usuario'));
		$id = $this->Model_consolidado->crearNuevo($datos);

		redirect("neg/seguimiento/seguimientoConsolidado/" . $id);
	}

	public function seguimientoConsolidado($consolidado)
	{
		$this->data['view'] = "neg/seguimiento/consolidado/seguimiento";
		$this->data['menu'] = "con";
		$this->data['sub_menu'] = "init";
		$this->data['consolidado'] = $consolidado;
		$this->data['embarque'] = $this->Model_embarque->getEmbarques(0);
		$this->data['titulo'] =  "Consolidado #" . $consolidado;
		$this->data['resultados'] = $this->Model_consolidado->getConsolidado($consolidado);
		$this->data['action'] = site_url('neg/seguimiento/editConsolidado');

		$this->load->view('layout/content', $this->data);
	}

	public function asignarGuiaConsolidado()
	{
		$consolidado = $this->input->post('consolidado');
		$orden = $this->input->post('orden');
		$datos = array('orden' => $orden, 'consolidado' => $consolidado);

		if ($this->Model_consolidado->asignarGuia($datos)) {
			notification_success('Guia asignada correcctamente');
			return true;
		} else {
			notification_danger('Error en asignación de guia');
			return false;
		}
	}

	public function quitarGuiaConsolidado()
	{
		$consolidado = $this->input->post('consolidado');
		$orden = $this->input->post('orden');

		if ($this->Model_consolidado->quitarGuia($consolidado, $orden)) {
			notification_warning('Guia eliminada');
			return true;
		} else {
			notification_danger('Error al eliminar la guia');
			return false;
		}
	}

	public function asignarGuiaMadre()
	{
		$consolidado = $this->input->post('consolidado');
		$orden = $this->input->post('orden');
		$guia = $this->input->post('guia');
		$guia .= '-CO-'.$consolidado;

		$datos = array('no_referencia' => $guia);

		if ($this->Model_consolidado->limpiarGuiaMadre($consolidado, $orden)) {
			if ($this->Model_consolidado->asignarGuiaMadre($consolidado, $orden)) {
				if ($this->Model_consolidado->editarConsolidado($consolidado, $datos)) {
					notification_primary('Guia madre asignada');
					return true;
				} else {
					notification_danger('Error al editar registro de consolidado');
					return false;
				}
			} else {
				notification_danger('Error al asignar guia madre');
				return false;
			}
		} else {
			notification_danger('Error al limpiar guia madre');
			return false;
		}
	}

	public function editConsolidado()
	{
		$consolidado = $this->input->post('consolidado');
		$referencia = $this->input->post('referencia');
		$observaciones = $this->input->post('observaciones');
		$embarque = $this->input->post('embarque');		

		$datos = array('no_referencia' => $referencia, 'observaciones' => $observaciones, 'embarque' => $embarque);

		if ($this->Model_consolidado->editarConsolidado($consolidado, $datos)) {
			notification_success('Datos actualizados exitosamente.');
		} else {
			notification_danger('No se actualizo el registro.');
		}
		redirect("neg/seguimiento/seguimientoConsolidado/" . $consolidado);
	}

	public function cambiarEstadoOrden()
	{
		$orden = $this->input->post('orden');
		$estado = $this->input->post('estado');
		$regresa = $this->input->post('regresa');

		if (strstr($orden, '-')) {
			$orden = explode("-", $orden)[2];
		}

		$descripion =  "actualizada";
		switch ($estado) {
			case '1':
				$descripion = "Completada";
				break;
			case '2':
				$descripion = "Enviada";
				break;
			case '3':
				$descripion = "Recibida";
				break;
			case '4':
				$descripion = "Finalizada";
				break;
		}

		$datos = array('estado' => $estado);
		if ($this->Model_orden->actualizarOrden($orden, $datos)) {
			if ($regresa == 1) {
				notification_warning('La orden ha regresado a estado: ' . $descripion);
			} else {
				notification_primary('La orden ha sido ' . $descripion);
			}

			return true;
		} else {
			notification_danger('No se actualizo la orden.');
			return false;
		}
	}

	function cambiarEstadoConsolidado()
	{
		$consolidado = $this->input->post('consolidado');
		$estado = $this->input->post('estado');
		$regresa = $this->input->post('regresa');

		$descripion =  "Iniciado";
		switch ($estado) {
			case '1':
				$descripion = "Enviado";
				break;
			case '2':
				$descripion = "Recibido";
				break;
		}

		$datos = array('estado' => $estado);
		if ($this->Model_consolidado->editarConsolidado($consolidado, $datos)) {
			if ($regresa == 1) {
				notification_warning('El consolidado ha regresado a estado: ' . $descripion);
			} else {
				notification_primary('El consolidado ha sido ' . $descripion);
			}

			return true;
		} else {
			notification_danger('No se actualizo la orden.');
			return false;
		}
	}

	public function validarConsolidado($consolidado)
	{
		$this->data['view'] = "neg/seguimiento/consolidado/desconsolidar";
		$this->data['menu'] = "con";
		$this->data['sub_menu'] = "desc";
		$this->data['consolidado'] = $consolidado;
		$this->data['titulo'] =  "Consolidado #" . $consolidado;
		$this->data['resultados'] = $this->Model_consolidado->getConsolidado($consolidado);

		$this->load->view('layout/content', $this->data);
	}

	public function desconsolidarOrden()
	{
		$consolidado = $this->input->post('consolidado');
		$orden = $this->input->post('orden');
		$datos = array('desconsolidado' => 1);

		if ($this->Model_consolidado->editarOrdenConsolidado($consolidado, $orden, $datos)) {
			notification_primary('La orden ha sido valida y desconsolidada.');
			return true;
		} else {
			notification_danger('No se desconsolido la orden.');
			return false;
		}

	}

	public function cerrarConsolidado()
	{
		$consolidado = $this->input->post('consolidado');
		$datos = array('estado' => 9);

		if ($this->Model_consolidado->editarConsolidado($consolidado, $datos)) {
			notification_primary('El consolidado ha sido cerrado.');
			return true;
		} else {
			notification_danger('No se cerro el consolidado.');
			return false;
		}
	}

}
