<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Embarque extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */


	public function __construct()
	{
		parent::__construct();
		hay_usuario();
		$model = array(
			'neg/Model_embarque'
		);

		$this->load->model($model);
	}

	public function index()
	{
		$this->data['titulo'] =  "Embarque";
		$this->data['view'] = "neg/embarque/index";
		$this->data['menu'] = "neg";
		$this->data['sub_menu'] = "emb";
		$this->data['resultados'] = $this->Model_embarque->getData();

		$this->load->view('layout/content', $this->data);
	}

	public function form($embarque = "")
	{
		$this->data['titulo'] = "Mantenimiento de embarques";
		$this->data['view'] = "neg/embarque/form";
		$this->data['menu'] = "neg";
		$this->data['sub_menu'] = "emb";
		$this->data['subTitulo'] = "Llene la información del embarque.";
		$this->data['action'] = site_url('neg/embarque/create');
		$this->data['resultado'] = null;

		if ($embarque != "") {
			$this->data['action'] = site_url('neg/embarque/edit');
			$this->data['resultado'] = $this->Model_embarque->getData($embarque);
		}
		
		$this->load->view('layout/content', $this->data);
	}

	public function create()
	{
		if($_POST){

			$descripcion = $this->input->post('descripcion');

			$datos = array(
				'descripcion' => $descripcion,
				'user_add' => $this->session->userdata('usuario')
			);

			$result = $this->Model_embarque->create($datos);
			if ($result != false) {
				$this->session->set_flashdata('eok', 'Registro creado satisfactoriamente');
			} else {
				$this->session->set_flashdata('eerror', 'Ocurrio un error al intentar crear el registro');
			}
			
		}else{
			$this->session->set_flashdata('eerror', 'Error al guardar el registro, contacte al administrador');
		}
		redirect("neg/embarque/form/".$result);
	}

	public function edit()
	{
		if($_POST){

			$embarque = $this->input->post('embarque');			
			$descripcion = $this->input->post('descripcion');				

			$datos = array(
				'descripcion' => $descripcion,
                'user_update' => $this->session->userdata('usuario'),
                'date_update' => date("Y-m-d H:i:s")
			);

			if ($this->Model_embarque->edit($embarque,$datos)) {
				$this->session->set_flashdata('eok', 'Registro editado satisfactoriamente');
			} else {
				$this->session->set_flashdata('eerror', 'Ocurrio un error al intentar editar el registro');
			}
			redirect("neg/embarque/form/".$embarque);			

		}else{
			$this->session->set_flashdata('eerror', 'Error al guardar el registro, contacte al administrador');
			redirect("neg/embarque/form/");
		}
		
	}	


}